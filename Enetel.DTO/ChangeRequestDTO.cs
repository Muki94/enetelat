﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Enetel.DTO
{
    public enum enum_RequestType
    {
        NEW = 200000000,
        EDIT = 200000001,
        TERMINATE = 200000002
    }

    public enum Gender
    {
        Female = 200000000,
        Male = 200000001
    }

    public enum InsuredRole
    {
        Principal = 200000000,
        Spouse = 200000001,
        Child = 200000002
    }

    public class ChangeRequestDTO
    {
        public string tmp_PolicyName { get; set; }
        public string tmp_PolicyNumber { get; set; }
        public string tmp_InsuredName { get; set; }
        public string tmp_PlanName { get; set; }
        public string tmp_ReportingResponsibleFullName { get; set; }
        public string tmp_PaymentResponsibleFullName { get; set; }
        public string tmp_CitizenshipName { get; set; }
        public string tmp_ResidenceCountryName { get; set; }
        public string tmp_PrincipalName { get; set; }
        public string tmp_PortalUserName { get; set; }
        public string tmp_CensusLineName { get; set; }
        public string tmp_InsuredRole { get; set; }
        public bool tmp_ShowSalary { get; set; }

        public Guid? Id { get; set; }

        public Guid? PortalUser { get; set; }

        [DisplayName("Insured")]
        public Guid? Insured { get; set; }

        [DisplayName("ID")]
        public string Name { get; set; }

        [DisplayName("First Name")]
        public string FirstName { get; set; }

        [DisplayName("Last Name")]
        [Required]
        public string LastName { get; set; }

        [DisplayName("Gender")]
        [Required]
        public Gender? Gender { get; set; }

        [DisplayName("Date of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        [Required]
        public DateTime? DateofBirth { get; set; }

        public int? YearOfBirth { get; set; }

        [DisplayName("Unique ID")]
        public string externalID { get; set; }

        [DisplayName("Citizenship")]
        [Required]
        public Guid? Citizenship { get; set; }

        [DisplayName("Residence Country")]
        [Required]
        public Guid? ResidenceCountry { get; set; }

        [DisplayName("Insured Role")]
        [Required]
        public InsuredRole? InsuredRole { get; set; }

        [DisplayName("Principal")]
        public Guid? Principal { get; set; }

        [DisplayName("Plan")]
        [Required]
        public Guid? Plan { get; set; }

        [DisplayName("Date Left")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        public DateTime? DateLeft { get; set; }

        [DisplayName("Date Joined")]
        [DataType(DataType.Date)]
        [Required]
        [DisplayFormat(ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, DataFormatString = "{0:yyyy-MM-dd}", HtmlEncode = true, NullDisplayText = "")]
        public DateTime? DateJoined { get; set; }

        [DisplayName("Salary")]
        [RegularExpression(@"^-?\d*\.?\d+", ErrorMessage = "Salary must be a number!")]
        public decimal? Salary { get; set; }

        [DisplayName("Salary Description")]
        public string SalaryDescription { get; set; }

        [DisplayName("Salary Currency")]
        public Guid? SalaryCurrencyId { get; set; }
        public string SalaryCurrency { get; set; }

        [DisplayName("Sum Insured")]
        [RegularExpression(@"^-?\d*\.?\d+", ErrorMessage = "Salary must be a number!")]
        public decimal? SumInsured { get; set; }

        [DisplayName("Sum Insured Currency")]
        public Guid? SumInsuredCurrencyId { get; set; }
        public string SumInsuredCurrency { get; set; }

        [DisplayName("Person’s Account")]
        public Guid? PersonsAccountId { get; set; }

        public string PersonsAccount { get; set; }

        public Guid? Policy { get; set; }

        //[DisplayName("This person is Payment Responsible")]
        //public bool? ThispersonisPaymentResponsible { get; set; }

        //[DisplayName("This person is Reporting Responsible")]
        //public bool? ThispersonisReportingResponsible { get; set; }

        [DisplayName("Payment Responsible")]
        [Required]
        public Guid? PaymentResponsible { get; set; }

        [DisplayName("Reporting Responsible")]
        public Guid? ReportingResponsible { get; set; }

        public DateTime? CreatedOn { get; set; }

        public int? RequestTypeNumber { get; set; }
        public string RequestType { get; set; }

        public int StatusCodeNumber { get; set; }
        public string StatusCode { get; set; }

        public Guid CensusLineId { get; set; }
        public string Remarks { get; set; }

        [DisplayName("Rejection Reason")]
        public string RejectionReason { get; set; }
        public int? RejectionReasonCode { get; set; }
        public string RejectedOtherReason { get; set; }
        public string FullName { get; set; }
        public string GenderName { get; set; }
    }
}
