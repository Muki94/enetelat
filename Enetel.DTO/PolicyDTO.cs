﻿using Enetel.DAL.Models;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Enetel.DTO
{
    public class PolicyDTO
    {
        public enum InsuranceType
        {
            Health = 101,
            Life = 141
        }

        public Guid Id { get; set; }
        public string ene_InternalPolicyName { get; set; }
        public string ene_policynumber { get; set; }
        public string ene_ExternalID { get; set; }
        public bool? ene_AdditionalIndividualUnderwriting { get; set; }
        public int? ene_Allcensuslinesactive { get; set; }

        [DisplayName("Census Lines Active Date")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_Allcensuslinesactive_Date { get; set; }
        public int? ene_Allcensuslinesactive_State { get; set; }
        public decimal? ene_ClaimsFund { get; set; }
        public bool? ene_Complete { get; set; }
        public string ene_ExternalPolicyNo { get; set; }
        public int? ene_GroupSize { get; set; }

        [DisplayName("Group Size Date")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_GroupSize_Date { get; set; }
        public int? ene_GroupSize_State { get; set; }
        public decimal? ene_GroupSizeDiscountIfAny { get; set; }
        public int? ene_GroupsizeManual { get; set; }
        public decimal? ene_GroupUnderwritingDiscountOrSurchargeIfAny { get; set; }
        public bool? ene_MandatoryGroup { get; set; }
        public int? ene_NumberofDraftCensusLines { get; set; }

        [DisplayName("Number Of Draft Census Lines Date")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_NumberofDraftCensusLines_Date { get; set; }
        public int? ene_NumberofDraftCensusLines_State { get; set; }

        [DisplayName("Modified On")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public Guid CreatedById { get; set; }
        public string ModifiedBy { get; set; }
        public Guid ModifiedById { get; set; }
        public string ene_PolicyPeriodNo { get; set; }
        public decimal? ene_Premium { get; set; }

        [DisplayName("Premium Date")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_Premium_Date { get; set; }
        public int? ene_Premium_State { get; set; }
        public bool? ene_Premiumratechanges { get; set; }
        public bool? ene_ReadyforInvoicing { get; set; }
        public bool? ene_ResetRateStatus { get; set; }

        [DisplayName("Valid From")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_ValidFrom { get; set; }

        [DisplayName("Start Date")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_StartDate { get; set; }

        [DisplayName("End Date")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_EndDate { get; set; }

        [DisplayName("Created On")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? CreatedOn { get; set; }
        public Guid? ene_ContractingCurrency { get; set; }
        public Guid? ene_PolicyHolder { get; set; }

        [DisplayName("Insurance Type")]
        public InsuranceType? ene_insuranceType { get; set; }
        public string tmp_insuranceType { get; set; }

        public string ene_policyStatus { get; set; }
        public int? ene_policyStatusCode { get; set; }
    }
}
