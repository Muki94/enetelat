﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enetel.DAL.Models;

namespace Enetel.DTO
{

    public enum DocumentType
    {
        Invoice = 200000000,
        RequestForPayment = 200000001,
        CreditNote = 200000002
    }

    public class InvoiceDTO
    {
        public Guid Id { get; set; }

        [DisplayName("Amount For Pay")]
        public decimal? AmountForPay { get; set; }

        [DisplayName("Archived")]
        public bool? Archived { get; set; }

        [DisplayName("Calculation Time Stamp")]
        public DateTime? CalculationTimeStamp { get; set; }

        [DisplayName("Currency")]
        public Guid? CurrencyId { get; set; }
        public string Currency { get; set; }

        [DisplayName("Document Date")]
        public DateTime? DocumentDate { get; set; }

        [DisplayName("Document No.")]
        public string DocumentNo { get; set; }

        [DisplayName("Document Template")]
        public Guid? DocumentTemplateId { get; set; }
        public string DocumentTemplate { get; set; }

        [DisplayName("Document Type")]
        public DocumentType? DocumentType { get; set; }

        [DisplayName("Due Date")]
        public DateTime? DueDate { get; set; }

        [DisplayName("Payment Responsible")]
        public Guid? PaymentResponsibleId { get; set; }
        public string PaymentResponsible { get; set; }

        [DisplayName("Period End Date")]
        public DateTime? PeriodEndDate { get; set; }

        [DisplayName("Period Start Date")]
        public DateTime? PeriodStartDate { get; set; }

        [DisplayName("Policy")]
        public Guid? PolicyId { get; set; }
        public string Policy { get; set; }

        [DisplayName("Created On")]
        public DateTime? CreatedOn { get; set; }

        [DisplayName("Status")]
        public string StatusReason { get; set; }
        public int? StatusReasonCode { get; set; }

        [DisplayName("Document Location")]
        public string DocumentLocationPath { get; set; }
    }
}
