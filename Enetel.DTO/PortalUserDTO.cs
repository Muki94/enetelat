﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Enetel.DTO
{
    public class PortalUserDTO
    {
        public Guid Id { get; set; }
        public string LogicalName { get; set; }

        [Required(ErrorMessage = "First name is a required field.")]
        public string ene_FirstName { get; set; }

        [Required(ErrorMessage = "Last name is a required field.")]
        public string ene_LastName { get; set; }

        [DataType(DataType.EmailAddress)]
        public string ene_Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string ene_PhoneNumber { get; set; }

        [Required(ErrorMessage = "Username is a required field.")]
        public string ene_name { get; set; }

        [DataType(DataType.Password)]
        [StringLength(50, MinimumLength = 8, ErrorMessage = "Password must have at least 8 characters.")]
        [RegularExpression("^(?=.*[a-zA-Z])(?=.*[0-9]).+$", ErrorMessage = "Password must contain at least one letter(a-z) and one digit(0-9) and has to be at least 8 characters long.")]
        //[RegularExpression("/\d/", ErrorMessage = "Password must contain at least one letter(a-z) and one digit(0-9) and has to be at least 8 characters long.")]
        //[RegularExpression("/[A-Z]/i", ErrorMessage = "Password must contain at least one letter(a-z) and one digit(0-9) and has to be at least 8 characters long.")]
        public string ene_Password { get; set; }

        public Guid? ene_Account { get; set; }
        public string ene_AccountLogicalName { get; set; }

        [Required]
        public bool ene_Admin { get; set; }

        [DisplayName("Created On")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? CreatedOn { get; set; }

        [DisplayName("Modified On")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ModifiedOn { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedBy { get; set; }
        public Guid? CreatedById { get; set; }
        public Guid? ModifiedById { get; set; }
        public string statuscode { get; set; }
        public string statecode { get; set; }
        public int statuscodeNumber { get; set; }
        public int statecodeNumber { get; set; }

        //change password properties
        public string oldPassword { get; set; }
        public string newPassword { get; set; }
        public string newPasswordConfirmation { get; set; }

        public string Salt { get; set; }

        public bool canAccessInvoices { get; set; }
    }
}
