﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.DTO
{
    public class FilterAttributesDTO
    {
        public enum CensusLineStatus
        {
            Active = 0,
            Inactive = 1
        }

        public enum DocumentTypes
        {
            ID = 200000000,
            Health_Questionnaire = 200000001,
            Text_Document = 200000002,
            Other = 200000003
        }

        public enum PersonsColumns
        {
            FullName = 1,
            ExternalId,
            Gender,
            DateOfBirth,
            Citizenship,
            ResidenceCountry
        }

        public enum PortalUsersColumns
        {
            FirstName = 1,
            LastName,
            Email,
            Name,
            CreatedOn,
            StatusCode
        }

        public enum PoliciesColumns
        {
            PolicyNumber = 1,
            ExternalPolicyNo,
            InsuranceType,
            StartDate,
            EndDate,
            GroupSize
        }

        public enum CensusLinesColumns
        {
            CensusLineNumber = 1,
            FullName,
            UniqueId,
            InsuredRole,
            Plan,
            DateJoined,
            Rate
        }

        public enum LogsColumns
        {
            ActionType = 0,
            Description,
            LongDescription,
            ObjectTypeCode,
            CreatedOn,
        }

        public enum InvoicesColumns
        {
            DocumentNo = 0,
            Policy,
            PeriodStartDate,
            PeriodEndDate,
            Amount,
            DueDate,
            Status
        }

        public enum CensusChangeRequestColumns
        {
            Name = 1,
            CreatedOn,
            FullName,
            ExternalId,
            PolicyName,
            RequestType,
            StatusCode
        }

        public int? start { get; set; } = 0;
        public int? length { get; set; } = 10;
        public int? status { get; set; }
        public int policyStatusCode { get; set; }
        public int? changeReqType { get; set; }
        public string searchValue { get; set; } = "";
        public int? genderCode { get; set; } = 0;
        public int? orderColumnNumber { get; set; }
        public string orderColumnOrientation { get; set; } = "asc";
        public Guid accountId { get; set; }
        public Guid policyId { get; set; }
        public int? insuredRole { get; set; }
        public Guid plan { get; set; }
    }
}
