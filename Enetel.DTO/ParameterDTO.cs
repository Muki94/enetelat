﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.DTO
{
    public class ParameterDTO
    {
        public string AccountName { get; set; }
        public string PersonFullName { get; set; }
        public string PersonId { get; set; }
        public FilterAttributesDTO.DocumentTypes FileType { get; set; }
        public string FolderPath { get; set; }
        public Stream FileMemoryStream { get; set; }
        public string FileName { get; set; }
        public string FileMEMEType { get; set; }
    }
}
