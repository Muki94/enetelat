﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Enetel.DTO
{
    public class CensusLineDTO
    {
        public Guid Id { get; set; }
        public string traversedpath { get; set; }

        [DisplayName("Modified On")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ModifiedOn { get; set; }

        [DisplayName("Date Joined")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_DateJoined { get; set; }
        public bool? ene_Complete { get; set; }
        public string ene_CensusLinenumber { get; set; }
        public int? ene_Ageatenrollment { get; set; }
        public int? ene_Age { get; set; }
        public bool? ene_ABRMismatch { get; set; }

        [DisplayName("ABR Change Rate")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_ABRChangeDate { get; set; }

        [DisplayName("Created On")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? CreatedOn { get; set; }
        public int? ImportSequenceNumber { get; set; }
        public decimal? ExchangeRate { get; set; }
        public decimal? ene_SumInsured { get; set; }

        public string ene_SumInsuredCurrency { get; set; }
        public Guid? ene_SumInsuredCurrencyId { get; set; }
    
        public string ene_SalaryDescription { get; set; }
        public decimal? ene_Salary { get; set; }

        public Guid? ene_SalaryCurrencyId { get; set; }
        public string ene_SalaryCurrency { get; set; }

        public string ene_ExternalId { get; set; }

        [DisplayName("DOB In This Year Now")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_DOBinthisyearNow { get; set; }

        [DisplayName("DOB In This Year")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_DOBinthisyear { get; set; }
        public int? ene_DiffInDaysNow { get; set; }
        public int? ene_DiffInDays { get; set; }
        public string ene_Description { get; set; }

        [DisplayName("Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_DateofBirth { get; set; }

        [DisplayName("Date Left")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? ene_DateLeft { get; set; }
        public bool? ene_ResetRateStatus { get; set; }
        public bool? ene_RatesMismatch { get; set; }
        public decimal? ene_Rate { get; set; }
        public decimal? ene_PreviousRate { get; set; }
        public string ene_Invoiceschedulingrulesetting { get; set; }
        public string ene_InsuredFullName { get; set; }
        public string ene_InsuredCategoryforCover { get; set; }
        public decimal? ene_InitialRate { get; set; }
        public string ene_Policy { get; set; }
        public Guid? ene_PolicyId { get; set; }
        public string ene_PaymentResponsible { get; set; }
        public string ene_ReportingResponsible { get; set; }
        public Guid? ene_PaymentResponsibleId { get; set; }
        public Guid? ene_ReportingResponsibleId { get; set; }
        public string ene_Plan { get; set; }
        public Guid? ene_PlanId { get; set; }
        public string ene_Insured { get; set; }
        public Guid? ene_InsuredId { get; set; }
        public string ene_PersonalAccount { get; set; }
        public Guid? ene_PersonalAccountId { get; set; }
        public string ene_Gender { get; set; }
        public int? ene_GenderValue { get; set; }
        public string ene_Citizenship { get; set; }
        public Guid? ene_CitizenshipId { get; set; }
        public string ene_ResidenceCountry { get; set; }
        public Guid? ene_ResidenceCountryId { get; set; }
        public string ene_InsuredExternalID { get; set; }
        public Guid? ene_InsuredExternalIDId { get; set; }
        public string ene_InsuredRole { get; set; }
        public int? ene_InsuredRoleValue { get; set; }
        public string ene_uniqueid { get; set; }

        public string ene_PrincipalName { get; set; }
        public Guid? ene_PrindipalId { get; set; }
        public string ene_PrincipalLogicalName { get; set; }

        public string ene_MembershipCard { get; set; }
        public Guid? ene_MembershipCardId { get; set; }
        public int statecode { get; set; }
    }
}
