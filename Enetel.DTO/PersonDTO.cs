﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.DTO
{
    public class PersonDTO
    {
        public Guid Id { get; set; }
        public string ExternalId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string FullName
        {
            get
            {
                return FirstName + " " + LastName;
            }
        }

        public int? GenderCode { get; set; }
        public string Gender { get; set; }

        [DisplayName("Date Of Birth")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? DateOfBirth { get; set; }

        public int? YearOfBirth { get; set; }
        public Guid? CitizenshipId { get; set; }
        public string Citizenship { get; set; }
        public Guid? ResidenceCountryId { get; set; }
        public string ResidenceCountry { get; set; }
        public Guid? ParentCustomerId { get; set; }
        public string ParentCustomerEntityName { get; set; }

        public Guid? AccountId { get; set; }
        public string AccountName { get; set; }
        public string ParentCustomerName { get; set; }
        public bool? documentTypeId { get; set; }
        public bool? documentTypeHealthQuestionnaire { get; set; }
        public bool? documentTypeTextDocument { get; set; }
        public bool? documentTypeOther { get; set; }
    }
}
