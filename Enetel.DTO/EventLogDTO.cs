﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Enetel.DTO
{
    public class EventLogDTO
    {
        public Guid Id { get; set; }
        public string ene_RecordID { get; set; }
        public string ene_Description { get; set; }
        public string ene_LongDescription { get; set; }
        public int? ene_ObjectTypeCode { get; set; }
        public string ene_ActionType { get; set; }
        public Guid? ene_B2BPortalUserID { get; set; }
        public string ene_B2BPortalUser { get; set; }

        [DisplayName("Created On")]
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}", ApplyFormatInEditMode = true, ConvertEmptyStringToNull = true, HtmlEncode = true, NullDisplayText = "")]
        [DataType(DataType.Date)]
        public DateTime? CreatedOn { get; set; }
        public string CreatedBy { get; set; }
        public Guid? CreatedById { get; set; }
    }
}
