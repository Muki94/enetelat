﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.DTO
{
    public class FileDTO
    {
        public string FileName { get; set; }
        public string FileType { get; set; }
        public Stream FileStream { get; set; }
    }
}
