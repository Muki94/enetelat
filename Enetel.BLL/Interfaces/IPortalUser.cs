﻿using Enetel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Interfaces
{
    public interface IPortalUser : IBaseService<PortalUserDTO>
    {
        List<PortalUserDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters);

        bool CheckExistingUser(string username);

        void Terminate(PortalUserDTO user);

        PortalUserDTO GetLoginRecord(string _username, string _password);

        bool CheckUserMailExistsForCompany(string ene_Email, Guid companyId);
    }
}
