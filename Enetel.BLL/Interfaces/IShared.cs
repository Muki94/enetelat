﻿using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Interfaces
{
    public interface IShared
    {
        dynamic GetCountries(string name = null);

        List<KeyValuePair<int?, string>> GetListFromOptionSet(string entityName, string fieldName);

        List<KeyValuePair<Guid, string>> GetCurrencies();

        Task<List<KeyValuePair<int, string>>> GetEnumerationListAsync<T>();

        int GetNumberOfRecordsForEntity(IOrganizationService service, string entityName, string filter);

        Task<List<KeyValuePair<int, string>>> GetChangeRequestTypesAsync();
    }
}
