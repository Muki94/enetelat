﻿using Enetel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Enetel.BLL.Interfaces
{
    public interface IInvoice : IBaseService<InvoiceDTO>
    {
        List<InvoiceDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters);
    }
}