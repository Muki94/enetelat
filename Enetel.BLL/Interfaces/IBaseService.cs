﻿using System;
using System.Collections.Generic;

namespace Enetel.BLL.Interfaces
{
    public interface IBaseService<T>
    {
        List<T> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10);

        T GetRecord(Guid id);

        Guid EditRecord(T element);

        Guid AddNewRecord(T element);
    }
}
