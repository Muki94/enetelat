﻿using Enetel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Interfaces
{
    public interface IPolicy : IBaseService<PolicyDTO>
    {
        List<PolicyDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters);
        List<PolicyDTO> GetRecords();
        KeyValuePair<string, int>? GetPolicyStatus(Guid policyId);
        int GetInsuranceType(Guid Id);
    }
}
