﻿using Enetel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Interfaces
{
    public interface IPlan: IBaseService<PlanDTO>
    {
        List<PlanDTO> GetPlansForPolicy(Guid policyId);
    }
}
