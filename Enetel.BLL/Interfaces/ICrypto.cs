﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Interfaces
{
    public interface ICrypto
    {
        byte[] GenerateSalt(int saltByteSize = 24);

        byte[] ComputeHash(string password, byte[] salt, int iterations = 10101, int hashByteSize = 24);

        bool VerifyPassword(string password, byte[] salt, byte[] passwordHash);

        bool AreHashesEqual(byte[] firstHash, byte[] secondHash);
    }
}
