﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;

namespace Enetel.BLL.Interfaces
{
    public interface ICensusChangeReq : IBaseService<ChangeRequestDTO>
    {
        bool Validate(System.Web.Mvc.ModelStateDictionary ModelState, ChangeRequestDTO changeRequest);
        ChangeRequestDTO GenerateTemplateCensusChangeReq(PolicyDTO model);
        ChangeRequestDTO GenerateTemplateCensusChangeReqFromCensusLine(CensusLineDTO model);

        Guid AddNewRecordEditRequest(ChangeRequestDTO changeRequest);

        Guid AddNewRecordAddRequest(ChangeRequestDTO changeRequest);

        Guid AddNewRecordTerminateRequest(ChangeRequestDTO changeRequest, CensusLineDTO censusLine);

        bool ChangeRequestExists(Guid policyId, string firstname = null, string lastname = null, DateTime? dateOfBirth = null);
        bool ChangeRequestExists(Guid policyId);

        ChangeRequestDTO GenerateCensusChangeReqFromCensusLine(CensusLineDTO model);

        dynamic GetPlans(Guid? policyid = null);

        dynamic GetCountries();

        EntityCollection GetContactsData(string fullname = null, bool? includeCitizenShip = false, bool? includeresidenceCountry = false, Guid? policyId = null);

        dynamic GetContacts(string fullname = null, bool? includeCitizenShip = false, bool? includeresidenceCountry = false, bool? includeCompany = false, Guid? policyId = null);
     
        EntityCollection GetAccountsData(string name = null);

        dynamic GetAccounts(string name = null);
        dynamic GetInsuredCustomers(string fullname = null, Guid? policyId = null);
        dynamic GetPaymentResponsibleCustomers(string fullname = null, Guid? policyId = null);

        dynamic GetPrincipalCustomers(string fullname = null, Guid? policyId = null);

        List<ChangeRequestDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters);
    }
}
