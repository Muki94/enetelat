﻿using Enetel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Interfaces
{
    public interface IPerson : IBaseService<PersonDTO>
    {
        List<PersonDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters);
        bool PersonExists(string externalId);
        bool PersonExists(string firstname, string lastname, int yearOfBirth);
        void AddDocument(Guid personId, FilterAttributesDTO.DocumentTypes fileType);
    }
}
