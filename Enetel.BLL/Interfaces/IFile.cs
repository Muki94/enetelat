﻿using Enetel.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Interfaces
{
    public interface IFile
    {
        FileDTO GetFileFromSharepoint(ParameterDTO parameterDTO);
        bool UploadFileToSharepoint(ParameterDTO parameterDTO);
    }
}
