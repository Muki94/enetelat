﻿using System.Collections.Generic;
using Enetel.DAL.Models;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;
using static Enetel.DTO.PolicyDTO;

namespace Enetel.BLL.Mappers
{
    public static class PolicyMapper
    {

        public static PolicyDTO MapPolicyToPolicyDTO(this ene_policy model)
        {
            return new PolicyDTO()
            {
                Id = model.Id,
                ene_InternalPolicyName = model.ene_InternalPolicyName,
                ene_policynumber = model.ene_policynumber,
                ene_ExternalID = model.ene_ExternalID,
                ene_AdditionalIndividualUnderwriting = model.ene_AdditionalIndividualUnderwriting,
                ene_Allcensuslinesactive = model.ene_Allcensuslinesactive,
                ene_Allcensuslinesactive_Date = model.ene_Allcensuslinesactive_Date,
                ene_Allcensuslinesactive_State = model.ene_Allcensuslinesactive_State,
                ene_ClaimsFund = model.ene_ClaimsFund,
                ene_Complete = model.ene_Complete,
                ene_ExternalPolicyNo = model.ene_ExternalPolicyNo,
                ene_GroupSize = model.ene_GroupSize,
                ene_GroupSize_Date = model.ene_GroupSize_Date,
                ene_GroupSize_State = model.ene_GroupSize_State,
                ene_GroupSizeDiscountIfAny = model.ene_GroupSizeDiscountIfAny,
                ene_GroupsizeManual = model.ene_GroupsizeManual,
                ene_GroupUnderwritingDiscountOrSurchargeIfAny = model.ene_GroupUnderwritingDiscountOrSurchargeIfAny,
                ene_MandatoryGroup = model.ene_MandatoryGroup,
                ene_NumberofDraftCensusLines = model.ene_NumberofDraftCensusLines,
                ene_NumberofDraftCensusLines_Date = model.ene_NumberofDraftCensusLines_Date,
                ene_NumberofDraftCensusLines_State = model.ene_NumberofDraftCensusLines_State,
                ModifiedOn = model.ModifiedOn,
                CreatedBy = model.CreatedBy.Name,
                CreatedById = model.CreatedBy.Id,
                ModifiedBy = model.ModifiedBy.Name,
                ModifiedById = model.ModifiedBy.Id,
                ene_PolicyPeriodNo = model.ene_PolicyPeriodNo,
                ene_Premium = model.ene_Premium,
                ene_Premium_Date = model.ene_Premium_Date,
                ene_Premium_State = model.ene_Premium_State,
                ene_Premiumratechanges = model.ene_Premiumratechanges,
                ene_ReadyforInvoicing = model.ene_ReadyforInvoicing,
                ene_ResetRateStatus = model.ene_ResetRateStatus,
                ene_ValidFrom = model.ene_ValidFrom,
                ene_StartDate = model.ene_StartDate,
                ene_EndDate = model.ene_EndDate,
                CreatedOn = model.CreatedOn,
                ene_ContractingCurrency = model.ene_ContractingCurrency?.Id,
                ene_PolicyHolder = model.ene_Policyholder?.Id,
                ene_insuranceType = model.ene_InsuranceType != null ? (InsuranceType?)model.ene_InsuranceType.Value : null,
                ene_policyStatus = model.statuscode != null ? model.statuscode.ToString() : null,
                ene_policyStatusCode = model.statuscode != null ? (int?)model.statuscode.Value : null,
                tmp_insuranceType = model.ene_InsuranceType != null ? ((InsuranceType?)model.ene_InsuranceType.Value).Value.ToString() : null,
            };
        }

        public static List<PolicyDTO> MapPolicyToPolicyDTO(this List<ene_policy> models)
        {
            List<PolicyDTO> policies = new List<PolicyDTO>();

            foreach (var model in models)
                policies.Add(model.MapPolicyToPolicyDTO());

            return policies;
        }

        public static ene_policy MapPolicyDTOToPolicy(this PolicyDTO model)
        {
            return new ene_policy()
            {
                Id = model.Id,
                ene_InternalPolicyName = model.ene_InternalPolicyName,
                ene_policynumber = model.ene_policynumber,
                ene_ExternalID = model.ene_ExternalID,
                ene_AdditionalIndividualUnderwriting = model.ene_AdditionalIndividualUnderwriting,
                ene_ClaimsFund = model.ene_ClaimsFund,
                ene_Complete = model.ene_Complete,
                ene_ExternalPolicyNo = model.ene_ExternalPolicyNo,
                ene_GroupSizeDiscountIfAny = model.ene_GroupSizeDiscountIfAny,
                ene_GroupsizeManual = model.ene_GroupsizeManual,
                ene_GroupUnderwritingDiscountOrSurchargeIfAny = model.ene_GroupUnderwritingDiscountOrSurchargeIfAny,
                ene_MandatoryGroup = model.ene_MandatoryGroup,
                ene_PolicyPeriodNo = model.ene_PolicyPeriodNo,
                ene_Premiumratechanges = model.ene_Premiumratechanges,
                ene_ReadyforInvoicing = model.ene_ReadyforInvoicing,
                ene_ResetRateStatus = model.ene_ResetRateStatus,
                ene_ValidFrom = model.ene_ValidFrom,
                ene_StartDate = model.ene_StartDate,
                ene_EndDate = model.ene_EndDate,
                ene_InsuranceType = model.ene_insuranceType != null ? new OptionSetValue((int)model.ene_insuranceType) : null
            };
        }

        public static List<ene_policy> MapPolicyDTOToPolicy(this List<PolicyDTO> models)
        {
            List<ene_policy> policies = new List<ene_policy>();

            foreach (var model in models)
                policies.Add(model.MapPolicyDTOToPolicy());

            return policies;
        }
    }
}