﻿using Enetel.DAL.Models;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Mappers
{
    public static class InvoiceMapper
    {
        public static InvoiceDTO MapInvoiceToInvoiceDTO(this ene_Invoice model)
        {
            return new InvoiceDTO()
            {
                Id = model.Id,
                AmountForPay = model.ene_AmountForPay,
                Archived = model.ene_Archived,
                CalculationTimeStamp = model.ene_CalculationTimeStamp,
                CreatedOn = model.CreatedOn,
                CurrencyId = model.ene_Currency?.Id,
                Currency = model.ene_Currency?.Name,
                DocumentDate = model.ene_DocumentDate,
                DocumentNo = model.ene_DocumentNo,
                DocumentTemplateId = model.ene_DocumentTemplate?.Id,
                DocumentTemplate = model.ene_DocumentTemplate?.Name,
                DocumentType = model.ene_DocumentType != null ? (DocumentType?)model.ene_DocumentType?.Value : null,
                DueDate = model.ene_DueDate,
                PaymentResponsibleId = model.ene_PaymentResponsible?.Id,
                PaymentResponsible = model.ene_PaymentResponsible?.Name,
                PeriodEndDate = model.ene_PeriodEndDate,
                PeriodStartDate = model.ene_PeriodStartDate,
                PolicyId = model.ene_Policy?.Id,
                Policy = model.ene_Policy?.Name,
                StatusReason = model.statuscode != null ? model.statuscode.ToString() : null,
                StatusReasonCode = model.statuscode != null ? (int?)model.statuscode.Value : null,
            };
        }

        public static List<InvoiceDTO> MapInvoiceToInvoiceDTO(this List<ene_Invoice> models)
        {
            List<InvoiceDTO> invoices = new List<InvoiceDTO>();

            foreach (ene_Invoice model in models)
                invoices.Add(model.MapInvoiceToInvoiceDTO());

            return invoices;
        }

        public static ene_Invoice MapInvoiceToInvoiceDTO(this InvoiceDTO model)
        {
            return new ene_Invoice()
            {
                Id = model.Id,
                ene_AmountForPay = model.AmountForPay,
                ene_Archived = model.Archived,
                ene_CalculationTimeStamp = model.CalculationTimeStamp,
                ene_Currency = model.CurrencyId != null ? new EntityReference(TransactionCurrency.EntityLogicalName, (Guid)model.CurrencyId) : null,
                ene_DocumentDate = model.DocumentDate,
                ene_DocumentNo = model.DocumentNo,
                ene_DocumentTemplate = model.DocumentTemplateId != null ? new EntityReference(ene_ReportList.EntityLogicalName, (Guid)model.DocumentTemplateId) : null,
                ene_DocumentType = model.DocumentType != null ? new OptionSetValue((int)model.DocumentType.Value) : null,
                ene_DueDate = model.DueDate,
                ene_PaymentResponsible = model.PaymentResponsibleId != null ? new EntityReference("account", (Guid)model.PaymentResponsibleId) : null,
                ene_PeriodEndDate = model.PeriodEndDate,
                ene_PeriodStartDate = model.PeriodStartDate,
                ene_Policy = model.PolicyId != null ? new EntityReference(ene_policy.EntityLogicalName, (Guid)model.PolicyId) : null,
                statuscode = model.StatusReasonCode != null ? (ene_invoice_statuscode?)model.StatusReasonCode : null
            };
        }

        public static List<ene_Invoice> MapInvoiceToInvoiceDTO(this List<InvoiceDTO> models)
        {
            List<ene_Invoice> invoices = new List<ene_Invoice>();

            foreach (InvoiceDTO model in models)
                invoices.Add(model.MapInvoiceToInvoiceDTO());

            return invoices;
        }
    }
}
