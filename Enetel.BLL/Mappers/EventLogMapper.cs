﻿using Enetel.DAL.Models;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;
using System.Collections.Generic;

namespace Enetel.BLL.Mappers
{
    public static class EventLogMapper
    {
        public static EventLogDTO MapEventLogToEventLogDTO(this ene_B2BPortalEventLog model)
        {
            return new EventLogDTO()
            {
                Id=model.Id,
                ene_RecordID = model.ene_RecordID,
                ene_Description= model.ene_Description,
                ene_LongDescription= model.ene_LongDescription,
                ene_ActionType= model.ene_ActionType,
                ene_ObjectTypeCode= model.ene_ObjectTypeCode,
                ene_B2BPortalUser= model.ene_B2BPortalUserID?.Name,
                ene_B2BPortalUserID= model.ene_B2BPortalUserID?.Id,
                CreatedOn = (model.CreatedOn != null ? model.CreatedOn.Value : model.CreatedOn),
                CreatedById = model.CreatedBy?.Id,
                CreatedBy = model.CreatedBy?.Name,
            };
        }

        public static List<EventLogDTO> MapEventLogToEventLogDTO(this List<ene_B2BPortalEventLog> models)
        {
            List<EventLogDTO> eventLogs = new List<EventLogDTO>();

            foreach (var model in models)
                eventLogs.Add(model.MapEventLogToEventLogDTO());

            return eventLogs;
        }

        public static ene_B2BPortalEventLog MapEventLogDTOToEventLog(this EventLogDTO model)
        {
            return new ene_B2BPortalEventLog()
            {
                Id = model.Id,
                ene_RecordID = model.ene_RecordID,
                ene_Description = model.ene_Description,
                ene_LongDescription = model.ene_LongDescription,
                ene_ActionType = model.ene_ActionType,
                ene_ObjectTypeCode = model.ene_ObjectTypeCode,
                ene_B2BPortalUserID = model.ene_B2BPortalUserID != null ? new EntityReference(ene_B2BPortalUser.EntityLogicalName, model.ene_B2BPortalUserID.Value) : null,
                OverriddenCreatedOn = (model.CreatedOn != null ? model.CreatedOn.Value : model.CreatedOn)
            };
        }

        public static List<ene_B2BPortalEventLog> MapEventLogDTOToEventLog(this List<EventLogDTO> models)
        {
            List<ene_B2BPortalEventLog> eventLogs = new List<ene_B2BPortalEventLog>();

            foreach (var model in models)
                eventLogs.Add(model.MapEventLogDTOToEventLog());

            return eventLogs;
        }
    }
}
