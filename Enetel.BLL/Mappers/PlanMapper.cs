﻿using System.Collections.Generic;
using Enetel.DAL.Models;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;
using static Enetel.DTO.PlanDTO;

namespace Enetel.BLL.Mappers
{
    public static class PlanMapper
    {
        public static PlanDTO MapPlanToPlanDTO(this ene_plan model)
        {
            return new PlanDTO()
            {
                Id = model.ene_planId,
                PlanName = model.ene_name ?? "" + " / " + model.ene_Plannumber ?? ""
            };
        }

        public static List<PlanDTO> MapPlanToPlanDTO(this List<ene_plan> models)
        {
            List<PlanDTO> policies = new List<PlanDTO>();

            foreach (var model in models)
                policies.Add(model.MapPlanToPlanDTO());

            return policies;
        }

        public static ene_plan MapPlanDTOToPlan(this PlanDTO model)
        {
            return new ene_plan()
            {
                ene_planId = model.Id
            };
        }

        public static List<ene_plan> MapPlanDTOToPlan(this List<PlanDTO> models)
        {
            List<ene_plan> policies = new List<ene_plan>();

            foreach (var model in models)
                policies.Add(model.MapPlanDTOToPlan());

            return policies;
        }
    }
}