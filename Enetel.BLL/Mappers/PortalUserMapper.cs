﻿using System.Collections.Generic;
using Enetel.DAL.Models;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;

namespace Enetel.BLL.Mappers
{
    public static class PortalUserMapper
    {
        public static PortalUserDTO MapPortalUserToPortalUserDTO(this ene_B2BPortalUser model)
        {
            return new PortalUserDTO() {
                Id = model.Id,
                ene_LastName = model.ene_LastName,
                ene_FirstName = model.ene_FirstName,
                ene_Email = model.ene_Email,
                ene_PhoneNumber = model.ene_PhoneNumber,
                ene_name = model.ene_name,
                ene_Password = model.ene_Password,
                ene_Admin = (bool)model.ene_Admin,
                ene_Account = model.ene_Account?.Id,
                ene_AccountLogicalName = model.ene_Account?.LogicalName,
                CreatedOn = (model.CreatedOn != null ? model.CreatedOn.Value : model.CreatedOn),
                ModifiedOn = (model.ModifiedOn != null ? model.ModifiedOn.Value : model.ModifiedOn),
                CreatedById = model.CreatedBy?.Id,
                ModifiedById = model.ModifiedBy?.Id,
                CreatedBy = model.CreatedBy?.Name,
                ModifiedBy = model.ModifiedBy?.Name,
                statuscode = model.statuscode.ToString(),
                statecode = model.statecode.ToString(),
                statuscodeNumber = (int)model.statuscode,
                statecodeNumber = (int)model.statecode,
                Salt = model.ene_Salt,
                canAccessInvoices = model.ene_CanAccessInvoices != null ? (bool)model.ene_CanAccessInvoices : false
            };
        }

        public static List<PortalUserDTO> MapPortalUserToPortalUserDTO(this List<ene_B2BPortalUser> models)
        {
            List<PortalUserDTO> portalUsers = new List<PortalUserDTO>();

            foreach (var model in models)
                portalUsers.Add(model.MapPortalUserToPortalUserDTO());

            return portalUsers;
        }

        public static ene_B2BPortalUser MapPortalUserDTOToPortalUser(this PortalUserDTO model)
        {
            return new ene_B2BPortalUser() {
                Id = model.Id,
                ene_LastName = model.ene_LastName,
                ene_FirstName = model.ene_FirstName,
                ene_Email = model.ene_Email,
                ene_PhoneNumber = model.ene_PhoneNumber,
                ene_name = model.ene_name,
                ene_Password = model.ene_Password,
                ene_Account = new EntityReference(ene_B2BPortalUser.EntityLogicalName, model.ene_Account.Value),
                ene_Admin = (bool)model.ene_Admin,
                OverriddenCreatedOn = (model.CreatedOn != null ? model.CreatedOn.Value : model.CreatedOn),
                statuscode = (ene_b2bportaluser_statuscode)model.statuscodeNumber,
                statecode = (ene_B2BPortalUserState)model.statecodeNumber,
                ene_Salt = model.Salt,
                ene_CanAccessInvoices = model.canAccessInvoices
            };
        }

        public static List<ene_B2BPortalUser> MapPortalUserDTOToPortalUser(this List<PortalUserDTO> models)
        {
            List<ene_B2BPortalUser> portalUsers = new List<ene_B2BPortalUser>();

            foreach (var model in models)
                portalUsers.Add(model.MapPortalUserDTOToPortalUser());

            return portalUsers;
        }
    }
}