﻿using Enetel.DAL.Models;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Mappers
{
    public static class PersonMapper
    {
        public static PersonDTO MapPersonToPersonDTO(this Contact model)
        {
            return new PersonDTO()
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                ExternalId = model.ene_ExternalID,
                Gender = model.ene_Gender != null ? model.FormattedValues["ene_gender"] : null,
                GenderCode = model.ene_Gender != null ? (int?)model.ene_Gender.Value : null,
                DateOfBirth = model.ene_DateOfBirth,
                YearOfBirth = model.ene_YearofBirth,
                Citizenship = model.ene_Citizenship?.Name,
                CitizenshipId = model.ene_Citizenship?.Id,
                ResidenceCountry = model.ene_ResidenceCountry?.Name,
                ResidenceCountryId = model.ene_ResidenceCountry?.Id,
                ParentCustomerId = model.ParentCustomerId?.Id,
                ParentCustomerName = model.ParentCustomerId?.Name,
                ParentCustomerEntityName = model.ParentCustomerId?.LogicalName,
                AccountId = model.AccountId?.Id,
                AccountName = model.AccountId?.Name,
                documentTypeHealthQuestionnaire = model.ene_DocumentTypeHealthQuestionnaire,
                documentTypeId = model.ene_DocumentTypeID,
                documentTypeOther = model.ene_DocumentTypeOther,
                documentTypeTextDocument = model.ene_DocumentTypeTextDocument,
            };
        }

        public static List<PersonDTO> MapPersonToPersonDTO(this List<Contact> models)
        {
            List<PersonDTO> portalUsers = new List<PersonDTO>();

            foreach (var model in models)
                portalUsers.Add(model.MapPersonToPersonDTO());

            return portalUsers;
        }

        public static Contact MapPersonDTOToPerson(this PersonDTO model)
        {
            return new Contact()
            {
                Id = model.Id,
                FirstName = model.FirstName,
                LastName = model.LastName,
                ene_ExternalID = model.ExternalId,
                ene_Gender = model.GenderCode != null ? new Microsoft.Xrm.Sdk.OptionSetValue(model.GenderCode.Value) : null,
                ene_DateOfBirth = model.DateOfBirth,
                ene_YearofBirth = model.YearOfBirth,
                ene_Citizenship = model.CitizenshipId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_country", model.CitizenshipId.Value) : null,
                ene_ResidenceCountry = model.ResidenceCountryId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_country", model.ResidenceCountryId.Value) : null,
                ParentCustomerId = (model.ParentCustomerId != null ? (model.ParentCustomerEntityName == Contact.EntityLogicalName ? new Microsoft.Xrm.Sdk.EntityReference(Contact.EntityLogicalName, model.ParentCustomerId.Value)
                                                                                                                                  : new Microsoft.Xrm.Sdk.EntityReference(Account.EntityLogicalName, model.ParentCustomerId.Value))
                                                                   : null),
                ene_DocumentTypeHealthQuestionnaire = model.documentTypeHealthQuestionnaire,
                ene_DocumentTypeID = model.documentTypeId,
                ene_DocumentTypeTextDocument = model.documentTypeTextDocument,
                ene_DocumentTypeOther = model.documentTypeOther
            };
        }

        public static List<Contact> MapPersonDTOToPerson(this List<PersonDTO> models)
        {
            List<Contact> portalUsers = new List<Contact>();

            foreach (var model in models)
                portalUsers.Add(model.MapPersonDTOToPerson());

            return portalUsers;
        }
    }
}
