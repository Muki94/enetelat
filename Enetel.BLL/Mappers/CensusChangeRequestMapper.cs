﻿using System.Collections.Generic;
using Enetel.DAL.Models;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;
using System;
using Enetel.BLL.Helpers;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using System.Linq;
using Enetel.Helper;

namespace Enetel.BLL.Mappers
{
    public static class CensusChangeRequestMapper
    {
        private static string GetOptionSetTextUsingValue(string entityName, string fieldName, int optionSetValue)
        {
            IOrganizationService _service = OrganizationService.GetOrganizationServiceInstance();
            var attReq = new RetrieveAttributeRequest();
            attReq.EntityLogicalName = entityName;
            attReq.LogicalName = fieldName;
            attReq.RetrieveAsIfPublished = true;
            var attResponse = (RetrieveAttributeResponse)_service.Execute(attReq);
            var attMetadata = (EnumAttributeMetadata)attResponse.AttributeMetadata;

            return attMetadata.OptionSet.Options.Where(x => x.Value == optionSetValue).FirstOrDefault().Label.UserLocalizedLabel.Label;
        }

        public static ChangeRequestDTO MapCensusChangeRequestToCensusChangeRequestDTO(this ene_censuschangerequest model)
        {
            return new ChangeRequestDTO()
            {
                Citizenship = model.ene_Citizenship?.Id,
                tmp_CitizenshipName = model.ene_Citizenship?.Name,
                ResidenceCountry = model.ene_ResidenceCountry?.Id,
                tmp_ResidenceCountryName = model.ene_ResidenceCountry?.Name,
                DateLeft = model.ene_DateLeft,
                externalID = model.ene_ExternalID,
                DateofBirth = model.ene_DateofBirth,
                FirstName = model.ene_FirstName,
                FullName = model.ene_FirstName + " " + model.ene_LastName,
                Gender = model != null && model.ene_Gender != null ? (Gender?)model.ene_Gender.Value : null,
                GenderName = model != null && model.ene_Gender != null ? ((Gender?)model.ene_Gender.Value).Value.ToString() : "",
                Id = model.Id,
                InsuredRole = model != null && model.ene_InsuredRole != null ? (InsuredRole?)model.ene_InsuredRole.Value : null,
                tmp_InsuredRole = model != null && model.ene_InsuredRole != null ? ((InsuredRole?)model.ene_InsuredRole.Value).Value.ToString() : null,
                LastName = model.ene_LastName,
                Name = model.ene_name,
                PaymentResponsible = model.ene_PaymentResponsible?.Id,
                PersonsAccountId = (model.ene_PersonsAccount != null ? (Guid?)model.ene_PersonsAccount.Id : null),
                PersonsAccount = (model.ene_PersonsAccount != null ? model.ene_PersonsAccount.Name : ""),
                Plan = model.ene_Plan?.Id,
                tmp_PlanName = model.ene_Plan?.Name,
                Policy = model.ene_Policy?.Id,
                tmp_PolicyName = model.ene_Policy?.Name,
                ReportingResponsible = model.ene_ReportingResponsible?.Id,
                Salary = model.ene_Salary,
                SalaryCurrencyId = model.ene_SalaryCurrency?.Id,
                SalaryCurrency = model.ene_SalaryCurrency?.Name,
                DateJoined = model.ene_DateJoined,
                SumInsured = model.ene_SumInsured,
                SumInsuredCurrency = model.ene_SumInsuredCurrency?.Name,
                SumInsuredCurrencyId = model.ene_SumInsuredCurrency?.Id,
                CreatedOn = model.CreatedOn,
                RequestTypeNumber = model.ene_RequestType.Value,
                RequestType = model.FormattedValues["ene_requesttype"],
                StatusCodeNumber = (int)model.statuscode,
                StatusCode = model.FormattedValues["statuscode"],
                Remarks = model.ene_Remarks,
                RejectionReason = model.ene_RejectionReasons != null ? GetOptionSetTextUsingValue(ene_censuschangerequest.EntityLogicalName, "ene_rejectionreasons", model.ene_RejectionReasons.Value) : "",
                RejectionReasonCode = model.ene_RejectionReasons != null ? (int?)model.ene_RejectionReasons.Value : null,
                RejectedOtherReason = model.ene_RejectedOtherReason,
                tmp_PrincipalName = model.ene_Principal?.Name,
                PortalUser = model.ene_B2BPortalUser?.Id,
                tmp_PortalUserName = model.ene_B2BPortalUser?.Name,
                tmp_CensusLineName = model.ene_CensusLine?.Name,
                SalaryDescription = model.ene_SalaryDescription,
                tmp_PaymentResponsibleFullName = model.ene_PaymentResponsible?.Name,
                tmp_InsuredName = model.ene_Insured?.Name,
                tmp_ReportingResponsibleFullName = model.ene_ReportingResponsible?.Name,
                CensusLineId = model.ene_CensusLine != null ? model.ene_CensusLine.Id : new Guid(),
                Insured = model.ene_Insured?.Id,
                Principal = model.ene_Principal?.Id,
                YearOfBirth = model.ene_Yearofbirth,
                tmp_ShowSalary = false
            };
        }

        public static List<ChangeRequestDTO> MapCensusChangeRequestToCensusChangeRequestDTO(this List<ene_censuschangerequest> models)
        {
            List<ChangeRequestDTO> censusChangeRequests = new List<ChangeRequestDTO>();

            foreach (var model in models)
                censusChangeRequests.Add(model.MapCensusChangeRequestToCensusChangeRequestDTO());

            return censusChangeRequests;
        }

        public static ene_censuschangerequest MapCensusChangeRequestDTOToCensusChangeRequest(this ChangeRequestDTO model)
        {
            return new ene_censuschangerequest()
            {
                ene_Citizenship = (model.Citizenship != null ? new EntityReference("country", model.Citizenship.Value) : null),
                ene_DateLeft = model.DateLeft,
                ene_ExternalID = model.externalID,
                ene_DateofBirth = model.DateofBirth,
                ene_FirstName = model.FirstName,
                ene_Gender = (model.Gender != null ? new OptionSetValue((int)model.Gender) : null),
                ene_InsuredRole = (model.InsuredRole != null ? new OptionSetValue((int)model.InsuredRole) : null),
                ene_LastName = model.LastName,
                //ene_name = model.Name,
                ene_PersonsAccount = (model.PersonsAccountId != null ? new EntityReference("ene_personsaccount", model.PersonsAccountId.Value) : null),
                ene_PaymentResponsible = (model.PaymentResponsible != null ? new EntityReference("ene_paymentresponsible", model.PaymentResponsible.Value) : null),
                ene_Plan = (model.Plan != null ? new EntityReference("ene_plan", model.Plan.Value) : null),
                ene_Policy = (model.Policy != null ? new EntityReference(ene_policy.EntityLogicalName, model.Policy.Value) : null),
                ene_ReportingResponsible = (model.ReportingResponsible != null ? new EntityReference("ene_reportingresponsible", model.ReportingResponsible.Value) : null),
                ene_Salary = model.Salary,
                ene_SalaryCurrency = (model.SalaryCurrencyId != null ? new EntityReference(TransactionCurrency.EntityLogicalName, model.SalaryCurrencyId.Value) : null),
                ene_DateJoined = model.DateJoined,
                ene_SumInsured = model.SumInsured,
                ene_SumInsuredCurrency = (model.SumInsuredCurrencyId != null ? new EntityReference(TransactionCurrency.EntityLogicalName, model.SumInsuredCurrencyId.Value) : null),
                OverriddenCreatedOn = model.CreatedOn,
                ene_RequestType = (model.RequestTypeNumber != null ? new OptionSetValue(model.RequestTypeNumber.Value) : null),
                ene_Remarks = model.Remarks,
                ene_RejectionReasons = model.RejectionReasonCode != null ? new OptionSetValue(model.RejectionReasonCode.Value) : null,
                ene_RejectedOtherReason = model.RejectedOtherReason,
                ene_ResidenceCountry = model.ResidenceCountry != null? new EntityReference("country", (Guid)model.ResidenceCountry) : null,
                ene_Yearofbirth = model.YearOfBirth
            };
        }

        public static List<ene_censuschangerequest> MapCensusChangeRequestDTOToCensusChangeRequest(this List<ChangeRequestDTO> models)
        {
            List<ene_censuschangerequest> censusChangeRequests = new List<ene_censuschangerequest>();

            foreach (var model in models)
                censusChangeRequests.Add(model.MapCensusChangeRequestDTOToCensusChangeRequest());

            return censusChangeRequests;
        }
    }
}