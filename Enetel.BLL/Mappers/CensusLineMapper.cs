﻿using System;
using System.Collections.Generic;
using Enetel.DAL.Models;
using Enetel.DTO;

namespace Enetel.BLL.Mappers
{
    public static class CensusLineMapper
    {
        public static CensusLineDTO MapCensusLineToCensusLineDTO(this ene_censusline model)
        {
            return new CensusLineDTO() {
                Id = model.Id,
                traversedpath = model.traversedpath,
                ModifiedOn = model.ModifiedOn,
                ene_DateJoined = model.ene_DateJoined,
                ene_Complete = model.ene_Complete,
                ene_CensusLinenumber = model.ene_CensusLinenumber,
                ene_Ageatenrollment = model.ene_Ageatenrollment,
                ene_Age = model.ene_Age,
                ene_ABRMismatch = model.ene_ABRMismatch,
                CreatedOn = model.CreatedOn,
                ImportSequenceNumber = model.ImportSequenceNumber,
                ExchangeRate = model.ExchangeRate,
                ene_SumInsured = model.ene_SumInsured,
                ene_SumInsuredCurrencyId = model.ene_SumInsuredCurrency?.Id,
                ene_SumInsuredCurrency = model.ene_SumInsuredCurrency?.Name,
                ene_SalaryDescription = model.ene_SalaryDescription,
                ene_Salary = model.ene_Salary,
                ene_SalaryCurrencyId = model.ene_SalaryCurrency?.Id,
                ene_SalaryCurrency = model.ene_SalaryCurrency?.Name,
                ene_ExternalId = model.ene_UniqueID,
                ene_DOBinthisyearNow = model.ene_DOBinthisyearNow,
                ene_DOBinthisyear = model.ene_DOBinthisyear,
                ene_DiffInDays = model.ene_DiffInDays,
                ene_DiffInDaysNow = model.ene_DiffInDaysNow,
                ene_Description = model.ene_Description,
                ene_DateofBirth = model.ene_DateofBirth,
                ene_DateLeft = model.ene_DateLeft,
                ene_ResetRateStatus = model.ene_ResetRateStatus,
                ene_RatesMismatch = model.ene_RatesMismatch,
                ene_Rate = model.ene_Rate,
                //ene_PreviousRate = model.ene_PreviousRate,
                ene_Invoiceschedulingrulesetting = model.ene_Invoiceschedulingrulesetting,
                ene_InsuredFullName = model.ene_InsuredFullName,
                ene_InsuredCategoryforCover = model.ene_InsuredCategoryforCover,
                ene_InitialRate = model.ene_InitialRate,
                ene_Policy = model.ene_Policy?.Name,
                ene_PolicyId = model.ene_Policy?.Id,
                ene_PaymentResponsible = model.ene_PaymentResponsible?.Name,
                ene_PaymentResponsibleId = model.ene_PaymentResponsible?.Id,
                ene_ReportingResponsible = model.ene_ReportingResponsible?.Name,
                ene_ReportingResponsibleId = model.ene_ReportingResponsible?.Id,
                ene_Plan = model.ene_Plan?.Name,
                ene_PlanId = model.ene_Plan?.Id,
                ene_Insured = model.ene_Insured?.Name,
                ene_InsuredId = model.ene_Insured?.Id,
                ene_PersonalAccount = model.ene_PersonsAccount?.Name,
                ene_PersonalAccountId = model.ene_PersonsAccount?.Id,
                ene_Gender = model.FormattedValues.ContainsKey("ene_gender") ? model.FormattedValues["ene_gender"] : "",
                ene_GenderValue = model.ene_Gender != null ? (int?)model.ene_Gender.Value : null,
                ene_Citizenship = model.ene_Citizenship?.Name,
                ene_CitizenshipId = model.ene_Citizenship?.Id,
                ene_ResidenceCountry = model.ene_ResidenceCountry?.Name,
                ene_ResidenceCountryId = model.ene_ResidenceCountry?.Id,
                ene_InsuredExternalID = model.ene_InsuredExternalID?.Name,
                ene_InsuredExternalIDId = model.ene_InsuredExternalID?.Id,
                ene_InsuredRole = model.FormattedValues.ContainsKey("ene_insuredrole") ? model.FormattedValues["ene_insuredrole"] : "",
                ene_InsuredRoleValue = model.ene_InsuredRole != null ? (int?)model.ene_InsuredRole.Value : null,
                //ene_ABRChangeDate = model.ene_ABRChangeDate,
                ene_uniqueid = model.ene_UniqueID,
                ene_PrindipalId = model.ene_Principal?.Id,
                ene_PrincipalName = model.ene_Principal?.Name,
                ene_PrincipalLogicalName = model.ene_Principal?.LogicalName,
                ene_MembershipCardId = model.ene_MembershipCardID?.Id,
                ene_MembershipCard = model.ene_MembershipCardID?.Name,
                statecode = (int)model.statecode.Value
            };
        }

        public static List<CensusLineDTO> MapCensusLineToCensusLineDTO(this List<ene_censusline> models)
        {
            List<CensusLineDTO> censusLines = new List<CensusLineDTO>();

            foreach (var model in models)
                censusLines.Add(model.MapCensusLineToCensusLineDTO());

            return censusLines;
        }

        public static ene_censusline MapCensusLineDTOToCensusLine(this CensusLineDTO model)
        {
            return new ene_censusline()
            {
                Id = model.Id,
                traversedpath = model.traversedpath,
                ene_DateJoined = model.ene_DateJoined,
                ene_Complete = model.ene_Complete,
                ene_CensusLinenumber = model.ene_CensusLinenumber,
                ImportSequenceNumber = model.ImportSequenceNumber,
                ene_SumInsured = model.ene_SumInsured,
                ene_SalaryDescription = model.ene_SalaryDescription,
                ene_Salary = model.ene_Salary,
                ene_ExternalId = model.ene_ExternalId,
                ene_Description = model.ene_Description,
                ene_DateofBirth = model.ene_DateofBirth,
                ene_DateLeft = model.ene_DateLeft,
                ene_ResetRateStatus = model.ene_ResetRateStatus,
                ene_RatesMismatch = model.ene_RatesMismatch,
                ene_Rate = model.ene_Rate,
                //ene_PreviousRate = model.ene_PreviousRate,
                ene_Invoiceschedulingrulesetting = model.ene_Invoiceschedulingrulesetting,
                ene_InsuredFullName = model.ene_InsuredFullName,
                ene_InsuredCategoryforCover = model.ene_InsuredCategoryforCover,
                ene_InitialRate = model.ene_InitialRate,
                ene_Policy = model.ene_PolicyId != null ? new Microsoft.Xrm.Sdk.EntityReference(ene_policy.EntityLogicalName, model.ene_PolicyId.Value) : null,
                ene_PaymentResponsible = model.ene_PaymentResponsibleId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_paymentresponsible", model.ene_PaymentResponsibleId.Value) : null,
                ene_ReportingResponsible = model.ene_ReportingResponsibleId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_reportingresponsible", model.ene_ReportingResponsibleId.Value) : null,
                ene_Plan = model.ene_PlanId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_plan", model.ene_PlanId.Value) : null,
                ene_Insured = model.ene_InsuredId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_insured", model.ene_InsuredId.Value) : null,
                ene_PersonsAccount = model.ene_PersonalAccountId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_personsaccount", model.ene_PersonalAccountId.Value) : null,
                ene_Gender = model.ene_GenderValue != null ? new Microsoft.Xrm.Sdk.OptionSetValue(model.ene_GenderValue.Value) : null,
                ene_Citizenship = model.ene_CitizenshipId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_citizenship", model.ene_CitizenshipId.Value) : null,
                ene_ResidenceCountry = model.ene_ResidenceCountryId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_residencecountry", model.ene_ResidenceCountryId.Value) : null,
                ene_InsuredExternalID = model.ene_InsuredExternalIDId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_insuredexternalid", model.ene_InsuredExternalIDId.Value) : null,
                ene_InsuredRole = model.ene_InsuredRoleValue != null ? new Microsoft.Xrm.Sdk.OptionSetValue(model.ene_InsuredRoleValue.Value) : null,
                //ene_ABRChangeDate = model.ene_ABRChangeDate,
                ene_UniqueID = model.ene_uniqueid,
                ene_Principal = model.ene_PrindipalId != null ? new Microsoft.Xrm.Sdk.EntityReference(model.ene_PrincipalLogicalName) : null,
                ene_MembershipCardID = model.ene_MembershipCardId != null ? new Microsoft.Xrm.Sdk.EntityReference("ene_membershipcardid", (Guid)model.ene_MembershipCardId) : null,
                ene_SalaryCurrency = model.ene_SalaryCurrencyId != null ? new Microsoft.Xrm.Sdk.EntityReference("transactioncurrency", (Guid)model.ene_SalaryCurrencyId) : null,
                ene_SumInsuredCurrency = model.ene_SumInsuredCurrencyId != null ? new Microsoft.Xrm.Sdk.EntityReference("transactioncurrency", (Guid)model.ene_SumInsuredCurrencyId) : null
            };
        }

        public static List<ene_censusline> MapCensusLineDTOToCensusLine(this List<CensusLineDTO> models)
        {
            List<ene_censusline> censusLines = new List<ene_censusline>();

            foreach (var model in models)
                censusLines.Add(model.MapCensusLineDTOToCensusLine());

            return censusLines;
        }
    }
}