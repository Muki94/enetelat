﻿using Microsoft.Xrm.Sdk;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace Enetel.BLL
{
    public class ResponseMsg
    {
        public bool SuccessResponse { get; set; }
        public string ResponseMessage { get; set; }
        public byte[] ResponseByte { get; set; }
    }

    public class CRMWebApi
    {
        private readonly string baseURL = @"https://1.dev.dhig.info/";
        private const string domain = @"enetel";
        private const string username = @"api@enetel.at";
        private const string password = "Vole2831";


        public ResponseMsg ws_GetContacts(string fullname = null)
        {
            ResponseMsg respMsg = new ResponseMsg();
            respMsg.SuccessResponse = false;
            try
            {


                HttpClient _client = new HttpClient(/*new HttpClientHandler()
                {
                    Credentials = new NetworkCredential(username, password, domain)
                }*/);

                //Base url of the CRM For e.g. http://servername:port/orgname
                _client.BaseAddress = new Uri(baseURL);

                string req_url = "api/data/v9.0/contacts";

                string filter = "";
                if (!string.IsNullOrWhiteSpace(fullname))
                    filter += "contains(fullname,'" + fullname + "')";

                if (!string.IsNullOrWhiteSpace(filter))
                    req_url = req_url + "?$filter=" + filter;

                Console.WriteLine("URL: " + baseURL + req_url);


                HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, req_url);
                // request.Content = new StringContent("");
                // request.Content.Headers.ContentType = MediaTypeHeaderValue.Parse("application/json");
                string encoded = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(username + ":" + password));
                request.Headers.Add("Authorization", "Basics " + encoded);

                request.Headers.Add("OData-MaxVersion", "4.0");
                request.Headers.Add("OData-Version", "4.0");
                request.Headers.Add("Accept", "application/json");
                //Send the HttpRequest
                Task<HttpResponseMessage> taskHttpResponsemessage = Task.Run((Func<Task<HttpResponseMessage>>)(async () =>
                {
                    return await _client.SendAsync(request);
                }));

                Task.WaitAll(taskHttpResponsemessage);
                HttpResponseMessage _httpResponsemessage = taskHttpResponsemessage.Result;

                Task<string> resultTask = Task.Run<string>(async () =>
                {
                    return await _httpResponsemessage.Content.ReadAsStringAsync();
                });

                Task.WaitAll(resultTask);
                respMsg.ResponseMessage = resultTask.Result;

                if (_httpResponsemessage.IsSuccessStatusCode)
                {
                    respMsg.SuccessResponse = true;
                }
                else
                {
                    throw new InvalidPluginExecutionException("ws_GetContacts problem. Status code " + ((int)_httpResponsemessage.StatusCode).ToString() +
                        ". Response: " + _httpResponsemessage.ReasonPhrase + ", " + respMsg.ResponseMessage);
                }


            }
            catch (Exception ex)
            {
                respMsg.ResponseMessage = ex.Message;
                throw new InvalidPluginExecutionException(ex.Message);
            }

            return respMsg;
        }



    }
}
