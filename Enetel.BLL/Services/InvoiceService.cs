﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.BLL.Mappers;
using Enetel.DAL.Models;
using Enetel.DTO;
using Enetel.Helper;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Services
{
    public class InvoiceService : IInvoice
    {
        private IOrganizationService _service;
        private IShared _sharedService;

        public InvoiceService(IShared sharedService)
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
            _sharedService = sharedService;

        }

        public Guid AddNewRecord(InvoiceDTO element)
        {
            throw new NotImplementedException();
        }

        public Guid EditRecord(InvoiceDTO element)
        {
            throw new NotImplementedException();
        }

        public InvoiceDTO GetRecord(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<InvoiceDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                string policyCondition = dataTableFilters.policyId != null && dataTableFilters.policyId != default(Guid) ? $"<condition attribute = 'ene_policy' operator= 'eq' value = '{dataTableFilters.policyId}'/>" : "";

                string filter = $@"<filter type='and'>
                                    <condition attribute = 'ene_paymentresponsible' operator= 'eq' value = '{dataTableFilters.accountId}'/>
                                    <condition attribute = 'statecode' operator= 'eq' value = '{0}'/>
                                    <condition attribute = 'statuscode' operator= 'eq' value = '{1}'/>
                                    <condition attribute = 'ene_visibleinb2bportal' operator= 'eq' value = '{1}'/>
                                    {policyCondition}
                                   </filter>";

                numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service, ene_Invoice.EntityLogicalName, filter);

                var invoices = crm.ene_InvoiceSet.Where(x => x.ene_PaymentResponsible.Id == dataTableFilters.accountId &&
                                                             x.statuscode == ene_invoice_statuscode.Created &&
                                                             x.statecode == ene_InvoiceState.Active &&
                                                             x.ene_VisibleInB2BPortal == true)
                                                 .AsQueryable();

                if (dataTableFilters.policyId != null && dataTableFilters.policyId != default(Guid))
                    invoices = invoices.Where(x => x.ene_Policy.Id == dataTableFilters.policyId);

                switch ((FilterAttributesDTO.InvoicesColumns)dataTableFilters.orderColumnNumber)
                {
                    case FilterAttributesDTO.InvoicesColumns.DocumentNo:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            invoices = invoices.OrderByDescending(x => x.ene_DocumentNo);
                        else
                            invoices = invoices.OrderBy(x => x.ene_DocumentNo);
                        break;
                    case FilterAttributesDTO.InvoicesColumns.Policy:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            invoices = invoices.OrderByDescending(x => x.ene_Policy);
                        else
                            invoices = invoices.OrderBy(x => x.ene_Policy);
                        break;
                    case FilterAttributesDTO.InvoicesColumns.PeriodStartDate:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            invoices = invoices.OrderByDescending(x => x.ene_PeriodStartDate);
                        else
                            invoices = invoices.OrderBy(x => x.ene_PeriodStartDate);
                        break;
                    case FilterAttributesDTO.InvoicesColumns.PeriodEndDate:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            invoices = invoices.OrderByDescending(x => x.ene_PeriodEndDate);
                        else
                            invoices = invoices.OrderBy(x => x.ene_PeriodEndDate);
                        break;
                    case FilterAttributesDTO.InvoicesColumns.Amount:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            invoices = invoices.OrderByDescending(x => x.ene_AmountForPay);
                        else
                            invoices = invoices.OrderBy(x => x.ene_AmountForPay);
                        break;
                    case FilterAttributesDTO.InvoicesColumns.DueDate:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            invoices = invoices.OrderByDescending(x => x.ene_DueDate);
                        else
                            invoices = invoices.OrderBy(x => x.ene_DueDate);
                        break;
                    case FilterAttributesDTO.InvoicesColumns.Status:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            invoices = invoices.OrderByDescending(x => x.statuscode);
                        else
                            invoices = invoices.OrderBy(x => x.statuscode);
                        break;
                    default:
                        invoices = invoices.OrderByDescending(x => x.CreatedOn);
                        break;
                }

                return invoices.Skip(dataTableFilters.start.Value)
                               .Take(dataTableFilters.length.Value)
                               .ToList()
                               .MapInvoiceToInvoiceDTO();
            }
        }

        public List<InvoiceDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10)
        {
            throw new NotImplementedException();
        }
    }
}
