﻿using Enetel.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;
using Enetel.DAL.Models;
using Enetel.Helper;
using Enetel.BLL.Mappers;

namespace Enetel.BLL.Services
{
    public class PlanService : IPlan
    {
        public PlanService()
        {
        }

        public Guid AddNewRecord(PlanDTO element)
        {
            throw new NotImplementedException();
        }

        public Guid EditRecord(PlanDTO element)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                throw new NotImplementedException(); 
            }
        }

        public List<PlanDTO> GetPlansForPolicy(Guid policyId)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                var plans = (from x in crm.CreateQuery("ene_ene_plan_ene_policy")
                            join pl in crm.ene_planSet on x.GetAttributeValue<Guid?>("ene_planid") equals pl.ene_planId
                        where x.GetAttributeValue<Guid?>("ene_policyid") == policyId
                        select pl as ene_plan).ToList();

                return plans.MapPlanToPlanDTO();
            }
        }

        public PlanDTO GetRecord(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<PlanDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10)
        {
            throw new NotImplementedException();
        }
    }
}
