﻿using Enetel.BLL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Enetel.DTO;
using Enetel.Helper;
using Enetel.BLL.Helpers;
using Microsoft.Xrm.Sdk;
using Enetel.DAL.Models;
using Enetel.BLL.Mappers;
using System.Globalization;

namespace Enetel.BLL.Services
{
    public class PersonService : IPerson
    {
        private IOrganizationService _service;
        private IShared _sharedService;

        public PersonService(IShared sharedService)
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
            _sharedService = sharedService;
        }

        public void AddDocument(Guid personId, FilterAttributesDTO.DocumentTypes fileType)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                var person = crm.ContactSet.Where(x => x.Id == personId).FirstOrDefault();

                switch (fileType)
                {
                    case FilterAttributesDTO.DocumentTypes.ID:
                        person.ene_DocumentTypeID = true;
                        break;
                    case FilterAttributesDTO.DocumentTypes.Health_Questionnaire:
                        person.ene_DocumentTypeHealthQuestionnaire = true;
                        break;
                    case FilterAttributesDTO.DocumentTypes.Text_Document:
                        person.ene_DocumentTypeTextDocument = true;
                        break;
                    case FilterAttributesDTO.DocumentTypes.Other:
                        person.ene_DocumentTypeOther = true;
                        break;
                    default:
                        break;
                }

                if (!crm.IsAttached(person))
                    crm.Attach(person);

                crm.UpdateObject(person);
                crm.SaveChanges();
            }
        }

        public Guid AddNewRecord(PersonDTO model)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                var dbModel = model.MapPersonDTOToPerson();

                dbModel.ParentCustomerId = new EntityReference(Identity.PortalUserSession.ene_AccountLogicalName, Identity.PortalUserSession.ene_Account.Value);

                crm.AddObject(dbModel);
                crm.SaveChanges();

                return dbModel.Id;
            }
        }

        public Guid EditRecord(PersonDTO element)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                throw new NotImplementedException();
            }
        }

        public PersonDTO GetRecord(Guid id)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                return crm.ContactSet.Where(x => x.Id == id)
                                     .FirstOrDefault()
                                     .MapPersonToPersonDTO();
            }
        }

        public List<PersonDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10)
        {
            throw new NotImplementedException();
        }

        public List<PersonDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                //handle null values
                dataTableFilters.searchValue = dataTableFilters.searchValue == null ? "" : dataTableFilters.searchValue;
                dataTableFilters.genderCode = dataTableFilters.genderCode == null ? 0 : dataTableFilters.genderCode;

                var genderCondition = dataTableFilters.genderCode != 0 ? $"<condition attribute = 'ene_gender' operator= 'eq' value = '{dataTableFilters.genderCode}'/>" 
                                                                    : "";
                string filter = $@"<filter type='and'>
                                    <filter type='or'>
                                        <condition attribute = 'fullname' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                        <condition attribute = 'ene_externalid' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                        <condition attribute = 'ene_citizenshipname' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                        <condition attribute = 'ene_residencecountryname' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                    </filter>
                                    {genderCondition}
                                    <condition attribute = 'accountid' operator= 'eq' value = '{dataTableFilters.accountId}'/>
                                    <condition attribute = 'statecode' operator= 'eq' value = '{0}'/>
                                    <condition attribute = 'statuscode' operator= 'eq' value = '{1}'/>
                                   </filter>";

                numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service, Contact.EntityLogicalName.ToLower(), filter);

                var persons = crm.ContactSet.AsQueryable()
                                            .Where(x => x.AccountId.Id == dataTableFilters.accountId &&
                                                        x.StatusCode == contact_statuscode.Active &&
                                                        x.StateCode == ContactState.Active)
                                            .ToList();

                if (dataTableFilters.genderCode != 0)
                    persons = persons.Where(x => x.GetAttributeValue<OptionSetValue>("ene_gender").Value == dataTableFilters.genderCode).ToList();

                persons = persons.Where(x => (x.FirstName != null && x.FirstName != "" && 
                                              (String.Compare(x.FirstName, dataTableFilters.searchValue, CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) == 0 )) ||
                                              (x.LastName != null && x.LastName != "" && 
                                              (String.Compare(x.LastName, dataTableFilters.searchValue, CultureInfo.CurrentCulture, CompareOptions.IgnoreNonSpace | CompareOptions.IgnoreCase) == 0 )) ||
                                              (x.ene_ExternalID != null && x.ene_ExternalID.Contains(dataTableFilters.searchValue)) ||
                                              (x.ene_Citizenship != null && x.ene_Citizenship.Name.ToLower().Contains(dataTableFilters.searchValue.ToLower())) ||
                                              (x.ene_ResidenceCountry != null && x.ene_ResidenceCountry.Name.ToLower().Contains(dataTableFilters.searchValue.ToLower())))
                                 .ToList();

                switch ((FilterAttributesDTO.PersonsColumns)dataTableFilters.orderColumnNumber)
                {
                    case FilterAttributesDTO.PersonsColumns.FullName:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            persons = persons.OrderByDescending(x => x.FullName).ToList();
                        else
                            persons = persons.OrderBy(x => x.FullName).ToList();
                        break;
                    case FilterAttributesDTO.PersonsColumns.ExternalId:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            persons = persons.OrderByDescending(x => x.ene_ExternalID).ToList();
                        else
                            persons = persons.OrderBy(x => x.ene_ExternalID).ToList();
                        break;
                    case FilterAttributesDTO.PersonsColumns.Gender:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            persons = persons.OrderByDescending(x => x.ene_Gender.Value).ToList();
                        else
                            persons = persons.OrderBy(x => x.ene_Gender.Value).ToList();
                        break;
                    case FilterAttributesDTO.PersonsColumns.DateOfBirth:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            persons = persons.OrderByDescending(x => x.ene_DateOfBirth).ToList();
                        else
                            persons = persons.OrderBy(x => x.ene_DateOfBirth).ToList();
                        break;
                    case FilterAttributesDTO.PersonsColumns.Citizenship:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            persons = persons.OrderByDescending(x => x.ene_Citizenship?.Name).ToList();
                        else
                            persons = persons.OrderBy(x => x.ene_Citizenship?.Name).ToList();
                        break;
                    case FilterAttributesDTO.PersonsColumns.ResidenceCountry:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            persons = persons.OrderByDescending(x => x.ene_ResidenceCountry?.Name).ToList();
                        else
                            persons = persons.OrderBy(x => x.ene_ResidenceCountry?.Name).ToList();
                        break;
                    default:
                        persons = persons.OrderByDescending(x => x.CreatedOn).ToList();
                        break;
                }

                return persons.Skip(dataTableFilters.start.Value)
                              .Take(dataTableFilters.length.Value)
                              .ToList()
                              .MapPersonToPersonDTO();                                
            }
        }

        public List<PersonDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10, Guid? ene_Account = null)
        {
            throw new NotImplementedException();
        }

        public bool PersonExists(string externalId)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                return crm.ContactSet.Where(x => x.ene_ExternalID == externalId &&
                                                 x.AccountId.Id == Identity.PortalUserSession.ene_Account.Value)
                                     .FirstOrDefault() != null;
            }
        }

        public bool PersonExists(string firstname, string lastname, int yearOfBirth)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                Contact c = crm.ContactSet.Where(x => x.FirstName == firstname &&
                                                 x.LastName == lastname &&
                                                 (x.ene_YearofBirth == yearOfBirth) &&
                                                 x.AccountId.Id == Identity.PortalUserSession.ene_Account.Value)
                                     .FirstOrDefault();

                if (c == null)
                {
                    List<Contact> cons = crm.ContactSet.Where(x => x.FirstName == firstname &&
                                                                    x.LastName == lastname &&
                                                                    x.AccountId.Id == Identity.PortalUserSession.ene_Account.Value)
                                                        .ToList();

                    if (cons != null)
                    {
                        foreach (var cc in cons)
                        {
                            if (cc.ene_DateOfBirth.Value.ToLocalTime().Year == yearOfBirth)
                            {
                                return true;
                            }
                        }
                    }
                }
                else return true;

                return false;
            }
        }
    }
}
