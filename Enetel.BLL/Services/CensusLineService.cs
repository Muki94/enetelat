﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.DAL.Models;
using Enetel.DTO;
using Enetel.Helper;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using Enetel.BLL.Mappers;

namespace Enetel.BLL.Services
{
    public class CensusLineService : ICensusLine
    {
        private CrmEntitiesContext crm;
        private IOrganizationService _service;
        private IShared _sharedService;

        public CensusLineService(IShared sharedService)
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
            crm = OrganizationService.GetCrmEntitiesContextInstance(_service);
            _sharedService = sharedService;
        }

        public CensusLineDTO GetRecord(Guid id)
        {
            return crm.ene_censuslineSet.Where(x => x.Id == id)
                                        .FirstOrDefault()
                                        .MapCensusLineToCensusLineDTO();
        }

        public Guid EditRecord(CensusLineDTO element)
        {
            throw new NotImplementedException();
        }

        public Guid AddNewRecord(CensusLineDTO element)
        {
            throw new NotImplementedException();
        }

        public void DeleteRecord(Guid id)
        {
            throw new NotImplementedException();
        }

        public List<CensusLineDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage)
        {
            throw new NotImplementedException();
        }

        public List<CensusLineDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            dataTableFilters.searchValue = dataTableFilters.searchValue == null ? "" : dataTableFilters.searchValue;
            dataTableFilters.status = dataTableFilters.status == null ? 1 : dataTableFilters.status;
            var insuredRoleFilter = dataTableFilters.insuredRole == null ? "" : $"<condition attribute = 'ene_insuredrole' operator= 'eq' value = '{dataTableFilters.insuredRole}'/>";
            var planFilter = dataTableFilters.plan == Guid.Empty ? "" : $"<condition attribute = 'ene_plan' operator= 'eq' value = '{dataTableFilters.plan}'/>";

            string filter = $@"<filter type='and'>
                                    <filter type='or'>
                                        <condition attribute = 'ene_insuredfullname' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                        <condition attribute = 'ene_uniqueid' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                    </filter>
                                    <condition attribute = 'ene_policy' operator= 'eq' value = '{dataTableFilters.policyId}'/>
                                    <condition attribute = 'statuscode' operator= 'eq' value = '{dataTableFilters.status}'/>
                                    {insuredRoleFilter}
                                    {planFilter}
                                   </filter>";

            numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service, ene_censusline.EntityLogicalName.ToLower(), filter);

            var censusLines = crm.ene_censuslineSet.Where(x => x.ene_Policy.Id == dataTableFilters.policyId &&
                                                               x.statuscode == (ene_censusline_statuscode)dataTableFilters.status);

            censusLines = censusLines.Where(x => x.ene_InsuredFullName.Contains(dataTableFilters.searchValue) ||
                                                 x.ene_UniqueID.Contains(dataTableFilters.searchValue));

            if (dataTableFilters.insuredRole != null)
                censusLines = censusLines.Where(x => x.ene_InsuredRole.Value == dataTableFilters.insuredRole.Value);

            if (dataTableFilters.plan != Guid.Empty)
                censusLines = censusLines.Where(x => x.ene_Plan.Id == dataTableFilters.plan);

            switch ((FilterAttributesDTO.CensusLinesColumns)dataTableFilters.orderColumnNumber)
            {
                case FilterAttributesDTO.CensusLinesColumns.CensusLineNumber:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderByDescending(x => x.ene_CensusLinenumber);
                    else
                        censusLines = censusLines.OrderBy(x => x.ene_CensusLinenumber);
                    break;
                case FilterAttributesDTO.CensusLinesColumns.FullName:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderByDescending(x => x.ene_InsuredFullName);
                    else
                        censusLines = censusLines.OrderBy(x => x.ene_InsuredFullName);
                    break;
                case FilterAttributesDTO.CensusLinesColumns.UniqueId:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderByDescending(x => x.ene_UniqueID);
                    else
                        censusLines = censusLines.OrderBy(x => x.ene_UniqueID);
                    break;
                case FilterAttributesDTO.CensusLinesColumns.InsuredRole:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderByDescending(x => x.ene_InsuredRole);
                    else
                        censusLines = censusLines.OrderBy(x => x.ene_InsuredRole);
                    break;
                case FilterAttributesDTO.CensusLinesColumns.Plan:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderByDescending(x => x.ene_Plan);
                    else
                        censusLines = censusLines.OrderBy(x => x.ene_Plan);
                    break;
                case FilterAttributesDTO.CensusLinesColumns.DateJoined:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderByDescending(x => x.ene_DateJoined);
                    else
                        censusLines = censusLines.OrderBy(x => x.ene_DateJoined);
                    break;
                case FilterAttributesDTO.CensusLinesColumns.Rate:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderByDescending(x => x.ene_Rate);
                    else
                        censusLines = censusLines.OrderBy(x => x.ene_Rate);
                    break;
                default:
                    if (dataTableFilters.orderColumnOrientation == "desc")
                        censusLines = censusLines.OrderBy(x => x.CreatedOn).OrderBy(x => x.ene_InsuredFullName);
                    else
                        censusLines = censusLines.OrderBy(x => x.CreatedOn).OrderBy(x => x.ene_InsuredFullName);
                    break;
            }

            return censusLines.Skip(dataTableFilters.start.Value)
                              .Take(dataTableFilters.length.Value)
                              .ToList()
                              .MapCensusLineToCensusLineDTO();
        }
    }
}
