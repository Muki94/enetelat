﻿using Enetel.BLL.Interfaces;
using Enetel.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Enetel.BLL.Services
{
    public class SharedService : IShared
    {
        IOrganizationService _service;

        public SharedService()
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
        }

        public dynamic GetCountries(string name = null)
        {
            string fetchxml = string.Format(@"  
                <fetch mapping='logical'>  
                    <entity name='ene_country'>   
                    <attribute name='ene_countryid'/>   
                    <attribute name='ene_name'/>  
                    <filter type='and'>   
                            <condition attribute='ene_name' operator='like' value='%{0}%'/>
                        </filter>  
                    </entity>   
                </fetch> ", name);

            return _service.RetrieveMultiple(new FetchExpression(fetchxml))
                            .Entities
                            .Select(x => new { id = x.GetAttributeValue<Guid?>("ene_countryid"), name = x.GetAttributeValue<string>("ene_name") ?? "" });
        }

        public List<KeyValuePair<int?, string>> GetListFromOptionSet(string entityName, string fieldName)
        {
            var attReq = new RetrieveAttributeRequest();
            attReq.EntityLogicalName = entityName;
            attReq.LogicalName = fieldName;
            attReq.RetrieveAsIfPublished = true;

            var attResponse = (RetrieveAttributeResponse)_service.Execute(attReq);
            var attMetadata = (EnumAttributeMetadata)attResponse.AttributeMetadata;

            return (from o in attMetadata.OptionSet.Options
                    select new KeyValuePair<int?, string>(o.Value, o.Label.UserLocalizedLabel.Label)).ToList();
        }

        public List<KeyValuePair<Guid, string>> GetCurrencies() {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                return crm.TransactionCurrencySet.Select(x => new KeyValuePair<Guid, string>(x.Id, $"{x.CurrencyName} {x.CurrencySymbol}"))
                                                 .ToList();
            }
        }

        public Task<List<KeyValuePair<int, string>>> GetEnumerationListAsync<T>()
        {
            return Task.Run(() =>
            {
                List<KeyValuePair<int, string>> enumerationList = new List<KeyValuePair<int, string>>();

                foreach (var name in Enum.GetNames(typeof(T)))
                    enumerationList.Add(new KeyValuePair<int, string>((int)Enum.Parse(typeof(T), name), name.Replace('_', ' ')));

                return enumerationList;
            });
        }

        public int GetNumberOfRecordsForEntity(IOrganizationService service, string entityName, string filter = "")
        {
            string fetchXMLQueryBuilder = $@"<fetch distinct='false' mapping='logical' aggregate='true'>   
                                                 <entity name='{entityName}'>   
                                                    <attribute name='{entityName}id' aggregate='count' alias='{entityName}_count'/>
                                                    {filter}
                                                 </entity>
                                             </fetch>";

            Entity entityRes = OrganizationService.GetOrganizationServiceInstance()
                                                  .RetrieveMultiple(new FetchExpression(fetchXMLQueryBuilder))
                                                  .Entities[0];

            return (Int32)((AliasedValue)entityRes[$"{entityName}_count"]).Value;
        }

        public Task<List<KeyValuePair<int, string>>> GetChangeRequestTypesAsync()
        {
            return Task.Run(() => {
                List<KeyValuePair<int, string>> types = new List<KeyValuePair<int, string>>();

                types.Add(new KeyValuePair<int, string>(200000000, "New"));
                types.Add(new KeyValuePair<int, string>(200000001, "Edit"));
                types.Add(new KeyValuePair<int, string>(200000002, "Revoke"));

                return types;
            });
        }
    }
}
