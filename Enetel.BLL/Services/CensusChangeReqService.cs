﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.BLL.Mappers;
using Enetel.DAL.Models;
using Enetel.DTO;
using Enetel.Helper;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Query;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web.ModelBinding;

namespace Enetel.BLL.Services
{
    public class CensusChangeReqService : ICensusChangeReq
    {
        private IOrganizationService _service;
        private ICensusLine _censusLineService;
        private IPolicy _policyService;
        private IShared _sharedService;

        public CensusChangeReqService(ICensusLine censusLineService, 
                                      IPolicy policyService, 
                                      IShared sharedService)
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
            _censusLineService = censusLineService;
            _policyService = policyService;
            _sharedService = sharedService;
        }

        public List<ChangeRequestDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {

                var statusCodeFilter = dataTableFilters.status != null ? $@"<condition attribute = 'statuscode' operator= 'eq' value = '{dataTableFilters.status}'/>" : "";
                var requestTypeFilter = dataTableFilters.changeReqType != null ? $@"<condition attribute = 'ene_requesttype' operator= 'eq' value = '{dataTableFilters.changeReqType}' />" : "";

                string filter = $@"<filter type='and'>
                                    <filter type='or'>
                                        <condition attribute = 'ene_firstname' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                        <condition attribute = 'ene_lastname' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                        <condition attribute = 'ene_name' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                        <condition attribute = 'ene_externalid' operator = 'like' value = '%{dataTableFilters.searchValue}%'/>
                                    </filter>
                                        <condition attribute = 'ene_personsaccount' operator= 'eq' value = '{dataTableFilters.accountId}'/>
                                        <condition attribute = 'ene_b2bportaluser' operator= 'not-null' />
                                        {statusCodeFilter}
                                        {requestTypeFilter}
                                  </filter>";

                numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service,ene_censuschangerequest.EntityLogicalName.ToLower(), filter);

                var censusChangeRequests = crm.ene_censuschangerequestSet.Where(x => x.ene_PersonsAccount.Id == Identity.PortalUserSession.ene_Account &&
                                                                                     x.ene_B2BPortalUser != null);

                if (!String.IsNullOrEmpty(dataTableFilters.searchValue))
                    censusChangeRequests = censusChangeRequests.Where(x => x.ene_FirstName.Contains(dataTableFilters.searchValue) ||
                                                                           x.ene_LastName.Contains(dataTableFilters.searchValue) ||
                                                                           x.ene_name.Contains(dataTableFilters.searchValue) ||
                                                                           x.ene_ExternalID == dataTableFilters.searchValue);

                if(dataTableFilters.changeReqType != null)
                    censusChangeRequests = censusChangeRequests.Where(x => x.ene_RequestType.Value == dataTableFilters.changeReqType);


                if (dataTableFilters.status != null)
                    censusChangeRequests = censusChangeRequests.Where(x => (int)x.statuscode.Value == dataTableFilters.status);

                switch ((FilterAttributesDTO.CensusChangeRequestColumns)dataTableFilters.orderColumnNumber)
                {
                    case FilterAttributesDTO.CensusChangeRequestColumns.Name:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            censusChangeRequests = censusChangeRequests.OrderByDescending(x => x.ene_name);
                        else
                            censusChangeRequests = censusChangeRequests.OrderBy(x => x.ene_name);
                        break;
                    case FilterAttributesDTO.CensusChangeRequestColumns.CreatedOn:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            censusChangeRequests = censusChangeRequests.OrderByDescending(x => x.CreatedOn);
                        else
                            censusChangeRequests = censusChangeRequests.OrderBy(x => x.CreatedOn);
                        break;
                    case FilterAttributesDTO.CensusChangeRequestColumns.FullName:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            censusChangeRequests = censusChangeRequests.OrderByDescending(x => x.ene_FirstName);
                        else
                            censusChangeRequests = censusChangeRequests.OrderBy(x => x.ene_FirstName);
                        break;
                    case FilterAttributesDTO.CensusChangeRequestColumns.ExternalId:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            censusChangeRequests = censusChangeRequests.OrderByDescending(x => x.ene_ExternalID);
                        else
                            censusChangeRequests = censusChangeRequests.OrderBy(x => x.ene_ExternalID);
                        break;
                    case FilterAttributesDTO.CensusChangeRequestColumns.PolicyName:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            censusChangeRequests = censusChangeRequests.OrderByDescending(x => x.ene_Policy);
                        else
                            censusChangeRequests = censusChangeRequests.OrderBy(x => x.ene_Policy);
                        break;
                    case FilterAttributesDTO.CensusChangeRequestColumns.RequestType:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            censusChangeRequests = censusChangeRequests.OrderByDescending(x => x.ene_RequestType);
                        else
                            censusChangeRequests = censusChangeRequests.OrderBy(x => x.ene_RequestType);
                        break;
                    case FilterAttributesDTO.CensusChangeRequestColumns.StatusCode:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            censusChangeRequests = censusChangeRequests.OrderByDescending(x => x.statuscode);
                        else
                            censusChangeRequests = censusChangeRequests.OrderBy(x => x.statuscode);
                        break;
                    default:
                        break;
                }

                return censusChangeRequests.Skip(dataTableFilters.start.Value)
                                           .Take(dataTableFilters.length.Value)
                                           .ToList()
                                           .MapCensusChangeRequestToCensusChangeRequestDTO();
            }
        }

        public dynamic GetPlans(Guid? policyid = null)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                try
                {
                    var query = from x in crm.CreateQuery("ene_ene_plan_ene_policy") 
                                join pl in crm.ene_planSet on x.GetAttributeValue<Guid?>("ene_planid") equals pl.ene_planId
                                where x.GetAttributeValue<Guid?>("ene_policyid") == policyid
                                select new
                                {
                                    id = pl.ene_planId,
                                    name = pl.ene_name ?? "" + " / " + pl.ene_Plannumber ?? ""
                                };
                    return query.ToList();
                }
                catch (Exception e)
                {
                    throw;
                }
            }
        }

        public dynamic GetCountries()
        {
            string fetchxml = @"  
                <fetch mapping='logical'>  
                    <entity name='ene_country'>   
                    <attribute name='ene_countryid'/>   
                    <attribute name='ene_name'/> 
                    </entity>   
                </fetch> ";
            return _service.RetrieveMultiple(new FetchExpression(fetchxml))
                .Entities.Select(x => new { id = x.GetAttributeValue<Guid?>("ene_countryid"), name = x.GetAttributeValue<string>("ene_name") ?? "" });
           
        }

        public EntityCollection GetContactsData(string fullname = null, bool? includeCitizenShip = false,
            bool? includeresidenceCountry = false, Guid? policyId=null)
        {

            string fetchxml = @"  
                   <fetch mapping='logical'>  
                     <entity name='contact'>   
                        <attribute name='contactid'/>   
                        <attribute name='fullname'/> 
                        <attribute name='firstname'/>
                        <attribute name='lastname'/>
                        <attribute name='ene_dateofbirth'/>  
                        <attribute name='ene_externalid'/> 
                        <attribute name='ene_gender'/> 
                        <attribute name='ene_citizenship'/> 
                        <attribute name='ene_residencecountry'/> 
                        <filter type='and'>   
                            <filter type='or'>
                                  <condition attribute='ene_externalid' operator='eq' value='{0}'/>
                                  <condition attribute='fullname' operator='like' value='%{0}%'/>  
                            </filter>
                           
                             <condition attribute='accountid' operator='eq' value='{1}' uitype='account'/>
                        </filter>
                        <link-entity name='account' from='accountid' to='accountid' alias='acc' link-type='outer'>
                                     <attribute name='accountid' alias='companyid' />                            
                                    <attribute name='name' alias='companyname' />
                        </link-entity>";

            if (policyId != null)
            {
                fetchxml = fetchxml + @"<link-entity name='ene_censusline' from='ene_insured' to='contactid' alias='cl1' link-type='inner'>
                                       <filter type='and'>   
                                         <condition attribute='statecode' operator='eq' value='0'/> 
                                         <condition attribute='ene_policy' operator='eq' value="+$"'{policyId.Value.ToString()}'/>"+@" 
                                    </filter>
                                    </link-entity>";
                
            }

            if (includeCitizenShip == true)
            {
                fetchxml = fetchxml + @"<link-entity name='ene_country' from='ene_countryid' to='ene_citizenship' alias='cnt' link-type='outer'>
                            <attribute name='ene_name' alias='citizenship_countryname' />
                        </link-entity>";
            }

            if (includeresidenceCountry == true)
            {
                fetchxml = fetchxml + @"<link-entity name='ene_country' from='ene_countryid' to='ene_residencecountry' alias='cnt2' link-type='outer'>
                            <attribute name='ene_name' alias='residencecountry_countryname' />
                        </link-entity>";
            }

            fetchxml = fetchxml + @"</entity>   
                </fetch>";

            EntityCollection entityCollection = _service.RetrieveMultiple(new FetchExpression(string.Format(fetchxml, fullname ?? "", 
                Identity.PortalUserSession.ene_Account.ToString() ?? "null")));

            return entityCollection;
        
        }

        public dynamic GetPaymentResponsibleCustomers(string fullname = null, Guid? policyId = null)
        {
            string fetchxml = @"  
                   <fetch mapping='logical'>  
                     <entity name='contact'>   
                        <attribute name='contactid'/>   
                        <attribute name='fullname'/> 
                        <attribute name='firstname'/>
                        <attribute name='lastname'/>
                        <attribute name='ene_dateofbirth'/>  
                        <attribute name='ene_externalid'/> 
                        <attribute name='ene_gender'/> 
                        <attribute name='ene_citizenship'/> 
                        <attribute name='ene_residencecountry'/> 
                        <filter type='and'>   
                            <filter type='or'>
                                  <condition attribute='ene_externalid' operator='eq' value='{0}'/>
                                  <condition attribute='fullname' operator='like' value='%{0}%'/>  
                            </filter>
                           
                        </filter>
                        <link-entity name='account' from='accountid' to='accountid' alias='acc' link-type='outer'>
                                     <attribute name='accountid' alias='companyid' />                            
                                    <attribute name='name' alias='companyname' />
                        </link-entity>";

            if (policyId != null)
            {
                fetchxml = fetchxml + @"<link-entity name='ene_censusline' from='ene_insured' to='contactid' alias='cl1' link-type='inner'>
                                       <filter type='and'>   
                                         <condition attribute='statecode' operator='eq' value='0'/> 
                                         <condition attribute='ene_policy' operator='eq' value=" + $"'{policyId.Value.ToString()}'/>" + @" 
                                    </filter>
                                    </link-entity>";

               

            }

            fetchxml = fetchxml + @"</entity>   
                </fetch>";


            EntityCollection entityCollection = _service.RetrieveMultiple(new FetchExpression(string.Format(fetchxml, fullname ?? "")));


            Entity policy_holder = GetPolicyHolder(fullname, policyId).Entities.FirstOrDefault();
            if (policy_holder != null)
                entityCollection.Entities.Add(policy_holder);

            return MappingCustomersResponse(entityCollection);

        }

        //public dynamic GetPrincipalCustomers(string fullname = null, Guid? policyId = null)
        //{
        //    string fetchxml = @"  
        //           <fetch mapping='logical' distinct='true'>  
        //             <entity name='contact'>   
        //                <attribute name='contactid'/>   
        //                <attribute name='fullname'/> 
        //                <attribute name='firstname'/>
        //                <attribute name='lastname'/>
        //                <attribute name='ene_dateofbirth'/>  
        //                <attribute name='ene_externalid'/> 
        //                <attribute name='ene_gender'/> 
        //                <attribute name='ene_citizenship'/> 
        //                <attribute name='ene_residencecountry'/>  
        //                <filter type='or'>
        //                        <condition attribute='ene_externalid' operator='eq' value='{0}'/>
        //                        <condition attribute='fullname' operator='like' value='%{0}%'/>  
        //                </filter>";

        //    if (policyId != null)
        //    {
        //        fetchxml = fetchxml + @"<link-entity name='ene_censusline' from='ene_insured' to='contactid' alias='cl1' link-type='inner'>
        //                               <filter type='and'>   
        //                                 <condition attribute='statuscode' operator='eq' value='1'/> 
        //                                 <condition attribute='ene_policy' operator='eq' value=" + $"'{policyId.Value.ToString()}'/>" + @" 
        //                            </filter>
        //                            </link-entity>";
        //    }
        //    fetchxml = fetchxml + @"</entity>   
        //        </fetch>";


        //    EntityCollection entityCollection = _service.RetrieveMultiple(new FetchExpression(string.Format(fetchxml, fullname ?? "")));


        //    return MappingCustomersResponse(entityCollection);

        //}
      
        private dynamic MappingCustomersResponse(EntityCollection entityCollection)
        {
            return entityCollection.Entities.Select(x => new
            {
                id = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<Guid?>("contactid") : x.GetAttributeValue<Guid?>("accountid"),
                name = x.LogicalName == Contact.EntityLogicalName ? ((x.GetAttributeValue<string>("fullname") ?? "")
              + " / Dob: " + (x.GetAttributeValue<DateTime?>("ene_dateofbirth")?.ConvertDateTimeToLocal().ToString("yyyy-MM-dd") ?? "")
              + " / Id: " + (x.GetAttributeValue<string>("ene_externalid") ?? "")) : (x.GetAttributeValue<string>("name") ?? ""),
                dateofbirth = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<DateTime?>("ene_dateofbirth")?.ConvertDateTimeToLocal().ToString("yyyy-MM-dd") : "",
                externalid = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<string>("ene_externalid") : "",
                gender = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<OptionSetValue>("ene_gender")?.Value : null,
                firstname = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<string>("firstname") : null,
                lastname = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<string>("lastname") : null,
                citizenship = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<EntityReference>("ene_citizenship")?.Id : null,
                citizenship_countryname = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<AliasedValue>("citizenship_countryname")?.Value : null,
                residencecountry = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<EntityReference>("ene_residencecountry")?.Id : null,
                residencecountry_countryname = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<AliasedValue>("residencecountry_countryname")?.Value : null,
                companyid = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<AliasedValue>("companyid")?.Value : null,
                companyname = x.LogicalName == Contact.EntityLogicalName ? x.GetAttributeValue<AliasedValue>("companyname")?.Value : null
            });
        }

        public dynamic GetContacts(string fullname = null, bool? includeCitizenShip = false,
            bool? includeresidenceCountry = false, bool? includeCompany = false, Guid? policyId = null)
        {

            EntityCollection entityCollection = GetContactsData(fullname, includeCitizenShip, includeresidenceCountry, policyId);

            if (includeCompany == true)
            {
                Entity person_account = GetAccountsData(fullname).Entities.FirstOrDefault();

                if (person_account != null && person_account.Id == Identity.PortalUserSession.ene_Account.Value)
                    entityCollection.Entities.Add(person_account);

            }

            return MappingCustomersResponse(entityCollection);
        }

        private EntityCollection FilterNotActiveCensusLieAndNotActiveChangeRequest(EntityCollection entityCollection, Guid policyId)
        {
            EntityCollection coll = new EntityCollection();
            foreach (Entity item in entityCollection.Entities)
            {
                string query1 = string.Format(@"<fetch mapping='logical' aggregate='true' version='1.0'>
                              <entity name='ene_censusline'>
                                <attribute name='ene_censuslineid' alias='COUNT_ene_censuslineid' aggregate='countcolumn' />
                                <filter>
                                  <condition attribute='ene_insured' operator='eq' value='{0}' />
                                  <condition attribute='ene_policy' operator='eq' value='{1}' />
                                  <condition attribute='statuscode' operator='eq' value='1' />
                                </filter>
                              </entity>
                            </fetch>", 
                            item.LogicalName == Contact.EntityLogicalName ? item.GetAttributeValue<Guid?>("contactid") : item.GetAttributeValue<Guid?>("accountid"),
                            policyId.ToString());

                string query2 = string.Format(@"<fetch mapping='logical' aggregate='true' version='1.0'>
                              <entity name='ene_censuschangerequest'>
                                <attribute name='ene_censuschangerequestid' alias='COUNT_ene_censuschangerequestid' aggregate='countcolumn' />
                                <filter>
                                  <condition attribute='ene_insured' operator='eq' value='{0}' />
                                   <condition attribute='ene_policy' operator='eq' value='{1}' />
                                  <condition attribute='statecode' operator='eq' value='0' />
                                </filter>
                              </entity>
                            </fetch>", item.LogicalName == Contact.EntityLogicalName ? item.GetAttributeValue<Guid?>("contactid") : item.GetAttributeValue<Guid?>("accountid"),
                            policyId.ToString());
               
                Entity e_qry1 = _service.RetrieveMultiple(new FetchExpression(query1)).Entities.First();
                int count_qry1 = ((int)((AliasedValue)e_qry1["COUNT_ene_censuslineid"]).Value);
              
                Entity e_qry2 = _service.RetrieveMultiple(new FetchExpression(query2)).Entities.First();
                int count_qry2 = ((int)((AliasedValue)e_qry2["COUNT_ene_censuschangerequestid"]).Value);

                if (count_qry1 == 0 && count_qry2 == 0)
                {
                    coll.Entities.Add(item);
                }

            }

            return coll;
        }
        public dynamic GetPrincipalCustomers(string fullname = null, Guid? policyId = null)
        {
            /*<filter type='or'>
                                <condition attribute='ene_externalid' operator='eq' value='{0}'/>
                                <condition attribute='fullname' operator='like' value='%{0}%'/>  
                        </filter>*/
            string fetchxml = @"  
                   <fetch mapping='logical' distinct='true'>  
                     <entity name='contact'>   
                        <attribute name='contactid'/>   
                        <attribute name='fullname'/> 
                        <attribute name='firstname'/>
                        <attribute name='lastname'/>
                        <attribute name='ene_dateofbirth'/>  
                        <attribute name='ene_externalid'/> 
                        <attribute name='ene_gender'/> 
                        <attribute name='ene_citizenship'/> 
                        <attribute name='ene_residencecountry'/>  
                        ";

            if (policyId != null)
            {
                fetchxml = fetchxml + @"<link-entity name='ene_censusline' from='ene_insured' to='contactid' alias='cl1' link-type='inner'>
                                        <filter type='and'>   
                                            <condition attribute='statuscode' operator='eq' value='1'/> 
                                            <condition attribute='ene_policy' operator='eq' value=" + $"'{policyId.Value.ToString()}'/>" + @" 
                                        </filter>
                                    </link-entity>";
            }
            fetchxml = fetchxml + @"</entity>   
                </fetch>";


            EntityCollection entityCollection = _service.RetrieveMultiple(new FetchExpression(fetchxml));


            return MappingCustomersResponse(entityCollection);

        }

        public dynamic GetInsuredCustomers(string fullname = null, Guid? policyId = null)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                Guid policyholder = crm.ene_policySet.First(x => x.ene_policyId == policyId).ene_Policyholder.Id;

                /* <!--<filter type='or'>
                                  <condition attribute='ene_externalid' operator='eq' value='{0}'/>
                                  <condition attribute='fullname' operator='like' value='%{0}%'/>  
                            </filter>-->    */
                string fetchxml = @"  
                   <fetch mapping='logical'>  
                     <entity name='contact'>   
                        <attribute name='contactid'/>   
                        <attribute name='fullname'/> 
                        <attribute name='firstname'/>
                        <attribute name='lastname'/>
                        <attribute name='ene_dateofbirth'/>  
                        <attribute name='ene_externalid'/> 
                        <attribute name='ene_gender'/> 
                        <attribute name='ene_citizenship'/> 
                        <attribute name='ene_residencecountry'/> 
                        <filter type='and'>   
                                             
                            <condition attribute='accountid' operator='eq' value='{0}' uitype='account'/>         
                        </filter>
                        <link-entity name='ene_country' from='ene_countryid' to='ene_citizenship' alias='acc2' link-type='inner'>
                                <attribute name='ene_name' alias='citizenship_countryname' />   
                        </link-entity>
                        <link-entity name='ene_country' from='ene_countryid' to='ene_residencecountry' alias='acc3' link-type='inner'>
                                <attribute name='ene_name' alias='residencecountry_countryname' />   
                        </link-entity>
                        <link-entity name='account' from='accountid' to='accountid' alias='acc' link-type='outer'>
                                <attribute name='accountid' alias='companyid' />                            
                                <attribute name='name' alias='companyname' />
                        </link-entity>

                        ";

               
                fetchxml = fetchxml + @"</entity>   
                </fetch>";
                
                EntityCollection entityCollection = _service.RetrieveMultiple(new FetchExpression(string.Format(fetchxml, /*fullname ?? "",*/ policyholder)));
                EntityCollection filteredEntColl = FilterNotActiveCensusLieAndNotActiveChangeRequest(entityCollection, policyId.Value);
                return MappingCustomersResponse(filteredEntColl);

            }

        }

        private EntityCollection GetPolicyHolder(string name, Guid? policyId)
        {


            string fetchxml = string.Format(@"  
                   <fetch mapping='logical'>  
                     <entity name='account'>   
                         <attribute name='accountid'/>   
                         <attribute name='name'/>   
                         <filter type='and'>   
                             <condition attribute='name' operator='like' value='%{0}%'/> 
                         </filter>  
                         
                        <link-entity name='ene_policy' from='ene_policyholder' to='accountid' alias='p' link-type='inner'>
                            <filter type='and'>   
                                <condition attribute='ene_policyid' operator='eq' value='{1}'/> 
                            </filter>  
                        </link-entity>

                     </entity>   
                   </fetch> ", name ?? "", policyId);

            return _service.RetrieveMultiple(new FetchExpression(fetchxml));

        }

        public EntityCollection GetAccountsData(string name = null)
        {

            string fetchxml = string.Format(@"  
                   <fetch mapping='logical'>  
                     <entity name='account'>   
                        <attribute name='accountid'/>   
                        <attribute name='name'/>   
                         <filter type='and'>   
                             <condition attribute='name' operator='like' value='%{0}%'/> 
                         </filter>  
                     </entity>   
                   </fetch> ", name ?? "");
            return _service.RetrieveMultiple(new FetchExpression(fetchxml));

        }

        public dynamic GetAccounts(string name = null)
        {
            return GetAccountsData(name)
                  .Entities.Select(x => new { id = x.GetAttributeValue<Guid?>("accountid"), name = x.GetAttributeValue<string>("name") });
        }

        public string GetCustomerName(Guid entityId)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
               
                Entity entity = entityId.GetCustomer(crm);

                if (entity.LogicalName == Contact.EntityLogicalName)
                {
                    var fullName = entity.GetAttributeValue<string>("fullname") ?? "";
                    var dateOfBirth = entity.GetAttributeValue<DateTime?>("ene_dateofbirth")?.ToString("yyyy-MM-dd") ?? "";
                    var externalId = entity.GetAttributeValue<string>("ene_externalid") ?? "";

                    return $@"{fullName} / Dob: {dateOfBirth} / Id: {externalId}";
                }
                else
                    return entity.GetAttributeValue<string>("name");
            }
        }

        public EntityReference GetCustomerEntityReference(Guid Id)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return new EntityReference(Id.GetCustomer(crm).LogicalName, Id);
            }
        }

        public ChangeRequestDTO GetRecord(Guid id)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return crm.ene_censuschangerequestSet.Where(x => x.Id == id)
                                                     .FirstOrDefault()
                                                     .MapCensusChangeRequestToCensusChangeRequestDTO();
            }
        }

        public Guid EditRecord(ChangeRequestDTO element)
        {
            throw new NotImplementedException();
        }

        public Guid AddNewRecord(ChangeRequestDTO cl)
        {
            throw new NotImplementedException();
        }

        public bool Validate(System.Web.Mvc.ModelStateDictionary ModelState, ChangeRequestDTO changeRequest)
        {
            bool result = true;
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                var policy = crm.ene_policySet.Where(x => x.ene_policyId == changeRequest.Policy).First();

                if (changeRequest.DateJoined < policy.ene_StartDate || changeRequest.DateJoined > policy.ene_EndDate)
                {
                    ModelState.AddModelError("DateJoined", "Date Joined must be between policy Start Date and policy End Date");
                    result = false;
                }
            }
           
            return result;
        }

        public Guid AddNewRecordEditRequest(ChangeRequestDTO changeRequest)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                CensusLineDTO censusLine = _censusLineService.GetRecord(changeRequest.CensusLineId);
                var dbModel = changeRequest.MapCensusChangeRequestDTOToCensusChangeRequest();
                
                dbModel.Id = Guid.NewGuid();
                dbModel.ene_SalaryDescription = changeRequest.SalaryDescription;
                dbModel.ene_CensusLine = new EntityReference(ene_censusline.EntityLogicalName, changeRequest.CensusLineId);//ZAVRSI POPUP
                dbModel.ene_Insured = censusLine.ene_InsuredId != null ? GetCustomerEntityReference(censusLine.ene_InsuredId.Value) : null;
                dbModel.ene_InsuredRole = censusLine.ene_InsuredRoleValue != null ? new OptionSetValue(censusLine.ene_InsuredRoleValue.Value) : null;
                dbModel.ene_Citizenship = censusLine.ene_CitizenshipId != null ? new EntityReference("ene_country", censusLine.ene_CitizenshipId.Value) : null;
                dbModel.ene_ResidenceCountry = censusLine.ene_ResidenceCountryId != null ? new EntityReference("ene_country", censusLine.ene_ResidenceCountryId.Value) : null; //--Lookup
                dbModel.ene_SumInsured = changeRequest.SumInsured;
                //dbModel.ene_name = changeRequest?.Name;
                dbModel.ene_PersonsAccount = censusLine.ene_PersonalAccountId != null ? new EntityReference("account", censusLine.ene_PersonalAccountId.Value) : null;
                dbModel.ene_DateofBirth = censusLine.ene_DateofBirth;

                if (changeRequest.Principal != null) {
                    Account acc = crm.AccountSet.Where(x => x.Id == (Guid)changeRequest.Principal).FirstOrDefault();
                    if (acc != null)
                    {
                        dbModel.ene_Principal = changeRequest.Principal != null ? new EntityReference(acc.LogicalName, (Guid)changeRequest.Principal) : null;
                    }
                    else
                    {
                        dbModel.ene_Principal = changeRequest.Principal != null ? new EntityReference(Contact.EntityLogicalName, (Guid)changeRequest.Principal) : null;
                    }
                }

                dbModel.ene_B2BPortalUser = new EntityReference("ene_b2bportaluser".ToLower(), Identity.PortalUserSession.Id);
                dbModel.ene_Policy = changeRequest.Policy != null ? new EntityReference("ene_policy".ToLower(), changeRequest.Policy.Value) : null;
                dbModel.ene_PaymentResponsible = censusLine.ene_PaymentResponsibleId != null ? GetCustomerEntityReference(censusLine.ene_PaymentResponsibleId.Value) : null;//Lookup
                dbModel.ene_ReportingResponsible = censusLine.ene_ReportingResponsibleId != null ? GetCustomerEntityReference(censusLine.ene_ReportingResponsibleId.Value) : null;//Lookup

                crm.AddObject(dbModel);
                crm.SaveChanges();

                return dbModel.Id;
            }
        }

        public Guid AddNewRecordAddRequest(ChangeRequestDTO changeRequest)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                ene_censuschangerequest ccr = new ene_censuschangerequest();

                ccr.Id = Guid.NewGuid();
                ccr.ene_ExternalID = changeRequest.externalID;
                ccr.ene_FirstName = changeRequest.FirstName;
                ccr.ene_LastName = changeRequest.LastName;
                ccr.ene_Gender = changeRequest.Gender != null ? new OptionSetValue((int)changeRequest.Gender) : null;
                ccr.ene_DateofBirth = changeRequest.DateofBirth;
                ccr.ene_Yearofbirth = changeRequest.YearOfBirth;
                ccr.ene_Citizenship = changeRequest.Citizenship != null ? new EntityReference("ene_country", changeRequest.Citizenship.Value) : null; //--Lookup
                ccr.ene_Insured = changeRequest.Insured != null ? GetCustomerEntityReference(changeRequest.Insured.Value) : null;
                ccr.ene_ResidenceCountry = changeRequest.ResidenceCountry != null ? new EntityReference("ene_country", changeRequest.ResidenceCountry.Value) : null; //--Lookup
                ccr.ene_Plan = changeRequest.Plan != null ? new EntityReference("ene_plan", changeRequest.Plan.Value) : null; //--Lookup
                ccr.ene_DateJoined = changeRequest.DateJoined;
                ccr.ene_DateLeft = changeRequest.DateLeft;
                ccr.ene_Salary = changeRequest.Salary;
                ccr.ene_SalaryCurrency = changeRequest.SalaryCurrencyId != null ? new EntityReference(TransactionCurrency.EntityLogicalName, changeRequest.SalaryCurrencyId.Value) : null;
                ccr.ene_SalaryDescription = changeRequest.SalaryDescription;
                ccr.ene_SumInsured = changeRequest.SumInsured;        
                ccr.ene_InsuredRole = changeRequest.InsuredRole != null ? new OptionSetValue((int)changeRequest.InsuredRole.Value) : null;
                ccr.ene_Principal = changeRequest.Principal != null ? new EntityReference(Contact.EntityLogicalName, changeRequest.Principal.Value) : null;
                ccr.ene_B2BPortalUser = new EntityReference("ene_b2bportaluser", Identity.PortalUserSession.Id);
                ccr.ene_PersonsAccount = new EntityReference(Account.EntityLogicalName, Identity.PortalUserSession.ene_Account.Value);
                ccr.ene_Policy = changeRequest.Policy != null ? new EntityReference(ene_policy.EntityLogicalName, changeRequest.Policy.Value) : null;
                ccr.ene_PaymentResponsible = changeRequest.PaymentResponsible != null ? GetCustomerEntityReference(changeRequest.PaymentResponsible.Value) : null;//Lookup
                ccr.ene_ReportingResponsible = changeRequest.ReportingResponsible != null ? GetCustomerEntityReference(changeRequest.ReportingResponsible.Value) : null;//Lookup
                ccr.ene_B2BPortalUser = new EntityReference(ene_B2BPortalUser.EntityLogicalName.ToLower(), Identity.PortalUserSession.Id);
                
                ccr.ene_Remarks = changeRequest.Remarks;

                crm.AddObject(ccr);
                crm.SaveChanges();
                return ccr.Id; 
            }
        }

        public Guid AddNewRecordTerminateRequest(ChangeRequestDTO changeRequest, CensusLineDTO censusLine)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                //CensusLineDTO censusLine = _censusLineService.GetRecord(changeRequest.CensusLineId);
                var dbModel = changeRequest.MapCensusChangeRequestDTOToCensusChangeRequest();

                dbModel.Id = Guid.NewGuid();
                dbModel.ene_ExternalID = censusLine.ene_ExternalId;
                dbModel.ene_SumInsuredCurrency = censusLine.ene_SumInsuredCurrencyId != null ? new EntityReference(TransactionCurrency.EntityLogicalName, (Guid)censusLine.ene_SumInsuredCurrencyId) : null;
                dbModel.ene_DateLeft = changeRequest.DateLeft;
                dbModel.ene_FirstName = censusLine.ene_InsuredFullName?.Split(' ')[0];
                dbModel.ene_LastName = censusLine.ene_InsuredFullName?.Split(' ')[1];
                dbModel.ene_Gender = censusLine.ene_GenderValue != null ? new OptionSetValue(censusLine.ene_GenderValue.Value) : null;
                dbModel.ene_DateofBirth = censusLine.ene_DateofBirth;
                dbModel.ene_Yearofbirth = censusLine.ene_DateofBirth?.Year;
                dbModel.ene_CensusLine = new EntityReference(ene_censusline.EntityLogicalName, changeRequest.CensusLineId);
                dbModel.ene_Insured = censusLine.ene_InsuredId != null ? GetCustomerEntityReference(censusLine.ene_InsuredId.Value) : null;
                dbModel.ene_InsuredRole = censusLine.ene_InsuredRoleValue != null ? new OptionSetValue(censusLine.ene_InsuredRoleValue.Value) : null;
                dbModel.ene_Plan = censusLine.ene_PlanId != null ? new EntityReference("ene_plan".ToLower(), censusLine.ene_PlanId.Value) : null; //--Lookup
                dbModel.ene_ExternalID = censusLine.ene_ExternalId;
                dbModel.ene_DateJoined = censusLine.ene_DateJoined;
                dbModel.ene_Citizenship = censusLine.ene_CitizenshipId != null ? new EntityReference("ene_country", censusLine.ene_CitizenshipId.Value) : null;
                dbModel.ene_ResidenceCountry = censusLine.ene_ResidenceCountryId != null ? new EntityReference("ene_country", censusLine.ene_ResidenceCountryId.Value) : null; //--Lookup
                dbModel.ene_Salary = censusLine.ene_Salary;
                dbModel.ene_SalaryCurrency = censusLine.ene_SalaryCurrencyId != null ? new EntityReference(TransactionCurrency.EntityLogicalName, censusLine.ene_SalaryCurrencyId.Value) : null;


                dbModel.ene_SumInsured = censusLine.ene_SumInsured;
                dbModel.ene_SalaryCurrency = (changeRequest.SalaryCurrencyId != null ? new EntityReference(TransactionCurrency.EntityLogicalName, changeRequest.SalaryCurrencyId.Value) : null);
                dbModel.ene_SalaryDescription = censusLine.ene_SalaryDescription;
                dbModel.ene_B2BPortalUser = new EntityReference(ene_B2BPortalUser.EntityLogicalName, Identity.PortalUserSession.Id);
                dbModel.ene_PersonsAccount = new EntityReference(Account.EntityLogicalName, Identity.PortalUserSession.ene_Account.Value);
                dbModel.ene_Policy = censusLine.ene_PolicyId != null ? new EntityReference(ene_policy.EntityLogicalName, censusLine.ene_PolicyId.Value) : null;
                dbModel.ene_PaymentResponsible = censusLine.ene_PaymentResponsibleId != null ? GetCustomerEntityReference(changeRequest.PaymentResponsible.Value) : null;//Lookup
                dbModel.ene_ReportingResponsible = censusLine.ene_ReportingResponsibleId != null ? GetCustomerEntityReference(changeRequest.ReportingResponsible.Value) : null;//Lookup
                //dbModel.ene_ThispersonisPaymentResponsible = censusLine.ene_InsuredId == censusLine.ene_PaymentResponsibleId ? true : false;
                //dbModel.ene_ThispersonisReportingResponsible = censusLine.ene_InsuredId == censusLine.ene_ReportingResponsibleId ? true : false;
                dbModel.statuscode = ene_censuschangerequest_statuscode.PendingApproval;

                crm.AddObject(dbModel);
                crm.SaveChanges();
                return dbModel.Id;
            }
        }

        public void DeleteRecord(Guid id)
        {
            throw new NotImplementedException();
        }

        public bool ChangeRequestExists(Guid id)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return crm.ene_censuschangerequestSet.Where(x => x.ene_PersonsAccount.Id == Identity.PortalUserSession.ene_Account.Value &&
                                                                 x.ene_CensusLine.Id == id &&
                                                                 x.statecode == ene_censuschangerequestState.Active)
                                                     .FirstOrDefault() != null;
            }
        }

        public ChangeRequestDTO GenerateCensusChangeReqFromCensusLine(CensusLineDTO model)
        {
            PolicyDTO policyDTO = _policyService.GetRecord(model.ene_PolicyId.Value);

            ChangeRequestDTO ccr = new ChangeRequestDTO();
            ccr.CensusLineId = model.Id;
            ccr.externalID = model.ene_ExternalId;
            ccr.FirstName = model.ene_InsuredFullName?.Split(' ')[0];
            ccr.LastName = model.ene_InsuredFullName?.Split(' ')[1];
            ccr.Gender = model.ene_Gender != null ? (Enetel.DTO.Gender?)model.ene_GenderValue : null;
            ccr.DateofBirth = model.ene_DateofBirth;
            ccr.YearOfBirth = model.ene_DateofBirth?.Year;
            ccr.Citizenship = model.ene_CitizenshipId;
            ccr.ResidenceCountry = model.ene_ResidenceCountryId;
            ccr.Plan = model.ene_PlanId;
            ccr.DateJoined = model.ene_DateJoined;
            ccr.DateLeft = model.ene_DateLeft;
            ccr.Salary = model.ene_Salary;
            ccr.SalaryCurrencyId = model.ene_SalaryCurrencyId;
            ccr.SalaryCurrency = model.ene_SalaryCurrency;
            ccr.Insured = model.ene_InsuredId;
            ccr.tmp_InsuredName = GetCustomerName(model.ene_InsuredId.Value);
            ccr.SumInsured = model.ene_SumInsured;
            ccr.SumInsuredCurrencyId = model.ene_SumInsuredCurrencyId;
            ccr.SumInsuredCurrency = model.ene_SumInsuredCurrency;
            ccr.SalaryDescription = model.ene_SalaryDescription;
            ccr.PersonsAccountId = Identity.PortalUserSession.ene_Account;
            ccr.PersonsAccount = model.ene_PersonalAccount;
            ccr.Policy = model.ene_PolicyId;
            ccr.tmp_PolicyName = policyDTO.ene_InternalPolicyName;
            ccr.tmp_PolicyNumber = policyDTO.ene_policynumber;
            ccr.PaymentResponsible = policyDTO.ene_PolicyHolder;
            ccr.tmp_PaymentResponsibleFullName = GetCustomerName(policyDTO.ene_PolicyHolder.Value);
            ccr.ReportingResponsible = model.ene_ReportingResponsibleId;
            ccr.tmp_ReportingResponsibleFullName = model.ene_ReportingResponsibleId != null ? GetCustomerName(model.ene_ReportingResponsibleId.Value) : null;
            ccr.Insured = model.ene_InsuredId;
            ccr.Principal = model.ene_PrindipalId;
          
            return ccr;
        }

        public ChangeRequestDTO GenerateTemplateCensusChangeReq(PolicyDTO model)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                ene_policy ene_Policy = crm.ene_policySet.First(x => x.ene_policyId == model.Id);
                return new ChangeRequestDTO()
                {
                    Policy = model.Id,
                    tmp_PolicyName = model.ene_InternalPolicyName,
                    tmp_PolicyNumber = model.ene_policynumber,
                    CensusLineId = model.Id,
                    PaymentResponsible = model.ene_PolicyHolder,
                    tmp_PaymentResponsibleFullName = model.ene_PolicyHolder != null ? GetCustomerName(model.ene_PolicyHolder.Value) : null,
                    SalaryCurrencyId = ene_Policy.ene_ContractingCurrency?.Id

                };
            }
        }

        public ChangeRequestDTO GenerateTemplateCensusChangeReqFromCensusLine(CensusLineDTO model)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                string _PaymentResponsibleName = GetCustomerName(model.ene_PaymentResponsibleId.Value);
                string _ReportingResponsibleName = "";
                if (model.ene_ReportingResponsibleId != null)//model.ene_ReportingResponsibleId ovo bude null i zato je bacalo onaj error, ne znam da li da ostavimo ovako ili da na drugaciji nacin hendlamo ovaj null
                {
                    _ReportingResponsibleName = GetCustomerName(model.ene_ReportingResponsibleId.Value);
                }
                string _InsuredName = GetCustomerName(model.ene_InsuredId.Value);
                PolicyDTO policyDTO = _policyService.GetRecord(model.ene_PolicyId.Value);

                ChangeRequestDTO ccr = new ChangeRequestDTO();
                ccr.CensusLineId = model.Id;
                //ccr.externalID = model.ene_ExternalId;
                //ccr.Name = model.ene_InsuredFullName;
                //ccr.FirstName = model.ene_InsuredFullName?.Split(' ')[0];
                //ccr.LastName = model.ene_InsuredFullName?.Split(' ')[1];
                //ccr.Gender = model.ene_Gender != null ? (Enetel.DTO.Gender?)model.ene_Gender.Value : null;
                //ccr.DateofBirth = model.ene_DateofBirth?.ToUniversalTime();
                //ccr.YearOfBirth = model.ene_DateofBirth?.Year;
                //ccr.Citizenship = model.ene_Citizenship?.Id;
                //ccr.ResidenceCountry = model.ene_ResidenceCountry?.Id;
                ccr.Plan = model.ene_PlanId;
                ccr.DateJoined = model.ene_DateJoined?.ConvertDateTimeToLocal();
                ccr.DateLeft = model.ene_DateLeft?.ConvertDateTimeToLocal();
                ccr.Salary = model.ene_Salary;
                ccr.SalaryCurrencyId = policyDTO.ene_ContractingCurrency;
                ccr.Insured = model.ene_InsuredId;
                ccr.tmp_InsuredName = _InsuredName;
                ccr.SumInsured = model.ene_SumInsured;
                ccr.SalaryDescription = model.ene_SalaryDescription;
                ccr.PersonsAccountId = Identity.PortalUserSession.ene_Account;
                ccr.PersonsAccount = model.ene_PersonalAccount;
                ccr.Policy = model.ene_PolicyId;
                ccr.tmp_PolicyName = policyDTO.ene_InternalPolicyName;
                ccr.tmp_PolicyNumber = policyDTO.ene_policynumber;
                ccr.PaymentResponsible = model.ene_PaymentResponsibleId;
                ccr.tmp_PaymentResponsibleFullName = _PaymentResponsibleName;
                ccr.ReportingResponsible = model.ene_ReportingResponsibleId;
                ccr.tmp_ReportingResponsibleFullName = _ReportingResponsibleName;
                //ccr.ThispersonisPaymentResponsible = model.ene_PaymentResponsible?.Id == model.ene_Insured?.Id;
                //ccr.ThispersonisReportingResponsible = model.ene_ReportingResponsible?.Id == model.ene_Insured?.Id;

                return ccr;
            }
        }

        public bool ChangeRequestExists(Guid policyId, string firstname, string lastname, DateTime? dateOfBirth)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                var cls = crm.ene_censuschangerequestSet.Where(x => x.ene_PersonsAccount.Id == Identity.PortalUserSession.ene_Account &&
                                                                    x.ene_Policy.Id == policyId && x.ene_FirstName == firstname &&
                                                                    x.ene_LastName == lastname && x.ene_DateofBirth == dateOfBirth &&
                                                                    x.statecode == ene_censuschangerequestState.Active)
                                                        .FirstOrDefault();

                if (cls != null)
                    return true;

                return false; 
            }
        }

        public List<ChangeRequestDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10)
        {
            throw new NotImplementedException();
        }
    }
}
