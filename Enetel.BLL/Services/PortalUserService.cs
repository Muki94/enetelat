﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.BLL.Mappers;
using Enetel.DAL.Models;
using Enetel.DTO;
using Enetel.Helper;
using Microsoft.Xrm.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Enetel.BLL.Services
{
    public class PortalUserService : IPortalUser
    {
        private IOrganizationService _service;
        private ICrypto _cryptoService;
        private IShared _sharedService;

        public PortalUserService(ICrypto cryptoService,
                                 IShared sharedService)
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
            _cryptoService = cryptoService;
            _sharedService = sharedService;
        }

        public List<PortalUserDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                string filter = $@"<filter type='and'>
                                        <condition attribute = 'ene_account' operator= 'eq' value = '{dataTableFilters.accountId}'/>
                                   </filter>";

                numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service,ene_B2BPortalUser.EntityLogicalName.ToLower(), filter);

                var portalUsers = crm.ene_B2BPortalUserSet.Where(x => x.ene_Account.Id == dataTableFilters.accountId);

                switch ((FilterAttributesDTO.PortalUsersColumns)dataTableFilters.orderColumnNumber)
                {
                    case FilterAttributesDTO.PortalUsersColumns.FirstName:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            portalUsers = portalUsers.OrderByDescending(x => x.ene_FirstName);
                        else
                            portalUsers = portalUsers.OrderBy(x => x.ene_FirstName);
                        break;
                    case FilterAttributesDTO.PortalUsersColumns.LastName:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            portalUsers = portalUsers.OrderByDescending(x => x.ene_LastName);
                        else
                            portalUsers = portalUsers.OrderBy(x => x.ene_LastName);
                        break;
                    case FilterAttributesDTO.PortalUsersColumns.Email:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            portalUsers = portalUsers.OrderByDescending(x => x.ene_Email);
                        else
                            portalUsers = portalUsers.OrderBy(x => x.ene_Email);
                        break;
                    case FilterAttributesDTO.PortalUsersColumns.Name:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            portalUsers = portalUsers.OrderByDescending(x => x.ene_name);
                        else
                            portalUsers = portalUsers.OrderBy(x => x.ene_name);
                        break;
                    case FilterAttributesDTO.PortalUsersColumns.CreatedOn:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            portalUsers = portalUsers.OrderByDescending(x => x.CreatedOn);
                        else
                            portalUsers = portalUsers.OrderBy(x => x.CreatedOn);
                        break;
                    case FilterAttributesDTO.PortalUsersColumns.StatusCode:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            portalUsers = portalUsers.OrderByDescending(x => x.statuscode);
                        else
                            portalUsers = portalUsers.OrderBy(x => x.statuscode);
                        break;
                    default:
                        break;
                }

                return portalUsers.Skip(dataTableFilters.start.Value)
                                  .Take(dataTableFilters.length.Value)
                                  .ToList()
                                  .MapPortalUserToPortalUserDTO();  
            }
        }

        public PortalUserDTO GetRecord(Guid _id)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return crm.ene_B2BPortalUserSet.Where(x => x.Id == _id)
                                       .FirstOrDefault()
                                       .MapPortalUserToPortalUserDTO(); 
            }
        }
         
        public PortalUserDTO GetLoginRecord(string _username, string _password)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                //take user 
                var user = crm.ene_B2BPortalUserSet.Where(x => x.ene_name == _username)
                                                   .FirstOrDefault()
                                                   .MapPortalUserToPortalUserDTO();

                var hashedPassword = Convert.FromBase64String(user.ene_Password);

                var salt = Convert.FromBase64String(user.Salt);

                //verify password matches
                var passwordsMatch = _cryptoService.VerifyPassword(_password, salt, hashedPassword);
                return passwordsMatch ? user : null;
            }
        }

        public void Terminate(PortalUserDTO user)
        {
            using (var context = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                var dbModel = context.ene_B2BPortalUserSet.Where(x => x.Id == user.Id)
                                                          .FirstOrDefault();

                dbModel.statuscode = ene_b2bportaluser_statuscode.Inactive;
                dbModel.statecode = ene_B2BPortalUserState.Inactive;

                if (!context.IsAttached(dbModel))
                    context.Attach(dbModel);

                context.UpdateObject(dbModel);
                context.SaveChanges(); 
            }
        }

        public Guid AddNewRecord(PortalUserDTO user)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                user.LogicalName = ene_B2BPortalUser.EntityLogicalName;
                user.ene_Account = Identity.PortalUserSession.ene_Account;
                user.statuscodeNumber = 1;
                user.statecodeNumber = 0;

                var dbModel = user.MapPortalUserDTOToPortalUser();

                var salt = _cryptoService.GenerateSalt();
                var hashedPassword = _cryptoService.ComputeHash(user.ene_Password, salt);

                crm.AddObject(dbModel);
                crm.SaveChanges();

                return dbModel.Id; 
            }
        }

        public Guid EditRecord(PortalUserDTO user)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                user.LogicalName = ene_B2BPortalUser.EntityLogicalName;
                user.ene_Account = Identity.PortalUserSession.ene_Account;

                var dbModel = user.MapPortalUserDTOToPortalUser();

                if (!crm.IsAttached(dbModel))
                    crm.Attach(dbModel);

                crm.UpdateObject(dbModel);
                crm.SaveChanges();

                return dbModel.Id; 
            }
        }

        public bool CheckExistingUser(string username)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return crm.ene_B2BPortalUserSet.Where(x => x.ene_name == username)
                                               .FirstOrDefault() != null ? true
                                                                         : false; 
            }
        }

        public bool CheckUserMailExistsForCompany(string ene_Email, Guid companyId)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return crm.ene_B2BPortalUserSet.Where(x => x.ene_Account.Id == companyId && x.ene_Email == ene_Email)
                                               .FirstOrDefault() != null ? true
                                                                         : false;
            }
        }

        public List<PortalUserDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10)
        {
            throw new NotImplementedException();
        }
    }
}
