﻿using Enetel.BLL.Interfaces;
using Enetel.DTO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using SP = Microsoft.SharePoint.Client;
using Enetel.BLL.Helpers;

namespace Enetel.BLL.Services
{
    public class FileService : IFile
    {
        public FileDTO GetFileFromSharepoint(ParameterDTO parameters)
        {
            try
            {
                using (var ctx = SharepointConnectionService.GetSharepointConnection())
                {
                    SP.FileCollection files = ctx.Web.GetFolderByServerRelativeUrl(parameters.FolderPath).Files;

                    ctx.Load(files);
                    ctx.ExecuteQuery();

                    if (files != null && files.Count > 0)
                    {
                        var file = files.FirstOrDefault();

                        FileDTO fileModel = new FileDTO();

                        fileModel.FileType = MimeMapping.GetMimeMapping(file.Name);
                        fileModel.FileName = file.Name;

                        ctx.ExecuteQuery();

                        SP.FileInformation fileInfo = SP.File.OpenBinaryDirect(ctx, file.ServerRelativeUrl);
                        ctx.ExecuteQuery();

                        fileModel.FileStream = fileInfo.Stream;

                        return fileModel;
                    }

                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public bool UploadFileToSharepoint(ParameterDTO parameters)
        {
            try
            {
                using (var ctx = SharepointConnectionService.GetSharepointConnection())
                {
                    string personIdModified = "";

                    foreach (var item in parameters.PersonId.ToUpper().Split('-'))
                        personIdModified += item;

                    SP.Folder folder = null;
                    string folderPath = $@"/Account/{parameters.AccountName}/contact/{parameters.PersonFullName}_{personIdModified}";
                    ctx.Web.TryGetFolderByServerRelativeUrl(folderPath, out folder);

                    if (folder == null)
                    {
                        ctx.Web.TryGetFolderByServerRelativeUrl($@"/Account/{parameters.AccountName}/contact", out folder);
                        folder = folder.Folders.Add($@"{parameters.PersonFullName}_{personIdModified}");
                    }

                    using (var memory = new MemoryStream())
                    {
                        parameters.FileMemoryStream.CopyTo(memory);

                        folder.Files.Add(new SP.FileCreationInformation
                        {
                            Overwrite = true,
                            Url = parameters.FileType.ToString() + parameters.FileName,
                            Content = memory.ToArray()
                        });
                    }

                    ctx.ExecuteQuery();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}
