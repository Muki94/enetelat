﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.DAL.Models;
using Enetel.DTO;
using Enetel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using Enetel.BLL.Mappers;
using Microsoft.Xrm.Sdk;

namespace Enetel.BLL.Services
{
    public class LogService : ILog
    {
        private IOrganizationService _service;
        private IShared _sharedService;

        public LogService(IShared sharedService)
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
            _sharedService = sharedService;
        }

        public Guid AddNewRecord(EventLogDTO element)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                if (Identity.UserAuthenticated())
                    element.ene_B2BPortalUserID = Identity.PortalUserSession.Id; //new EntityReference(ene_B2BPortalUser.EntityLogicalName, Authentication.PortalUserSession.Id);

                element.Id = Guid.NewGuid();
                element.ene_RecordID = Identity.PortalUserSession?.ene_Account.Value.ToString();
                var dbModel = element.MapEventLogDTOToEventLog();

                crm.AddObject(dbModel);
                crm.SaveChanges();

                return element.Id;
            }
        }

        public void DeleteRecord(Guid id)
        {
            throw new NotImplementedException();
        }

        public Guid EditRecord(EventLogDTO element)
        {
            throw new NotImplementedException();
        }

        public EventLogDTO GetRecord(Guid id)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return crm.ene_B2BPortalEventLogSet.Where(x => x.Id == id)
                                                   .FirstOrDefault()
                                                   .MapEventLogToEventLogDTO();
            }
        }

        public List<EventLogDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service, ene_B2BPortalEventLog.EntityLogicalName.ToLower(), "");

                var logs = crm.ene_B2BPortalEventLogSet.ToList();

                switch ((FilterAttributesDTO.LogsColumns)dataTableFilters.orderColumnNumber)
                {
                    case FilterAttributesDTO.LogsColumns.ActionType:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            logs = logs.OrderByDescending(x => x.ene_ActionType).ToList();
                        else
                            logs = logs.OrderBy(x => x.ene_ActionType).ToList();
                        break;
                    case FilterAttributesDTO.LogsColumns.Description:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            logs = logs.OrderByDescending(x => x.ene_Description).ToList();
                        else
                            logs = logs.OrderBy(x => x.ene_Description).ToList();
                        break;
                    case FilterAttributesDTO.LogsColumns.LongDescription:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            logs = logs.OrderByDescending(x => x.ene_LongDescription).ToList();
                        else
                            logs = logs.OrderBy(x => x.ene_LongDescription).ToList();
                        break;
                    case FilterAttributesDTO.LogsColumns.ObjectTypeCode:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            logs = logs.OrderByDescending(x => x.ene_ObjectTypeCode).ToList();
                        else
                            logs = logs.OrderBy(x => x.ene_ObjectTypeCode).ToList();
                        break;
                    case FilterAttributesDTO.LogsColumns.CreatedOn:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            logs = logs.OrderByDescending(x => x.CreatedOn).ToList();
                        else
                            logs = logs.OrderBy(x => x.CreatedOn).ToList();
                        break;
                    default:
                        logs = logs.OrderByDescending(x => x.CreatedOn).ToList();
                        break;
                }

                return logs.Skip(dataTableFilters.start.Value)
                           .Take(dataTableFilters.length.Value)
                           .ToList()
                           .MapEventLogToEventLogDTO();
            }
        }

        public List<EventLogDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                string filter = $@"<filter type='and'>
                                <condition attribute='ene_recordid' operator='eq' value='{Identity.PortalUserSession.ene_Account.Value.ToString()}'/>
                               </filter>";

                numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service, ene_B2BPortalEventLog.EntityLogicalName.ToLower(), filter);

                return crm.ene_B2BPortalEventLogSet.Where(x=>x.ene_RecordID == Identity.PortalUserSession.ene_Account.Value.ToString()).OrderByDescending(x => x.CreatedOn)
                                                      .Skip((page - 1) * numberOfRecordsPerPage)
                                                      .Take(numberOfRecordsPerPage)
                                                      .ToList()
                                                      .MapEventLogToEventLogDTO();
            }
        }
    }
}
