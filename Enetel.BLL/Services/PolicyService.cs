﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.DAL.Models;
using Enetel.DTO;
using Enetel.Helper;
using System;
using System.Collections.Generic;
using System.Linq;
using Enetel.BLL.Mappers;
using Microsoft.Xrm.Sdk;

namespace Enetel.BLL.Services
{
    public class PolicyService : IPolicy
    {
        private IOrganizationService _service;
        private IShared _sharedService;

        public PolicyService(IShared sharedService)
        {
            _service = OrganizationService.GetOrganizationServiceInstance();
            _sharedService = sharedService;
        }

        public Guid AddNewRecord(PolicyDTO element)
        {
            throw new NotImplementedException();
        }

        public void DeleteRecord(Guid id)
        {
            throw new NotImplementedException();
        }

        public Guid EditRecord(PolicyDTO element)
        {
            throw new NotImplementedException();
        }

        public PolicyDTO GetRecord(Guid id)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                return crm.ene_policySet.Where(x => x.Id == id)
                                        .FirstOrDefault()
                                        .MapPolicyToPolicyDTO(); 
            }
        }

        public List<PolicyDTO> GetRecords(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                ene_policyState state = ene_policyState.Active;

                dataTableFilters.policyStatusCode = dataTableFilters.policyStatusCode == 0 ? (int)ene_policy_statuscode.Active : dataTableFilters.policyStatusCode;

                if (dataTableFilters.policyStatusCode == 200000001)
                    state = ene_policyState.Inactive;

                string filter = $@"<filter type='and'><condition attribute = 'ene_policyholder' operator= 'eq' value = '{dataTableFilters.accountId}'/>
                                        <condition attribute = 'statecode' operator= 'eq' value = '{(int)state}'/>
                                        <condition attribute = 'statuscode' operator= 'eq' value = '{dataTableFilters.policyStatusCode}'/>
                                    </filter>";

                numberOfElements = _sharedService.GetNumberOfRecordsForEntity(_service,ene_policy.EntityLogicalName.ToLower(), filter);

                var policies = crm.ene_policySet.Where(x => x.ene_Policyholder.Id == dataTableFilters.accountId && x.statecode == state && x.statuscode == (ene_policy_statuscode)dataTableFilters.policyStatusCode);

                switch ((FilterAttributesDTO.PoliciesColumns)dataTableFilters.orderColumnNumber)
                {
                    case FilterAttributesDTO.PoliciesColumns.PolicyNumber:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            policies = policies.OrderByDescending(x => x.ene_policynumber);
                        else
                            policies = policies.OrderBy(x => x.ene_policynumber);
                        break;
                    case FilterAttributesDTO.PoliciesColumns.ExternalPolicyNo:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            policies = policies.OrderByDescending(x => x.ene_ExternalPolicyNo);
                        else
                            policies = policies.OrderBy(x => x.ene_ExternalPolicyNo); break;
                    case FilterAttributesDTO.PoliciesColumns.InsuranceType:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            policies = policies.OrderByDescending(x => x.ene_InsuranceType);
                        else
                            policies = policies.OrderBy(x => x.ene_InsuranceType); break;
                    case FilterAttributesDTO.PoliciesColumns.StartDate:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            policies = policies.OrderByDescending(x => x.ene_StartDate);
                        else
                            policies = policies.OrderBy(x => x.ene_StartDate); break;
                    case FilterAttributesDTO.PoliciesColumns.EndDate:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            policies = policies.OrderByDescending(x => x.ene_EndDate);
                        else
                            policies = policies.OrderBy(x => x.ene_EndDate); break;
                    case FilterAttributesDTO.PoliciesColumns.GroupSize:
                        if (dataTableFilters.orderColumnOrientation == "desc")
                            policies = policies.OrderByDescending(x => x.ene_GroupSize);
                        else
                            policies = policies.OrderBy(x => x.ene_GroupSize); break;
                    default:
                        break;
                }

                return policies.Skip(dataTableFilters.start.Value)
                               .Take(dataTableFilters.length.Value)
                               .ToList()
                               .MapPolicyToPolicyDTO();  
            }
        }

        public List<PolicyDTO> GetRecords(ref int numberOfElements, int page, int numberOfRecordsPerPage = 10)
        {
            throw new NotImplementedException();
        }

        public List<PolicyDTO> GetRecords()
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance(_service))
            {
                List<PolicyDTO> policies = PolicyMapper.MapPolicyToPolicyDTO(crm.ene_policySet.Where(x => x.ene_Policyholder.Id == Identity.PortalUserSession.ene_Account.Value &&
                                                                                                         ((x.statecode == ene_policyState.Active && x.statuscode == ene_policy_statuscode.Active) || 
                                                                                                         x.ene_EndDate.Value > DateTime.Now.AddMonths(-24)))
                                                                                              .ToList());

                return policies;
            }
        }

        public KeyValuePair<string, int>? GetPolicyStatus(Guid policyId)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                var policy = crm.ene_policySet.Where(x => x.Id == policyId).FirstOrDefault();

                if (policy != null)
                    return new KeyValuePair<string, int>(policy.statecode.ToString(), (int)policy.statecode.Value);

                return null;
            }
        }

        public int GetInsuranceType(Guid Id)
        {
            string[] collumns = new string[] { "ene_insurancetype" };
            ene_policy policy = (ene_policy)_service.Retrieve(ene_policy.EntityLogicalName, Id, new Microsoft.Xrm.Sdk.Query.ColumnSet(collumns));
            return policy.ene_InsuranceType != null ? policy.ene_InsuranceType.Value : 0;
        }
    }
}
