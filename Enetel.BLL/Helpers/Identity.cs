﻿using Enetel.BLL.Services;
using Enetel.DTO;
using Enetel.Helper;
using System;
using System.Linq;
using System.Web;

namespace Enetel.BLL.Helpers
{
    public class Identity
    {
        public static PortalUserDTO PortalUserSession
        {
            get { return (PortalUserDTO)HttpContext.Current.Session["user"]; }
            set { HttpContext.Current.Session["user"] = value; }
        }

        public static string GetLogedInUserName()
        {
            return PortalUserSession != null ?
                   $"{PortalUserSession.ene_name}" :
                   "User";
        }

        public static void Logout()
        {
            HttpContext.Current.Session.Remove("user");
        }

        public static bool UserAuthenticated()
        {
            if (Identity.PortalUserSession == null)
                return false;
            return true;
        }

        public static bool ChangePassword(string newPassword)
        {
            using (var crm = OrganizationService.GetCrmEntitiesContextInstance())
            {
                var userId = PortalUserSession.Id;
                var user = crm.ene_B2BPortalUserSet.Where(x => x.Id == userId).FirstOrDefault();

                user.ene_Password = newPassword;

                if (!crm.IsAttached(user))
                    crm.Attach(user);

                crm.UpdateObject(user);
                crm.SaveChanges();

                return true;
            }
        }
    }
}
