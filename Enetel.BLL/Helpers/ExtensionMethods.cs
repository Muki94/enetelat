﻿using Enetel.DAL.Models;
using Microsoft.SharePoint.Client;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Messages;
using Microsoft.Xrm.Sdk.Metadata;
using System;
using System.Globalization;
using System.Linq;

namespace Enetel.BLL.Helpers
{
    public static class ExtensionMethods
    {
        public static Entity GetCustomer(this Guid custid, CrmEntitiesContext context)
        {

            Entity entity = context.ContactSet.FirstOrDefault(x => x.ContactId == custid);

            if (entity == null)
                entity = context.AccountSet.FirstOrDefault(x => x.AccountId == custid);

            return entity;

        }

        public static bool TryGetFileByServerRelativeUrl(this Web web, string serverRelativeUrl, out File file)
        {
            var ctx = web.Context;
            try
            {
                file = web.GetFileByServerRelativeUrl(serverRelativeUrl);
                ctx.Load(file);
                ctx.ExecuteQuery();
                return true;
            }
            catch (ServerException ex)
            {
                if (ex.ServerErrorTypeName == "System.IO.FileNotFoundException")
                {
                    file = null;
                    return false;
                }
                throw;
            }
        }
       
        public static DateTime ConvertDateTimeToLocal(this DateTime datetime) 
        {
            DateTime dateValue;

            if (DateTime.TryParseExact(datetime.ToString("dd.MM.yyyy HH:mm:ss"), @"dd.MM.yyyy HH:mm:ss", new CultureInfo("de-AT"), DateTimeStyles.AssumeUniversal, out dateValue))
                return dateValue;

            return dateValue;

        }

        public static bool TryGetFolderByServerRelativeUrl(this Web web, string serverRelativeUrl, out Folder folder)
        {
            var ctx = web.Context;
            try
            {
                folder = web.GetFolderByServerRelativeUrl(serverRelativeUrl);
                ctx.Load(folder);
                ctx.ExecuteQuery();
                return true;
            }
            catch (ServerException ex)
            {
                if (ex.ServerErrorTypeName == "System.IO.FileNotFoundException")
                {
                    folder = null;
                    return false;
                }
                throw;
            }
        }
    }
}
