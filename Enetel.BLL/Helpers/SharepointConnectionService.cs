﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using SP = Microsoft.SharePoint.Client;

namespace Enetel.BLL.Helpers
{
    public static class SharepointConnectionService
    {
        public static string SiteUrl => "https://files.1.dev.dhig.info:17171";
        //public static string Password => "Uhsd3398";
        //public static string Username => "no@enetel.at";
        public static string Password => "Vole2831";
        public static string Username => "api@enetel.at";

        public static SP.ClientContext GetSharepointConnection()
        {
            var secure = new SecureString();

            foreach (var c in Password)
                secure.AppendChar(c);

            ICredentials credentials = new NetworkCredential(Username, secure);

            SP.ClientContext ctx = new SP.ClientContext(SiteUrl);
            ctx.Credentials = credentials;

            return ctx;
        }
    }
}
