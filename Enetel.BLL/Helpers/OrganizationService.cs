﻿using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using System;
using System.Net;
using System.ServiceModel.Description;
using Enetel.DAL.Models;

namespace Enetel.Helper
{
    public static class OrganizationService
    {
        public enum FilterType
        {
            and,
            or
        }

        public static IOrganizationService service()
        {
            try
            {
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = "api@enetel.at";
                clientCredentials.UserName.Password = "Vole2831";

                //clientCredentials.Windows.ClientCredential.Domain=""

                // For Dynamics 365 Customer Engagement V9.X, set Security Protocol as TLS12
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                // Get the URL from CRM, Navigate to Settings -> Customizations -> Developer Resources
                // Copy and Paste Organization Service Endpoint Address URL
                // Two tipes of environments 1. TEST 2. DEV

                string url = $"https://1.dev.dhig.info:443/XRMServices/2011/Organization.svc";

                using (OrganizationServiceProxy proxy = new OrganizationServiceProxy(new Uri(url), null, clientCredentials, null))
                {
                    proxy.EnableProxyTypes();

                    if (proxy != null)
                        return proxy;

                    return null;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public static IOrganizationService GetOrganizationServiceInstance()
        {
           return OrganizationService.service();
        }

        public static CrmEntitiesContext GetCrmEntitiesContextInstance(IOrganizationService _service=null)
        {
            if (_service == null)
                _service = GetOrganizationServiceInstance();
            return new CrmEntitiesContext(_service);
        }

        public static string GenerateFilter(FilterType filterType, string attributeName, string operation, string attributeValue)
        {
            return $@"<filter type='{filterType}'>   
                        <condition attribute='{attributeName}' operator='{operation}' value='{attributeValue}' />   
                      </filter>";
        }
    }
}