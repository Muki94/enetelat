﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.DAL.Models;
using Enetel.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EnetelApp.Controllers
{
    public class PolicyController : Controller
    {
        readonly IPolicy _policyService;
        readonly IShared _sharedService;

        public PolicyController(IPolicy policyService,
                                IShared sharedService)
        {
            _policyService = policyService;
            _sharedService = sharedService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            Session["CURRENTPAGE"] = "POLICIES";

            if (!Identity.UserAuthenticated())
                return RedirectToAction("Index", "Login", null);

            return View();
        }

        public JsonResult Index_Ajax(FilterAttributesDTO dataTableFilters)
        {
            if (!Identity.UserAuthenticated())
                return Json(new { code = 401, message = "Niste autorizovani da pristupite stranici!" }, JsonRequestBehavior.AllowGet);

            int numberOfElements = 0;

            var policies = Search(ref numberOfElements,
                                     dataTableFilters);

            return Json(new
            {
                start = dataTableFilters.start,
                recordsTotal = numberOfElements,
                recordsFiltered = numberOfElements,
                data = policies
            }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public int GetPolicyInsuranceType(Guid Id)
        {
            return _policyService.GetInsuranceType(Id);
        }

        public IEnumerable<PolicyDTO> Search(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            dataTableFilters.accountId = Identity.PortalUserSession.ene_Account.Value;

            return _policyService.GetRecords(ref numberOfElements, dataTableFilters);
        }

        public async Task<JsonResult> GetPolicyStatuses()
        {
            List<KeyValuePair<int,string>> statuses = (await _sharedService.GetEnumerationListAsync<ene_policy_statuscode>()).Where(x => x.Value == ene_policy_statuscode.Active.ToString() ||
                                                                                                                                    x.Value == ene_policy_statuscode.Inactive.ToString())
                                                                                                                        .ToList();

            return Json(statuses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPolicyStatus(Guid policyId) {
            try
            {
                var policyStatus = _policyService.GetPolicyStatus(policyId);

                return Json( policyStatus, JsonRequestBehavior.AllowGet );
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}
