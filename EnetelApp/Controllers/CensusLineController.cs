﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.DAL.Models;
using Enetel.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EnetelApp.Controllers
{
    public class CensusLineController : Controller
    {
        private readonly IPolicy _policyService;
        private readonly ICensusLine _censusLineService;
        private readonly IShared _sharedService;
        private readonly IPlan _planService;

        public CensusLineController(IPolicy policyService,
                                    ICensusLine censusLineService,
                                    IShared sharedService,
                                    IPlan planService)
        {
            _policyService = policyService;
            _censusLineService = censusLineService;
            _sharedService = sharedService;
            _planService = planService;
        }

        [HttpGet]
        public async Task<ActionResult> Index(Guid? policyId)
        {
            if(policyId == null)
                return RedirectToAction("Index", "Policy", null);

            TempData["policy"] = _policyService.GetRecord(policyId.Value);
            TempData["states"] = await _sharedService.GetEnumerationListAsync<FilterAttributesDTO.CensusLineStatus>();

            return View();
        }

        public JsonResult Index_Ajax(FilterAttributesDTO dataTableFilters)
        {
            if (!Identity.UserAuthenticated())
                return Json(new { code = 401, message = "You are not authorized to access this site!" }, JsonRequestBehavior.AllowGet);

            int numberOfElements = 0;

            var policyStatus = _policyService.GetPolicyStatus(dataTableFilters.policyId);

            if (policyStatus != null && policyStatus.Value.Key.ToUpper() == "INACTIVE")
                dataTableFilters.status = policyStatus.Value.Value;

            var persons = Search(ref numberOfElements,
                                 dataTableFilters);

            return Json(new
            {
                start = dataTableFilters.start,
                recordsTotal = numberOfElements,
                recordsFiltered = numberOfElements,
                data = persons
            }, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<CensusLineDTO> Search(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            return _censusLineService.GetRecords(ref numberOfElements, dataTableFilters);
        }

        [HttpGet]
        public ActionResult Details(Guid id, string error = "")
        {
            var censusLine = _censusLineService.GetRecord(id);

            if (censusLine == null)
                return new HttpNotFoundResult();

            TempData["policy"] = _policyService.GetRecord(censusLine.ene_PolicyId.Value);
            TempData["Error"] = error;

            return View("Details", censusLine);
        }

        public async Task<JsonResult> GetCensusLineStatuses()
        {
            List<KeyValuePair<int, string>> statuses = await _sharedService.GetEnumerationListAsync<ene_censusline_statuscode>();
            statuses = statuses.Where(x => x.Key == 1 || x.Key == 200000002).ToList();

            

            return Json(statuses, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetCensusLineInsuredRoles()
        {
            List<KeyValuePair<int?, string>> insuredRoles = _sharedService.GetListFromOptionSet("ene_censusline", "ene_insuredrole");

            return Json(insuredRoles, JsonRequestBehavior.AllowGet);
        }

        public JsonResult GetPlans(Guid policyId) {
            return Json(_planService.GetPlansForPolicy(policyId), JsonRequestBehavior.AllowGet);
        }
    }
}
