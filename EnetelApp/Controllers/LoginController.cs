﻿using Enetel.BLL.Helpers;
using System;
using System.Web.Mvc;
using Enetel.DTO;
using Enetel.BLL.Interfaces;

namespace EnetelApp.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
        private readonly ILog _logService;
        private readonly IPortalUser _portalUserService;

        public LoginController(ILog logService,
                               IPortalUser portalUserService)
        {
            _logService = logService;
            _portalUserService = portalUserService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            if (Identity.PortalUserSession != null)
                return RedirectToAction("Index", "Policy");

            TempData["AuthenticationMessage"] = null;

            return View("Login");
        }

        [HttpPost]
        public ActionResult CheckLogin(PortalUserDTO user)
        {
            try
            {
                if (String.IsNullOrEmpty(user.ene_Password) || String.IsNullOrEmpty(user.ene_name))
                {
                    TempData["AuthenticationMessage"] = "Please enter username and password.";

                    _logService.AddNewRecord(new EventLogDTO
                    {
                        ene_Description = $"Failed attempt at login on {DateTime.Now.ToString()}",
                        ene_LongDescription = "",
                        ene_ObjectTypeCode = 1,
                        ene_ActionType = "Login"
                    });

                    ModelState.Clear();

                    if (String.IsNullOrEmpty(user.ene_name))
                        ModelState.AddModelError("ene_name", "Username is required");
                    else
                        ModelState.AddModelError("ene_Password", "Password is required");

                    return View("Login", user);
                }

                PortalUserDTO portalUser = _portalUserService.GetLoginRecord(user.ene_name, user.ene_Password);

                if (portalUser == null)
                {
                    TempData["AuthenticationMessage"] = "Username or password is incorect!";

                    _logService.AddNewRecord(new EventLogDTO
                    {
                        ene_Description = $"Failed attempt at login on {DateTime.Now.ToString()}",
                        ene_LongDescription = "",
                        ene_ObjectTypeCode = 1,
                        ene_ActionType = "Login"
                    });

                    return View("Login");
                }

                if(portalUser?.statuscode.ToUpper() == "INACTIVE")
                {
                    TempData["AuthenticationMessage"] = "This user has been deactivated! Contact your administrator.";

                    _logService.AddNewRecord(new EventLogDTO
                    {
                        ene_Description = $"Failed attempt at login with deactivated user \'" + portalUser.ene_name + "\' on " + DateTime.Now.ToString(),
                        ene_LongDescription = "",
                        ene_ObjectTypeCode = 1,
                        ene_ActionType = "Login"
                    });

                    return View("Login");
                }

                Session["CURRENTPAGE"] = null;
                Identity.PortalUserSession = portalUser;
                Identity.PortalUserSession.ene_Admin = portalUser.ene_Admin;

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"User {Identity.PortalUserSession.ene_name} logged in on {DateTime.Now.ToString()}.",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Login"
                });

                return RedirectToAction("Index", "Policy");
            }
            catch (Exception ex)
            {
                TempData["AuthenticationMessage"] = "There has been an unexpected error.Please contact your administrator.";

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"Failed attempt at login on {DateTime.Now.ToString()}",
                    ene_LongDescription = $"{ex.Message}, {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Login"
                });

                return View("Login");
            }
        }

        [HttpGet]
        public ActionResult Logout()
        {
            try
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"User {Identity.PortalUserSession.ene_name} logged out on {DateTime.Now.ToString()}.",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Login"
                });

                Identity.Logout();

                return RedirectToAction("Index", "Login");
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"Unexpected error on {DateTime.Now.ToString()}.",
                    ene_LongDescription = $"{ex.Message} {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Login"
                });

                return RedirectToAction("Index", "Login");
            }
        }
    }
}
