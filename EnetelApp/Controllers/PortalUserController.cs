﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Enetel.BLL.Helpers;
using PagedList;
using Enetel.DTO;
using Enetel.BLL.Interfaces;
using System.Text.RegularExpressions;

namespace EnetelApp.Controllers
{
    public class PortalUserController : Controller
    {
        private readonly IPortalUser _portalUserService;
        private readonly ILog _logService;
        private readonly ICrypto _cryptoService;

        public PortalUserController(IPortalUser portalUserService,
                                    ILog logService,
                                    ICrypto cryptoService)
        {
            _portalUserService = portalUserService;
            _logService = logService;
            _cryptoService = cryptoService;
        }

        [HttpGet]
        public ActionResult Index()
        {
            Session["CURRENTPAGE"] = "PORTALUSER";

            if (!(bool)Identity.PortalUserSession.ene_Admin)
                return RedirectToAction("Index", "Home", null);

            return View();
        }

        public JsonResult Index_Ajax(FilterAttributesDTO dataTableFilters)
        {
            if (!Identity.UserAuthenticated())
                return Json(new { code = 401, message = "Niste autorizovani da pristupite stranici!" }, JsonRequestBehavior.AllowGet);

            int numberOfElements = 0;

            var portalUsers = Search(ref numberOfElements,
                                     dataTableFilters);

            return Json(new
            {
                start = dataTableFilters.start,
                recordsTotal = numberOfElements,
                recordsFiltered = numberOfElements,
                data = portalUsers
            }, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<PortalUserDTO> Search(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            dataTableFilters.accountId = Identity.PortalUserSession.ene_Account.Value;

            return _portalUserService.GetRecords(ref numberOfElements, dataTableFilters);
        }

        [HttpGet]
        public ActionResult Create()
        {
            if (!(bool)Identity.PortalUserSession.ene_Admin)
                return RedirectToAction("Index", "Home", null);

            return View();
        }

        [HttpPost]
        public ActionResult Create(PortalUserDTO user)
        {
            try
            {
                if (!(bool)Identity.PortalUserSession.ene_Admin)
                    return RedirectToAction("Index", "Home", null);

                if (_portalUserService.CheckExistingUser(user.ene_name))
                {
                    ModelState.AddModelError("ene_name", "This username is already taken. Please try another one!");
                    return View(user);
                }
                else if(_portalUserService.CheckUserMailExistsForCompany(user.ene_Email, Identity.PortalUserSession.ene_Account.Value))
                {
                    ModelState.AddModelError("ene_Email", "This email is already taken. Please try another one!");
                    return View(user);
                }
                else if (String.IsNullOrEmpty(user.ene_Password))
                {
                    ModelState.AddModelError("ene_Password", "Password is required!");
                    return View(user);
                }

                if (!ModelState.IsValid)
                    return View(user);

                _portalUserService.AddNewRecord(user);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"Successfully created portal user: {user.ene_name}",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Portal User"
                });

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.Id.ToString(),
                    ene_Description = $"Failed: {ex.Message}",
                    ene_LongDescription = $"{ex.Message} {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create/Edit Portal User"
                });

                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            if (!(bool)Identity.PortalUserSession.ene_Admin)
                return RedirectToAction("Index", "Home", null);

            PortalUserDTO portalUser = _portalUserService.GetRecord(id);

            return View(portalUser);
        }

        [HttpPost]
        public ActionResult Edit(PortalUserDTO user)
        {
            try
            {
                if (!(bool)Identity.PortalUserSession.ene_Admin)
                    return RedirectToAction("Index", "Home", null);

                if (!ModelState.IsValid)
                    return RedirectToAction("Edit", user);

                _portalUserService.EditRecord(user);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = user.Id.ToString(),
                    ene_Description = $"Successfully saved changes on portal user: : {user.ene_name}",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Edit Portal User"
                });

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.Id.ToString(),
                    ene_Description = $"Failed: {ex.Message}",
                    ene_LongDescription = $"{ex.Message} {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create/Edit Portal User"
                });

                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpPost]
        public JsonResult Delete(Guid id)
        {
            try
            {
                if (!(bool)Identity.PortalUserSession.ene_Admin)
                    return Json(new { Code = 401 });

                PortalUserDTO user = _portalUserService.GetRecord(id);

                _portalUserService.Terminate(user);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = id.ToString(),
                    ene_Description = $"SUCCESS: User {user.ene_name} has been terminated.",
                    ene_LongDescription = $"-ID: {id.ToString()}\n-Username: {user.ene_name} \n-Full name: {user.ene_FirstName} {user.ene_LastName} \n-Email: {user.ene_Email}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Delete Portal User"
                });

                return Json(new { Code = 200 });
            }
            catch (Exception e)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"FAILED: {e.Message}",
                    ene_LongDescription = $"{e.Message} {e.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Terminate Portal User"
                });

                return Json(new { Code = 500 });
            }
        }

        [HttpPost]
        public JsonResult ChangePassword(PortalUserDTO user)
        {
            try
            {
                if (user.newPassword.Length < 8 || !Regex.IsMatch(user.newPassword, "^(?=.*[a-zA-Z])(?=.*[0-9]).+$"))
                {
                    return Json(new { Code = 404, Message = "Password must contain at least one letter(a-z) and one digit(0-9) and has to be at least 8 characters long." });
                }

                var oldPasswordMatches = _cryptoService.VerifyPassword(user.oldPassword,
                                                                       Convert.FromBase64String(Identity.PortalUserSession.Salt),
                                                                       Convert.FromBase64String(Identity.PortalUserSession.ene_Password));

                if (!oldPasswordMatches)
                    return Json(new { Code = 404, Message = "Old password does not match!" });

                if (user.newPassword != user.newPasswordConfirmation)
                    return Json(new { Code = 404, Message = "New password and confirmation password do not match!" });

                if (String.IsNullOrEmpty(user.newPassword))
                    return Json(new { Code = 404, Message = "New password can't be empty!" });

                Identity.ChangePassword(user.newPassword);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.Id.ToString(),
                    ene_Description = $"Password changed on {DateTime.Now.ToShortDateString()}",
                    ene_LongDescription = $"Changed password for user {Identity.PortalUserSession.ene_name}",
                    ene_ObjectTypeCode = 50,
                    ene_ActionType = "Change password"
                });

                return Json(new { Code = 200, Message = "Your password has been changed successfully." });
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"Failed change password attempt {DateTime.Now.ToShortDateString()}",
                    ene_LongDescription = $"{ex.Message}, {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 50,
                    ene_ActionType = "Change password"
                });

                return Json(new { Code = 500, Message = $"{ex.Message} {ex.InnerException?.Message}" });
            }
        }
    }
}
