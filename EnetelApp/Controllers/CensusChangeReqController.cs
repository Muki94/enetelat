﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;
using Enetel.DTO;
using Enetel.BLL.Interfaces;
using Enetel.BLL.Helpers;
using System.Threading.Tasks;
using Enetel.DAL.Models;
using System.Linq;

namespace EnetelApp.Controllers
{
    public class CensusChangeReqController : Controller
    {
        private readonly ICensusChangeReq _changeReqService;
        private readonly IPolicy _policyService;
        private readonly ICensusLine _censusLineService;
        private readonly ILog _logService;
        private readonly IShared _sharedService;

        public CensusChangeReqController(ICensusChangeReq changeReqService,
                                         IPolicy policyService,
                                         ICensusLine censusLineService,
                                         ILog logService,
                                         IShared sharedService
            )
        {
            _changeReqService = changeReqService;
            _policyService = policyService;
            _censusLineService = censusLineService;
            _logService = logService;
            _sharedService = sharedService;
        }

        [HttpGet]
        public ActionResult Index(int? page)
        {
            Session["CURRENTPAGE"] = "CENSUSCHANGEREQ";

            return View();
        }

        [HttpGet]
        public ActionResult Index_Ajax(FilterAttributesDTO dataTableFilters)
        {
            if (!Identity.UserAuthenticated())
                return Json(new { code = 401, message = "You are not authorized to access this site!" }, JsonRequestBehavior.AllowGet);

            int numberOfElements = 0;

            var changeRequests = Search(ref numberOfElements,
                                 dataTableFilters);

            foreach(var c in changeRequests.Where(x=>x.Policy != null))
            {
                if (_policyService.GetInsuranceType((Guid)changeRequests.Where(x => x.Id == c.Id).FirstOrDefault().Policy) == 141) {
                    changeRequests.Where(x => x.Id == c.Id).FirstOrDefault().tmp_ShowSalary = true;
                }
            }

            return Json(new
            {
                start = dataTableFilters.start,
                recordsTotal = numberOfElements,
                recordsFiltered = numberOfElements,
                data = changeRequests
            }, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<ChangeRequestDTO> Search(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            dataTableFilters.accountId = Identity.PortalUserSession.ene_Account.Value;
            return _changeReqService.GetRecords(ref numberOfElements, dataTableFilters);
        }

        [Route("create/{id}")]
        [HttpGet]
        public ActionResult Create(Guid id)
        {
            PolicyDTO ene_Policy = _policyService.GetRecord(id);

            var censusChangeReq = _changeReqService.GenerateTemplateCensusChangeReq(ene_Policy);
            int insuranceType = ene_Policy.ene_insuranceType != null? (int)ene_Policy.ene_insuranceType.Value : 0;
            TempData["insuranceType"] = insuranceType;
            return View(censusChangeReq);
        }

        [Route("createfromtemplate/{id}")]
        [HttpGet]
        public ActionResult CreateFromTemplate(Guid id)
        {
            try
            {
                CensusLineDTO censusLine = _censusLineService.GetRecord(id);
                PolicyDTO ene_Policy = _policyService.GetRecord(censusLine.ene_PolicyId.Value);

                var censusChangeReq = _changeReqService.GenerateTemplateCensusChangeReqFromCensusLine(censusLine);
                int insuranceType = ene_Policy.ene_insuranceType != null ? (int)ene_Policy.ene_insuranceType.Value : 0;
                TempData["insuranceType"] = insuranceType;
                return View("Create", censusChangeReq);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        [HttpPost]
        public ActionResult Create(ChangeRequestDTO changeRequest)
        {
            try
            {
                if (!_changeReqService.Validate(ModelState, changeRequest))
                {
                    return View(changeRequest);
                }

                var censusChangeRequestId = _changeReqService.AddNewRecordAddRequest(changeRequest);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"SUCCESS: Census Line New Change Request Saved (" + censusChangeRequestId.ToString() + ")",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line New Change Request"
                });

                return RedirectToAction("Index", "CensusChangeReq", null);
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"FAILED: {ex.Message}",
                    ene_LongDescription = $"{ex.Message}, {ex.InnerException?.Message}\n",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line New Change Request"
                });

                return Json(new { Success = false, Message = ex.Message });
            }
        }

        [HttpGet]
        public ActionResult Edit(Guid id)
        {
            try
            {
                CensusLineDTO censusLine = _censusLineService.GetRecord(id);

                if (censusLine == null || _changeReqService.ChangeRequestExists(id))
                {
                    TempData["changeRequestExists"] = true;
                    return RedirectToAction("Index", "CensusLine", new { page = 0, policyId = censusLine.ene_PolicyId });
                }

                PolicyDTO policy = null;

                if(censusLine.ene_PolicyId != null)
                    policy = _policyService.GetRecord((Guid)censusLine.ene_PolicyId);

                ChangeRequestDTO changeReq = _changeReqService.GenerateCensusChangeReqFromCensusLine(censusLine);

                ViewData["policy"] = policy;
                ViewData["censusLineNo"] = censusLine.ene_CensusLinenumber;

                return View(changeReq);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult Edit(ChangeRequestDTO changeReq)
        {
            try
            {
                if (changeReq == null)
                    return View("Edit", changeReq);


                _changeReqService.AddNewRecordEditRequest(changeReq);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"SUCCESS: Census Line Edit Change Request Saved (" + changeReq.CensusLineId.ToString() + ")",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line Edit Change Request"
                });

                return RedirectToAction("Index", "CensusChangeReq", null);
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"FAILED: {ex.Message}\n(" + changeReq.CensusLineId.ToString() + ")",
                    ene_LongDescription = $"{ex.Message}, {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line Edit Change Request"
                });

                return Json(new { Success = false, Message = ex.Message + "\n- " + ex.InnerException?.Message });
            }
        }

        [HttpGet]
        public ActionResult Delete(Guid id)
        {
            if (_changeReqService.ChangeRequestExists(id))
                return RedirectToAction("Details", "CensusLine", new { id, error = "This census line already has a change request pending for approval." });

            CensusLineDTO censusLine = _censusLineService.GetRecord(id);

            if (censusLine == null)
                return View("Index", censusLine);

            ChangeRequestDTO ccr = _changeReqService.GenerateCensusChangeReqFromCensusLine(censusLine);

            return View("Terminate", ccr);
        }

        [HttpPost]
        public ActionResult Terminate(ChangeRequestDTO changeReq, CensusLineDTO censusLine)
        {
            try
            {
                PolicyDTO policy = _policyService.GetRecord(changeReq.Policy.Value);

                if (changeReq.DateLeft == null || changeReq.DateLeft < policy.ene_StartDate || changeReq.DateLeft > policy.ene_EndDate)
                {
                    ModelState.Clear();
                    ModelState.AddModelError("DateLeft", "Date is invalid!");
                    return View("Terminate", changeReq);
                }

                var censusChangeRequestId = _changeReqService.AddNewRecordTerminateRequest(changeReq, censusLine);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"SUCCESS: Census Line Terminate Change Request Saved ( " + censusChangeRequestId.ToString() + ")",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line Terminate Change Request"
                });

                return RedirectToAction("Index", "CensusChangeReq", null);
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"FAILED: {ex.Message}\n" + changeReq.CensusLineId.ToString(),
                    ene_LongDescription = $"{ex.Message}, {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line Terminate Change Request"
                });

                return Json(new { Success = false, Message = ex.Message + "\n- " + ex.InnerException?.Message });
            }
        }

        [HttpPost]
        public JsonResult Termination(Guid censusLineId, DateTime terminationDate)
        {
            try
            {
                CensusLineDTO censusLineDTO = _censusLineService.GetRecord(censusLineId);
                ChangeRequestDTO changeReq = _changeReqService.GenerateCensusChangeReqFromCensusLine(censusLineDTO);

                changeReq.DateLeft = terminationDate;
                changeReq.RequestTypeNumber = 200000002;
                PolicyDTO policy = _policyService.GetRecord(changeReq.Policy.Value);
                if (terminationDate == null || terminationDate < censusLineDTO.ene_DateJoined || terminationDate > policy.ene_EndDate)
                {
                    ModelState.Clear();
                    return Json(new { Success = false, Message = "Termination date must be valid! It must be between census line join date and assigned policy end date.", Code = 404 });
                }

                var censusChangeRequestId = _changeReqService.AddNewRecordTerminateRequest(changeReq, censusLineDTO);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"SUCCESS: Census Line Terminate Change Request Saved ( " + censusChangeRequestId.ToString() + ")",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line Terminate Change Request"
                });
                
                return Json(new { Success = true, Message = "Successfuly created revoke change request." , Code = 200});
                //return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.ene_Account.Value.ToString(),
                    ene_Description = $"FAILED: {ex.Message}\n" + censusLineId.ToString(),
                    ene_LongDescription = $"{ex.Message}, {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Census Line Terminate Change Request"
                });

                return Json(new { Code = 500, Success = false, Message = ex.Message + "\n- " + ex.InnerException?.Message });
            }
        }

        [HttpGet]
        public ActionResult GetCustomers(string cont_fullname = null,
                                 string acc_name = null,
                                 bool? includeCitizenShip = false,
                                 bool? includeresidenceCountry = false,
                                 bool? includeCompany = false, Guid? policyid=null)
        {
            dynamic response = null;

            try
            {
            
                if (!string.IsNullOrWhiteSpace(cont_fullname))
                    response = _changeReqService.GetContacts(cont_fullname,
                                                             includeCitizenShip,
                                                             includeresidenceCountry,
                                                             includeCompany, policyid);
                else if (!string.IsNullOrWhiteSpace(acc_name))
                    response = _changeReqService.GetAccounts(acc_name);

                return Json(new { data = response, code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { data = e.Message, code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCustomersInsured(string cont_fullname = null, Guid? policyid = null)
        {
            dynamic response = null;

            try
            {

                /*if (!string.IsNullOrWhiteSpace(cont_fullname))
                    response = _changeReqService.GetInsuredCustomers(cont_fullname, policyid);*/

                response = _changeReqService.GetInsuredCustomers(cont_fullname, policyid);

                return Json(new { data = response, code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { data = e.Message, code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCustomersPaymentResponsible(string cont_fullname = null, Guid? policyid = null)
        {
            dynamic response = null;

            try
            {

                if (!string.IsNullOrWhiteSpace(cont_fullname))
                    response = _changeReqService.GetPaymentResponsibleCustomers(cont_fullname, policyid);

                return Json(new { data = response, code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { data = e.Message, code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public ActionResult GetCustomersPrincipal(string cont_fullname = null, Guid? policyid = null)
        {
            dynamic response = null;

            try
            {

                /*if (!string.IsNullOrWhiteSpace(cont_fullname))*/
                    response = _changeReqService.GetPrincipalCustomers(cont_fullname, policyid);

                return Json(new { data = response, code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { data = e.Message, code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }


        [HttpGet]
        public ActionResult GetCountries()
        {
            try
            {            
                return Json(new { data = _changeReqService.GetCountries(), code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { data = e.Message, code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }
       
        [HttpGet]
        public ActionResult GetPlans(Guid? policyid = null)
        {
            try
            {
                dynamic _data = _changeReqService.GetPlans(policyid); 
                return Json(new { data =_data, code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception e)
            {
                return Json(new { data = e.Message, code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }

        public async Task<JsonResult> GetChangeRequestFilters()
        {
            try
            {
                var statuses = await _sharedService.GetEnumerationListAsync<ene_censuschangerequest_statuscode>();
                var types = await _sharedService.GetChangeRequestTypesAsync();

                return Json(new { types = types, statuses = statuses }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(JsonRequestBehavior.AllowGet);
            }
        }
    }
}
