﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Enetel.BLL.Interfaces;
using Enetel.BLL.Helpers;
using Enetel.DTO;
using PagedList;
using System.IO;
using System.Threading.Tasks;

namespace EnetelApp.Controllers
{
    public class PersonController : Controller
    {
        IPerson _personService;
        ILog _logService;
        IShared _sharedService;
        IFile _fileService;

        public PersonController(IPerson personService,
                                ILog logService,
                                IShared sharedService,
                                IFile fileService)
        {
            _personService = personService;
            _logService = logService;
            _sharedService = sharedService;
            _fileService = fileService;
        }

        [HttpGet]
        public async Task<ActionResult> Index()
        {
            Session["CURRENTPAGE"] = "PERSON";

            TempData["documentTypes"] = await _sharedService.GetEnumerationListAsync<FilterAttributesDTO.DocumentTypes>();

            return View();
        }

        public JsonResult Index_Ajax(FilterAttributesDTO dataTableFilters)
        {
            if (!Identity.UserAuthenticated())
                return Json(new { code = 401, message = "You are not authorized to acces this page!" }, JsonRequestBehavior.AllowGet);

            int numberOfElements = 0;

            var persons = Search(ref numberOfElements, 
                                 dataTableFilters);

            return Json(new { start = dataTableFilters.start,
                              recordsTotal = numberOfElements,
                              recordsFiltered = numberOfElements,
                              data = persons }, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<PersonDTO> Search(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            dataTableFilters.accountId = Identity.PortalUserSession.ene_Account.Value;

            return _personService.GetRecords(ref numberOfElements, dataTableFilters);
        }

        // GET: Person/Create
        public ActionResult Create()
        {
            ViewBag.GenderList = _sharedService.GetListFromOptionSet("contact", "ene_gender");
            return View();
        }

        // POST: Person/Create
        [HttpPost]
        public ActionResult Create(PersonDTO model)
        {
            try
            {
                //check external id 
                if(!String.IsNullOrEmpty(model.ExternalId))
                {
                    if (_personService.PersonExists(model.ExternalId))
                        ModelState.AddModelError("ExternalId", "It is not possible to create a new person record because another person already exists in the system with the same data.");
                }else
                {
                    if (String.IsNullOrEmpty(model.FirstName))
                        ModelState.AddModelError("FirstName", "Field is required!");

                    if (String.IsNullOrEmpty(model.LastName))
                        ModelState.AddModelError("LastName", "Field is required!");
                }

                //check gender code 
                if (model.GenderCode == null)
                    ModelState.AddModelError("GenderCode", "Field is required!");

                //check firstname and lastname
                if (String.IsNullOrEmpty(model.FirstName) && String.IsNullOrEmpty(model.LastName))
                {
                    if (String.IsNullOrEmpty(model.ExternalId))
                        ModelState.AddModelError("ExternalId", "Field is required!");
                }

                //check date of birth
                if (model.DateOfBirth == null || model.DateOfBirth == default(DateTime))
                {
                    if (model.YearOfBirth == null || model.YearOfBirth == 0)
                        ModelState.AddModelError("YearOfBirth", "Field is required!");
                }

                //check year of birth
                if (model.YearOfBirth == null || model.YearOfBirth == 0)
                {
                    if (model.DateOfBirth == null || model.DateOfBirth == default(DateTime))
                        ModelState.AddModelError("DateOfBirth", "Field is required!");
                }

                //check citizenshipId
                if (model.CitizenshipId == null || model.CitizenshipId == default(Guid))
                    ModelState.AddModelError("CitizenshipId", "Field is required!");

                //check residenceCountryId
                if (model.ResidenceCountryId == null || model.ResidenceCountryId == default(Guid))
                    ModelState.AddModelError("ResidenceCountryId", "Field is required!");

                if (!String.IsNullOrEmpty(model.FirstName) && !String.IsNullOrEmpty(model.LastName))
                {
                    if (model.DateOfBirth != null && model.DateOfBirth != default(DateTime))
                    {
                        if (_personService.PersonExists(model.FirstName, model.LastName, model.DateOfBirth.Value.Year))
                            ModelState.AddModelError("DateOfBirth", "It is not possible to create a new person record because another person already exists in the system with the same data.");
                    }

                    if (model.YearOfBirth != null && model.YearOfBirth != 0)
                    {
                        if (_personService.PersonExists(model.FirstName, model.LastName, model.YearOfBirth.Value))
                            ModelState.AddModelError("YearOfBirth", "It is not possible to create a new person record because another person already exists in the system with the same data.");
                    }
                }

                if (!ModelState.IsValid)
                {
                    ViewBag.GenderList = _sharedService.GetListFromOptionSet("contact", "ene_gender");
                    return View(model);
                }

                _personService.AddNewRecord(model);

                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_Description = $"Successfully created person: {model.FirstName} {model.LastName} {model.ExternalId}",
                    ene_LongDescription = "",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Person"
                });

                return RedirectToAction("Index");
            }
            catch (Exception ex)
            {
                _logService.AddNewRecord(new EventLogDTO
                {
                    ene_RecordID = Identity.PortalUserSession.Id.ToString(),
                    ene_Description = $"Failed: {ex.Message}",
                    ene_LongDescription = $"{ex.Message} {ex.InnerException?.Message}",
                    ene_ObjectTypeCode = 1,
                    ene_ActionType = "Create Person"
                });

                ViewBag.GenderList = _sharedService.GetListFromOptionSet("contact", "ene_gender");

                return View(model);
            }
        }

        // GET: Person/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Person/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult UploadDocument(ParameterDTO parameters, HttpPostedFileBase File)
        {
            try
            {
                parameters.FileMemoryStream = File.InputStream;
                parameters.FileName = $"{DateTime.Now.Year}{DateTime.Now.Month}{DateTime.Now.Day}{DateTime.Now.Hour}{DateTime.Now.Minute}{DateTime.Now.Second}";
                parameters.FileMEMEType = File.ContentType;

                var fileUploaded = _fileService.UploadFileToSharepoint(parameters);

                if (fileUploaded)
                    _personService.AddDocument(Guid.Parse(parameters.PersonId), parameters.FileType);

                return Json(new { code = 200, message = "Successfully uploaded file!" });
            }
            catch (Exception ex)
            {
                return Json(new { code = 500, message = $@"Server error {ex.Message}, {ex.InnerException?.Message}!" });
            }
        }

        public async Task<JsonResult> GetGenders() {
            return await Task.Run(() =>
            {
                return Json(_sharedService.GetListFromOptionSet("contact", "ene_gender"), JsonRequestBehavior.AllowGet);
            });
        }
    }
}
