﻿using Enetel.BLL.Interfaces;
using System;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace EnetelApp.Controllers
{
    public class HomeController : Controller
    {
        private readonly ICensusChangeReq _changeReqService;
        private readonly IPolicy _policyService;
        private readonly ICensusLine _censusLineService;
        private readonly ILog _logService;
        private readonly IShared _sharedService;
        private CensusChangeReqController controller;

        public HomeController(IShared sharedService,
                              ICensusChangeReq changeReqService,
                              IPolicy policyService,
                              ICensusLine censusLineService,
                              ILog logService)
                            {
            _changeReqService = changeReqService;
            _policyService = policyService;
            _censusLineService = censusLineService;
            _logService = logService;
            _sharedService = sharedService;
            controller = new CensusChangeReqController(_changeReqService, _policyService, _censusLineService, _logService, _sharedService);
        }

        [HttpGet]
        public JsonResult ShowSidebar()
        {
            try
            {
                Session["IsSidebarOpen"] = true;

                return Json(new { code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult HideSidebar()
        {
            try
            {
                Session["IsSidebarOpen"] = false;

                return Json(new { code = 200 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception)
            {
                return Json(new { code = 404 }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetCountries()
        {
            return Json(new { data = _sharedService.GetCountries() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult GetCurrencies()
        {
            return Json(new { data = _sharedService.GetCurrencies() }, JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Playground()
        {
            return View();
        }
    }
}