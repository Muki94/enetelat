﻿using PagedList;
using System;
using System.Web.Mvc;
using Enetel.DTO;
using Enetel.BLL.Interfaces;
using System.Collections.Generic;

namespace EnetelApp.Controllers
{
    public class LogController : Controller
    {
        private readonly ILog _logService;

        public LogController(ILog logService)
        {
            _logService = logService;
        }

        [HttpGet]
        public ActionResult Index(int? page)
        {
            Session["CURRENTPAGE"] = "LOG";

            return View();
        }

        public JsonResult Index_Ajax(FilterAttributesDTO dataTableFilters)
        {
            int numberOfElements = 0;

            var persons = Search(ref numberOfElements,
                                 dataTableFilters);

            return Json(new
            {
                start = dataTableFilters.start,
                recordsTotal = numberOfElements,
                recordsFiltered = numberOfElements,
                data = persons
            }, JsonRequestBehavior.AllowGet);
        }

        public IEnumerable<EventLogDTO> Search(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            return _logService.GetRecords(ref numberOfElements, dataTableFilters);
        }
    }
}
