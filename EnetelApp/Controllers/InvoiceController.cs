﻿using Enetel.BLL.Helpers;
using Enetel.BLL.Interfaces;
using Enetel.DTO;
using PagedList;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Security;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace EnetelApp.Controllers
{
    public class InvoiceController : Controller
    {
        IInvoice _invoiceService;
        IPolicy _policyService;
        IFile _fileService;

        public InvoiceController(IInvoice invoiceService, 
                                 IPolicy policyService,
                                 IFile fileService)
        {
            _invoiceService = invoiceService;
            _policyService = policyService;
            _fileService = fileService;
        }

        // GET: Invoice
        public ActionResult Index(int? page, Guid? policyId)
        {
            Session["CURRENTPAGE"] = "INVOICE";

            if (!Identity.UserAuthenticated() || !Identity.PortalUserSession.canAccessInvoices)
                return RedirectToAction("Index", "Login", null);

            return View();
        }

        public JsonResult Index_Ajax(FilterAttributesDTO dataTableFilters)
        {
            int numberOfElements = 0;

            var persons = Search(ref numberOfElements,
                                 dataTableFilters);

            return Json(new
            {
                start = dataTableFilters.start,
                recordsTotal = numberOfElements,
                recordsFiltered = numberOfElements,
                data = persons
            }, JsonRequestBehavior.AllowGet);
        }

        // GET: Invoice/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        public IEnumerable<InvoiceDTO> Search(ref int numberOfElements, FilterAttributesDTO dataTableFilters)
        {
            //accountId
            dataTableFilters.accountId = Identity.PortalUserSession.ene_Account.Value;
            //policyId
            return _invoiceService.GetRecords(ref numberOfElements, dataTableFilters);
        }

        [HttpGet]
        public ActionResult DownloadFile(ParameterDTO parameters)
        {
            try
            {
                var file = _fileService.GetFileFromSharepoint(parameters);

                if (file == null)
                    return Json(new { Code = 404, Message = "File not found on file server!" }, JsonRequestBehavior.AllowGet);

                using (var memoryStream = new MemoryStream())
                {
                    file.FileStream.CopyTo(memoryStream);
                    return Json( new { Code = 200, FileStream = memoryStream.ToArray(), file.FileType, file.FileName, Message = "File successfully downloaded!" }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json(new { Code = 500, Message = $"Server error, {ex.Message}, {ex.InnerException?.Message}"}, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpGet]
        public JsonResult GetPolicies()
        {
            return Json(_policyService.GetRecords().Where(x => x.ene_policyStatus.ToUpper() == "ACTIVE" || x.ene_EndDate < DateTime.Now).ToList(), JsonRequestBehavior.AllowGet);
        }
    }
}
