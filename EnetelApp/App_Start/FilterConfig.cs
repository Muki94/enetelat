﻿using EnetelApp.CustomAttributes;
using System.Web.Mvc;

namespace EnetelApp
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new CustomAuthorizeAttribute());
            filters.Add(new InternationalizationAttribute());
        }
    }
}
