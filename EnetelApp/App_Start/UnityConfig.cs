using System.Web.Mvc;
using Unity;
using Unity.Mvc5;
using Enetel.BLL.Interfaces;
using Enetel.BLL.Services;
using Enetel.DTO;
using Microsoft.Xrm.Sdk;

namespace EnetelApp
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IPolicy, PolicyService>();
            container.RegisterType<IPortalUser, PortalUserService>();
            container.RegisterType<ILog, LogService>();
            container.RegisterType<ICensusLine, CensusLineService>();
            container.RegisterType<ICensusChangeReq, CensusChangeReqService>();
            container.RegisterType<ICrypto, CryptoService>();
            container.RegisterType<IPerson, PersonService>();
            container.RegisterType<IShared, SharedService>();
            container.RegisterType<IInvoice, InvoiceService>();
            container.RegisterType<IFile, FileService>();
            container.RegisterType<IPlan, PlanService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}