﻿using Enetel.BLL.Helpers;
using System.Web.Mvc;
using System.Web.Routing;

namespace EnetelApp.CustomAttributes
{
    public class CustomAuthorizeAttribute: AuthorizeAttribute
    {
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!Identity.UserAuthenticated())
                filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Login" }));
        }
    }
}