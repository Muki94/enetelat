﻿jQuery(function ($) {
    $(".sidebar-dropdown > a").click(function () {
        $(".sidebar-submenu").slideUp(200);
        if (
          $(this)
            .parent()
            .hasClass("active")
        ) {
            $(".sidebar-dropdown").removeClass("active");
            $(this)
              .parent()
              .removeClass("active");
        } else {
            $(".sidebar-dropdown").removeClass("active");
            $(this)
              .next(".sidebar-submenu")
              .slideDown(200);
            $(this)
              .parent()
              .addClass("active");
        }
    });

    $("#close-sidebar").click(function () {
        $(".page-wrapper").removeClass("toggled");

        $.ajax({
            url: "/Home/HideSidebar",
            type: "GET",
            success: (data) => {

            },
            error: (err) => {

            }
        })
    });

    $("#show-sidebar").click(function () {
        $(".page-wrapper").addClass("toggled");

        $.ajax({
            url: "/Home/ShowSidebar",
            type: "GET",
            success: (data) => {

            },
            error: (err) => {

            }
        })
    });

    $("input[type=date]").on("change", function () {

        let presetValue = this.value;

        if (!presetValue)
            presetValue = this.defaultValue;
        
        let formatedDate = moment(presetValue, "YYYY-MM-DD").format(this.getAttribute("data-date-format"));

        if (!formatedDate || formatedDate.toUpperCase() == "INVALID DATE")
            formatedDate = "";

        this.setAttribute("data-date", formatedDate);
    }).trigger("change");
});

function spinnerOn(button) {
    button.attr("disabled", "disabled");
    button.html(`<div class="fa fa-spinner fa-spin fa-1x"></div>`);
}

function spinnerOff(button, buttonText) {
    button.removeAttr("disabled");
    button.html(buttonText);
}

function spinnerOnDataTable(el) {
    el.html(`<i class="fa fa-spinner fa-spin fa-3x fa-fw"></i><span class="sr-only">Loading...</span>`);
}

function spinnerOffDataTable(el) {
    el.html(``);
}

function accordionateRows(dt, currentPressedRow, formatCallback) {
    let row = '';
    let dataTableRow = '';
    let icon = '';
    //Get all shown rows
    let shownRows = $(".shown");

    //Get current pressed row and save its current state
    let tr = $(currentPressedRow).closest('tr');
    let currentRowIsShown = dt.row(tr).child.isShown();

    //Hide all shown rows
    for (var j = 0; j < shownRows.length; j++) {

        row = $(shownRows[j]).closest('tr');
        dataTableRow = dt.row(row);
        dataTableRow.child.hide();
        icon = row.find('i');
        row.removeClass('shown');
        var ii = icon[0];
        //ii.addClass('fa-plus');
        //ii.removeClass('fa-minus'); Ove zakomentarisane linije su uzrokovale da se i ostale ikonice u istom redu promjene
        ii.classList.add('fa-plus');
        ii.classList.remove('fa-minus');
    }

    icon = $(currentPressedRow).find('i');
    dataTableRow = dt.row(tr);

    if (!currentRowIsShown) {
        dataTableRow.child(formatCallback(dataTableRow.data())).show();
        tr.addClass('shown');
        icon.removeClass('fa-plus');
        icon.addClass('fa-minus')
    }
}
