﻿$(document).ready(function () {

    var dt = $('#censusLinesTable').DataTable({
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "paggination": true,
        "ajax": {
            "url": "/CensusLine/Index_Ajax",
            "data": function (d) {
                d.status = d.columns[1].search.value;
                d.insuredRole = d.columns[4].search.value;
                d.plan = $("#planSelectList").val();
                d.searchValue = d.search.value;
                d.policyId = $("#policyId").val();
                d.orderColumnNumber = d.order[0].column;
                d.orderColumnOrientation = d.order[0].dir;
                delete d.columns;
            },
            "beforeSend": function () {
                spinnerOnDataTable($("#censusLinesTable_processing"));
            },
            "complete": function () {
                spinnerOffDataTable($("#censusLinesTable_processing"));
            }
        },
        "columns": [
            {
                "orderable": false,
                "data": null,
                "defaultContent": "",
                "render": function () {
                    return `<a class="actionButton details-control" name="expandDetails" href="#"><i class="fa fa-plus"></i></a>`;
                }
            },
            {
                "data": "ene_CensusLinenumber",
            },
            {
                "data": "ene_InsuredFullName",
            },
            {
                "data": "ene_uniqueid",
            },
            {
                "data": "ene_InsuredRole",
            },
            {
                "data": "ene_Plan",
            },
            {
                "data": "ene_DateJoined",
            },
            {
                "data": "ene_Rate",
            },
            {
                "orderable": false,
                "data": null,
                "searchable": false,
                "render": function (data) {

                    let censusLineButton = ``;

                    if (data.statecode == 0)
                        censusLineButton += `<a class="actionButton" href="/CensusChangeReq/Edit?id=${data.Id}" data-toggle="tooltip" data-placement="top" data-container="body" title="Edit"><i class="fa fa-edit"></i></a>`;

                    censusLineButton += `<a class="actionButton" href="/CensusChangeReq/createfromtemplate/${data.Id}" data-toggle="tooltip" data-placement="top" title="Create new from this one"><i class="fa fa-clone"></i></a>`;

                    return censusLineButton;
                }
            }
        ],
        columnDefs: [
             {
                 targets: 6,
                 render: function (data) {
                     return moment(data).format("DD-MM-YYYY");
                 }
             }
        ],
        initComplete: function () {
            //create filter containers
            let statusesFilterContainer = `<div id="statusesFilter" style="display:inline;"></div>`;
            let insuredRoleFilterContainer = `<div id="insuredRolesFilter" style="display:inline;"></div>`;
            let planFilterContainer = `<div id="plansFilter" style="display:inline;"></div>`;

            $("#censusLinesTable_filter").append($(statusesFilterContainer));
            $("#censusLinesTable_filter").append($(insuredRoleFilterContainer));
            $("#censusLinesTable_filter").append($(planFilterContainer));

            spinnerOn($("#statusesFilter"));
            spinnerOn($("#insuredRolesFilter"));
            spinnerOn($("#plansFilter"));

            //add filter logic
            this.api().columns(1).every(function () {
                let column = this;

                getCensusLineStatuses().then((data) => {

                    let selectControl = `<select id="statusSelectList" class="dataTables_filter ml-1" style="height:33px">`;
                    for (var i = 0; i < data.length; i++) {
                        if (data[i].Key == 200000002) {
                            selectControl += `<option value="${data[i].Key}">Inactive</option>`;
                        } else {
                            selectControl += `<option value="${data[i].Key}">${data[i].Value}</option>`;
                        }
                        
                    }

                        selectControl += `</select>`;

                        $("#statusesFilter").html(selectControl);

                        var select = $(document).on('change', '#statusSelectList', function () {
                            var val = $.fn.dataTable.util.escapeRegex($(this).val());

                            column.search(val ? val : '', true, false)
                                  .draw();
                        });
                }).catch((err) => {
                    toast.error("Something went wrong while loading census line statuses!");
                });
            });

            this.api().columns(4).every(function () {

                let column = this;

                getInsuredRoles().then((data) => {
                    let selectControl = `<select id="insuredRoleSelectList" class="dataTables_filter ml-1" style="height:33px">`;
                    selectControl += `<option value="">All roles...</option>`;

                    for (var i = 0; i < data.length; i++)
                        selectControl += `<option value="${data[i].Key}">${data[i].Value}</option>`;

                    selectControl += `</select>`;

                    $("#insuredRolesFilter").html(selectControl);

                    var select = $(document).on('change', '#insuredRoleSelectList', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());

                        column.search(val ? val : '', true, false)
                              .draw();
                    });
                }).catch((err) => {
                    toast.error("Something went wrong while loading insured roles!");
                });
            });

            this.api().columns(5).every(function () {

                let column = this;

                getPlans().then((data) => {
                    let selectControl = `<select id="planSelectList" class="dataTables_filter ml-1" style="height:33px">`;
                    selectControl += `<option value="">All plans...</option>`;

                    for (var i = 0; i < data.length; i++)
                        selectControl += `<option value="${data[i].Id}">${data[i].PlanName}</option>`;

                    selectControl += `</select>`;

                    $("#plansFilter").html(selectControl);

                    var select = $(document).on('change', '#planSelectList', function () {
                        var val = $.fn.dataTable.util.escapeRegex($(this).val());

                        column.search(val ? val : '', true, false)
                              .draw();
                    });
                }).catch((err) => {
                    toast.error("Something went wrong while loading insured roles!");
                });
            });
        }
        , "order": [[1, 'asc']]
    });


    function format(d) {

        let isLifeInsurance = document.getElementById("insuranceType").value.toUpperCase() == "LIFE" ? true : false;
        let insuredRoleExists = (d.ene_InsuredRoleValue != 200000000 && d.ene_InsuredRoleValue != null) ? true : false;

        let accordion = `<div class="card card-override">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">`;

        if (insuredRoleExists) {
            accordion += `<div class="row">
                             <div class="col-md-5 left-container">
                                <label class="control-label highlight left-container-text" for="ene_PrincipalName">Principal</label>
                             </div>
                             <div class="col-md-7 right-container">
                                <span class="right-container-text">
                                    ${d.ene_PrincipalName ? d.ene_PrincipalName : ""}
                                </span>
                            </div>
                         </div>`;
        }

        accordion += `<div class="row">
                            <div class="col-md-5 left-container">
                                <label class="control-label highlight left-container-text" for="ene_Gender">Gender</label>
                            </div>
                            <div class="col-md-7 right-container">
                                <span class="right-container-text">
                                    ${d.ene_Gender ? d.ene_Gender : ""}
                                </span>
                            </div>
                        </div>

                    <div class="row">
                        <div class="col-md-5 left-container">
                            <label class="control-label highlight left-container-text" for="ene_DateofBirth">Date Of Birth</label>
                        </div>
                        <div class="col-md-7 right-container">
                            <span class="right-container-text">
                                ${d.ene_DateofBirth ? moment(d.ene_DateofBirth).format("DD-MM-YYYY") : ""}
                            </span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-5 left-container">
                            <label class="control-label highlight left-container-text" for="ene_Age">Age</label>
                        </div>
                        <div class="col-md-7 right-container">
                            <span class="right-container-text">
                                ${d.ene_Age ? d.ene_Age : ""}
                            </span>
                        </div>
                    </div>`;

        if (isLifeInsurance) {
            accordion += `<div class="row">
                            <div class="col-md-5 left-container" >
                                <label class="control-label highlight left-container-text" for="ene_Salary">Salary</label>
                            </div >
                            <div class="col-md-7 right-container">
                                <span class="right-container-text">${d.ene_Salary ? d.ene_Salary : ""}</span>
                            </div>
                        </div >
                        <div class="row">
                            <div class="col-md-5 left-container">
                                <label class="control-label highlight left-container-text" for="ene_SalaryCurrency">Salary Currency</label>
                            </div>
                            <div class="col-md-7 right-container">
                                <span class="right-container-text">
                                    ${d.ene_SalaryCurrency ? d.ene_SalaryCurrency : ""}
                                </span>
                            </div>
                        </div>`;
        }

        accordion += `</div>`;

        accordion += `<div class="col-md-4 col-sm-12">
                            <div class="row">
                                <div class="col-md-5 left-container">
                                    <label class="control-label highlight left-container-text" for="DateLeft">Date Left</label>
                                </div>
                                <div class="col-md-7 right-container">
                                    <span class="right-container-text">
                                        ${d.ene_DateLeft ? moment(d.ene_DateLeft).format("DD-MM-YYYY") : ""}
                                    </span>
                                </div>
                            </div>            
    
                            <div class="row">
                                <div class="col-md-5 left-container">
                                    <label class="control-label highlight left-container-text" for="ene_SumInsured">Sum Insured</label>
                                </div>
                                <div class="col-md-7 right-container">
                                    <span class="right-container-text">
                                        ${d.ene_SumInsured ? d.ene_SumInsured : ""}
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 left-container">
                                    <label class="control-label highlight left-container-text" for="ene_SumInsuredCurrency">Sum Insured Currency</label>
                                </div>
                                <div class="col-md-7 right-container">
                                    <span class="right-container-text">
                                        ${d.ene_SumInsuredCurrency ? d.ene_SumInsuredCurrency : ""}
                                    </span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-5 left-container">
                                    <label class="control-label highlight left-container-text" for="CreatedOn">Created On</label>
                                </div>
                                <div class="col-md-7 right-container">
                                    <span class="right-container-text">
                                        ${d.CreatedOn ? moment(d.CreatedOn).format("DD-MM-YYYY") : ""}
                                    </span>
                                </div>
                            </div>`;

        if(isLifeInsurance){
            accordion += `<div class ="row">
                            <div class ="col-md-5 left-container">
                                <label class ="control-label highlight left-container-text" for="ene_SalaryDescription">Salary Description</label>
                            </div>
                            <div class ="col-md-7 right-container">
                                <span class ="right-container-text">
                                    ${d.ene_SalaryDescription}
                            </div>
                        </div>`;
        }

        accordion += `</div>`;


        accordion += `<div class="col-md-4 col-sm-12">
                        <div class="row">
                            <div class="col-md-5 left-container">
                                <label class="control-label highlight left-container-text" for="ene_InsuredCategoryforCover">Insured Category for Cover</label>
                            </div>
                            <div class="col-md-7 right-container">
                                <span class="right-container-text">
                                    ${d.ene_InsuredCategoryforCover ? d.ene_InsuredCategoryforCover : ""}
                                </span>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-5 left-container">
                                <label class="control-label highlight left-container-text" for="ene_Ageatenrollment">Age At Enrollment</label>
                            </div>
                            <div class="col-md-7 right-container">
                                <span class="right-container-text">
                                    ${d.ene_Ageatenrollment ? d.ene_Ageatenrollment : ""}
                                </span>
                            </div>
                        </div>

                       <div class ="row">
                            <div class ="col-md-5 left-container">
                                <label class ="control-label highlight left-container-text" for="ene_PaymentResponsible">Payment Responsible</label>
                            </div>
                            <div class="col-md-7 right-container">
                                <span class="right-container-text">
                                    ${d.ene_PaymentResponsible ? d.ene_PaymentResponsible : ""}
                                </span>
                            </div>
                        </div>

                        <div class ="row">
                            <div class ="col-md-5 left-container">
                                <label class ="control-label highlight left-container-text" for="ene_MembershipCard">Membership Card</label>
                            </div>
                            <div class="col-md-7 right-container">
                                <span class="right-container-text">
                                    ${d.ene_MembershipCard ? d.ene_MembershipCard : ""}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>`;

        return accordion;
    }

    $(document).on("click", "[name=expandDetails]", function (e) {
        accordionateRows(dt, e.currentTarget, format);
    });

    // Grab the datatables input box and alter how it is bound to events
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("input keyup", function (e) { // Bind our desired behavior
            if (e.keyCode == 13) {
                // Call the API search function
                dt.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dt.search("").draw();
            }

            return;
        });

    function getCensusLineStatuses() {
        return new Promise((res, rej) => {
            $.ajax({
                url: '/CensusLine/GetCensusLineStatuses',
                type: 'GET',
                success: function (data) {

                    getPolicyStatus().then(policyStatus => {

                        let modifiedArray = [];

                        if (policyStatus.Key.toUpperCase() == "INACTIVE") {
                            let indexOfStatusActive = data.findIndex(x => x.Value.toUpperCase() == "INACTIVE");
                            modifiedArray.push(data[indexOfStatusActive]);
                            return res(modifiedArray);
                        }

                        return res(data);
                    })
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }

    function getPolicyStatus() {
        return new Promise((res, rej) => {
            $.ajax({
                url: `/Policy/GetPolicyStatus?policyId=${$("#policyId").val()}`,
                type: 'GET',
                success: function (data) {
                    return res(data);
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }

    function getInsuredRoles() {
        return new Promise((res, rej) => {
            $.ajax({
                url: '/CensusLine/GetCensusLineInsuredRoles',
                type: 'GET',
                success: function (data) {
                    return res(data);
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }

    function getPlans() {
        return new Promise((res, rej) => {
            $.ajax({
                url: `/CensusLine/GetPlans?policyId=${$("#policyId").val()}`,
                type: 'GET',
                success: function (data) {
                    return res(data);
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }
});
