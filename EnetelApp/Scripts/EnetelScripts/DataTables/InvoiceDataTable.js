﻿$(document).ready(function () {
    var dt = $('#invoicesTable').DataTable({
        "autoWidth": false,
        "dom": 'flrtip',
        "processing": true,
        "serverSide": true,
        "paggination": true,
        "ajax": {
            "url": "/Invoice/Index_Ajax",
            "data": function (d) {
                d.searchValue = d.search.value;
                d.policyId = d.columns[0].search.value;
                d.orderColumnNumber = d.order[0].column;
                d.orderColumnOrientation = d.order[0].dir;
                delete d.columns;
            },
            "beforeSend": function () {
                spinnerOnDataTable($("#invoicesTable_processing"));
            },
            "complete": function () {
                spinnerOffDataTable($("#invoicesTable_processing"));
            }
        },
        "columns": [
            {
                "data": "DocumentNo",
                "searchable": true
            },
            {
                "data": "Policy",
                "searchable": true
            },
            {
                "data": "PeriodStartDate",
                "searchable": true
            },
            {
                "data": "PeriodEndDate",
                "searchable": false
            },
            {
                "data": "AmountForPay",
                "searchable": false
            },
            {
                "data": "DueDate",
                "searchable": true
            },
            {
                "data": "StatusReason",
                "searchable": true
            },
            {
                "orderable": false,
                "data": null,
                "searchable": false,
                "render": function (data) {
                    return `<div class="actionButton" name="downloadInvoice" data-toggle="tooltip" data-placement="top" title="Download" data-folder='${data.DocumentNo}' data-file='${data.Id.replace("-", "")}'><i class="fa fa-arrow-circle-down" style="cursor:pointer;"></i></div>`;
                }
            }
        ],
        columnDefs: [
             {
                 targets: [2, 3, 5],
                 render: function (data) {
                     return moment(data).format("DD-MM-YYYY");
                 }
             }
        ],
        initComplete: function () {

            let policiesFilterContainer = `<div id="policiesFilter" style="display:inline;"></div>`;

            $("#invoicesTable_filter").append($(policiesFilterContainer));

            spinnerOn($("#policiesFilter"));

            this.api().columns(0).every(function () {
                var column = this;

                getPolicies().then((data) => {
                    let selectControl = `<select id="policyFilter" class="dataTables_filter ml-1" style="height:33px">
                                                <option value="">Policies...</option>`;

                    for (var i = 0; i < data.length; i++)
                        selectControl += `<option value="${data[i].Id}">${data[i].ene_PolicyPeriodNo}</option>`;

                    selectControl += `</select>`;

                    $("#policiesFilter").html(selectControl);   

                    var select = $(document).on('change', '#policyFilter', function () {
                                                    column.search($(this).val(), true, false)
                                                          .draw();
                        });
                }).catch((err) => {
                    toast.error("Something went wrong while loading policies!");
                });
            });
        },
        "order": [[0, 'asc']]
    });

    $("#invoicesTable_filter").find("label").css("display", "none")

    $(document).on("click", "[name=downloadInvoice]", function (e) {
        let currentTarget = $(e.currentTarget);

        let oldButtonText = currentTarget.html();

        spinnerOn(currentTarget);

        let folder = currentTarget.attr("data-folder");
        let file = currentTarget.attr("data-file");

        $.ajax({
            url: "/Invoice/DownloadFile",
            type: "GET",
            data: {
                FolderPath: `/Account/Beatles/ene_invoice/${folder}_${file}`
            },
            dataType: "json",
            success: (res) => {

                spinnerOff(currentTarget, oldButtonText);

                if (res.Code == 200) {
                    var bytes = new Uint8Array(res.FileStream); // pass your byte response to this constructor
                    var blob = new Blob([bytes], { type: res.FileType });// change resultByte to bytes

                    var link = document.createElement('a');
                    let fileUrl = window.URL.createObjectURL(blob);
                    link.href = fileUrl;
                    link.download = res.FileName;
                    link.click();

                    window.open(fileUrl);

                    toastr.success(res.Message);
                    return;
                }

                toastr.error(res.Message);
            },
            error: (err) => {
                spinnerOff(currentTarget, oldButtonText);
            }
        })
    })

    function getPolicies() {
        return new Promise((res, rej) => {
            $.ajax({
                url: '/Invoice/GetPolicies',
                type: 'GET',
                success: function (data) {
                    return res(data);
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }

    // Grab the datatables input box and alter how it is bound to events
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("input keyup", function (e) { // Bind our desired behavior
            // If the length is 3 or more characters, or the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dt.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dt.search("").draw();
            }

            return;
        });
});
