﻿$(document).ready(function () {
    var dt = $('#policiesTable').DataTable({
        "autoWidth": false,
        "dom": 'flrtip', //ako se zeli dodati filter samo dodati slovo f
        "processing": true,
        "serverSide": true,
        "paggination": true,
        "ajax": {
            "url": "/Policy/Index_Ajax",
            "data": function (d) {
                d.policyStatusCode = d.columns[0].search.value;
                d.orderColumnNumber = d.order[0].column;
                d.orderColumnOrientation = d.order[0].dir;
                delete d.columns;
            },
            "beforeSend": function () {
                spinnerOnDataTable($("#policiesTable_processing"));
            },
            "complete": function () {
                spinnerOffDataTable($("#policiesTable_processing"));
            }
        },
        "columns": [
            {
                "orderable": false,
                "data": null,
                "defaultContent": "",
                "render": function () {
                    return `<a class="actionButton details-control" name="expandDetails" href="#"><i class="fa fa-plus"></i></a>`;
                }
            },
            {
                "data": "ene_policynumber",
            },
            {
                "data": "ene_ExternalPolicyNo",
            },
            {
                "data": "tmp_insuranceType",
            },
            {
                "data": "ene_StartDate",
            },
            {
                "data": "ene_EndDate",
            },
            {
                "data": "ene_GroupSize",
            },
            {
                "orderable": false,
                "data": null,
                "searchable": false,
                "render": function (data) {
                    return `<a class="actionButton" href="/CensusLine?policyId=${data.Id}" data-toggle="tooltip" data-placement="top" title="Census Lines"><i class="fa fa-archive"></i></a>`;
                }
            }
        ],
        columnDefs: [
             {
                 targets: [4, 5],
                 render: function (data) {
                     return moment(data).format("DD-MM-YYYY");
                 }
             }
        ],
        initComplete: function () {

            let policiesFilterContainer = `<div id="statusesFilter" style="display:inline;"></div>`;

            $("#policiesTable_filter").append($(policiesFilterContainer));

            spinnerOn($("#statusesFilter"));

            this.api().columns(0).every(function () {
                var column = this;

                getPolicyStatuses().then((data) => {
                    let selectControl = `<select id="statusSelectList" class="dataTables_filter ml-1" style="height:33px">`;

                    for (var i = 0; i < data.length; i++)
                        selectControl += `<option value="${data[i].Key}">${data[i].Value}</option>`;

                    selectControl += `</select>`;

                    $("#statusesFilter").html(selectControl);

                    var select = $(document).on('change', '#statusSelectList', function () {

                        var val = $.fn.dataTable.util.escapeRegex($(this).val());

                        column.search(val ? val : '', true, false)
                              .draw();
                    });

                }).catch((err) => {
                    toast.error("Something went wrong while loading policy statuses!");
                });
            });
        },
        "order": [[1, 'asc']]
    });

    $("#policiesTable_filter").find("label").css("display", "none")

    $(document).on("click", "[name=expandDetails]", function (e) {
        accordionateRows(dt, e.currentTarget, format);
    });

    function format(d) {
        return `<div class="card card-override">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-4 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-5 left-container">
                                            <label class="control-label highlight left-container-text" for="ene_AdditionalIndividualUnderwriting">Additional Individual Underwriting</label>
                                        </div>
                                        <div class="col-md-7 right-container">
                                            <span class="right-container-text">
                                                ${d.ene_AdditionalIndividualUnderwriting ? d.ene_AdditionalIndividualUnderwriting : ""}
                                            </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-4 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-5 left-container">
                                            <label class="control-label highlight left-container-text" for="CreatedOn">Created On</label>
                                        </div>
                                        <div class="col-md-7 right-container">
                                            <span class="right-container-text">
                                                ${d.CreatedOn ? moment(d.CreatedOn).format("DD-MM-YYYY") : ""}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>`;
    }

    function getPolicyStatuses() {
        return new Promise((res, rej) => {
            $.ajax({
                url: '/Policy/GetPolicyStatuses',
                type: 'GET',
                success: function (data) {
                    return res(data);
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }
});
