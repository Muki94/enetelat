﻿$(document).on("click", "[name=deleteButton]", (e) => {
    let selectedTagName = $(e.target).prop("tagName");
    let selectedElementId = null;

    if (selectedTagName && selectedTagName == "I")
        selectedElementId = $(e.target).parent().attr("data-id");
    else
        selectedElementId = $(e.target).attr("data-id");

    Swal.fire({
        title: 'Are you sure?',
        icon: 'error',
        showCancelButton: true,
        confirmButtonText: 'Yes, disable it!',
        confirmButtonColor: '#d33',
        cancelButtonColor: '#3085d6',
        showLoaderOnConfirm: true,
        preConfirm: () => {

            return $.ajax({
                url: "/PortalUser/Delete",
                type: "POST",
                data: {
                    id: selectedElementId
                },
                success: (res) => {
                    return res;
                },
                error: (err) => {
                    return err;
                }
            })
        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            if (result.value.Code == 200) {
                Swal.fire(
                    'Disabled!',
                    'User is marked as inactive.',
                    'success'
                );
                location.reload();
            } else if (result.value.Code == 401) {
                location.href = "/Login";
            } else if (result.value.Code == 500) {
                Swal.showValidationMessage(
                    `Request failed server error occured!`
                )
            }
        }
    })
})

$(document).ready(function () {

    var dt = $('#portalUsersTable').DataTable({
        "dom": 'lrtip', //ako se zeli dodati filter samo dodati slovo f
        "processing": true,
        "serverSide": true,
        "paggination": true,
        "ajax": {
            "url": "/PortalUser/Index_Ajax",
            "data": function (d) {
                d.orderColumnNumber = d.order[0].column;
                d.orderColumnOrientation = d.order[0].dir;
                delete d.columns;
            },
            "beforeSend": function () {
                spinnerOnDataTable($("#portalUsersTable_processing"));
            },
            "complete": function () {
                spinnerOffDataTable($("#portalUsersTable_processing"));
            }
        },
        "columns": [
            {
                "orderable": false,
                "data": null,
                "defaultContent": "",
                "render": function () {
                    return `<a class="actionButton details-control" name="expandDetails" href="#"><i class="fa fa-plus"></i></a>`;
                }
            },
            {
                "data": "ene_FirstName",
            },
            {
                "data": "ene_LastName",
            },
            {
                "data": "ene_Email",
            },
            {
                "data": "ene_name",
            },
            {
                "data": "CreatedOn",
            },
            {
                "data": "statuscode",
            },
            {
                "orderable": false,
                "data": null,
                "render": function (data) {

                    let renderButtons = `<a class="actionButton" href="/PortalUser/Edit?id=${data.Id}" data-toggle="tooltip" data-placement="top" title="Edit"><i class="fa fa-edit"></i></a>`;

                    if (data.statuscode.toUpperCase() == "ACTIVE")
                        renderButtons += `<a name="deleteButton" class="actionButton" href="#" data-toggle="tooltip" data-placement="top" title="Disable" data-id="${data.Id}"><i class="fa fa-times"></i></a>`;

                    return renderButtons;
                }
            }
        ],
        columnDefs: [
        {
            targets: 5,
            render: function (data) {
                return moment(data).format("DD-MM-YYYY");
            }
        }
        ],
        "order": [[1, 'asc']]
    });

    $(document).on("click", "[name=expandDetails]", function (e) {
        accordionateRows(dt, e.currentTarget, format);
    });

    function format(d) {
        return `<div class="card card-override">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="ene_PhoneNumber">Phone Number</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.ene_PhoneNumber != null ? d.ene_PhoneNumber : ""}
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="ene_Admin">Is Admin</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.ene_Admin != null ? d.ene_Admin : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class ="control-label highlight left-container-text" for="canAccessInvoices">Can Access Financial Details</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.canAccessInvoices != null ? d.canAccessInvoices : ""}
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;
    }
});
