﻿$(document).ready(function () {

    var dt = $('#logsTable').DataTable({
        "dom": 'lrtip',
        "processing": true,
        "serverSide": true,
        "paggination": true,
        "ajax": {
            "url": "/Log/Index_Ajax",
            "data": function (d) {
                d.orderColumnNumber = d.order[0].column;
                d.orderColumnOrientation = d.order[0].dir;
                delete d.columns;
            },
            "beforeSend": function () {
                spinnerOnDataTable($("#logsTable_processing"));
            },
            "complete": function () {
                spinnerOffDataTable($("#logsTable_processing"));
            }
        },
        "columns": [
            {
                "data": "ene_ActionType",
                "searchable": true
            },
            {
                "data": "ene_Description",
                "searchable": true
            },
            {
                "data": "ene_LongDescription",
                "searchable": false
            },
            {
                "data": "ene_ObjectTypeCode",
                "searchable": false
            },
            {
                "data": "CreatedOn",
                "searchable": true
            }
        ],
        columnDefs: [
             {
                 targets: 4,
                 render: function (data) {
                     return moment(data).format("DD-MM-YYYY");
                 }
             }
        ],
        "order": [[4, 'desc']]
    });

    // Grab the datatables input box and alter how it is bound to events
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("input keyup", function (e) { // Bind our desired behavior
            // If the length is 3 or more characters, or the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dt.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dt.search("").draw();
            }

            return;
        });
});
