﻿$(document).ready(function () {

    var dt = $('#changeRequestsTable').DataTable({
        "autoWidth": false,
        "dom": 'flrtip',
        "processing": true,
        "serverSide": true,
        "paggination": true,
        "ajax": {
            "url": "/CensusChangeReq/Index_Ajax",
            "data": function (d) {
                d.searchValue = d.search.value;
                d.policyId = d.columns[0].search.value;
                d.orderColumnNumber = d.order[0].column;
                d.orderColumnOrientation = d.order[0].dir;
                d.status = d.columns[6].search.value;
                d.changeReqType = d.columns[7].search.value;
                delete d.columns;
            },
            "beforeSend": function () {
                spinnerOnDataTable($("#changeRequestsTable_processing"));
            },
            "complete": function () {
                spinnerOffDataTable($("#changeRequestsTable_processing"));
            }
        },
        "columns": [
            {
                "orderable": false,
                "data": null,
                "defaultContent": "",
                "render": function () {
                    return `<a class="actionButton details-control" name="expandDetails" href="#"><i class="fa fa-plus"></i></a>`;
                }
            },
            {
                "data": "Name",
            },
            {
                "data": "CreatedOn",
            },
            {
                "data": "FullName",
            },
            {
                "data": "externalID",
            },
            {
                "data": "tmp_PolicyName",
            },
            {
                "data": "RequestType",
            },
            {
                "data": "StatusCode",
            }
        ],
        columnDefs: [
            {
                targets: 2,
                render: function (data) {
                    return moment(data).format("DD-MM-YYYY");
                }
            }
        ],
        initComplete: function () {

            //prepair selects with spinners
            let statusesFilterContainer = `<div id="statusesFilter" style="display:inline;"></div>`;
            let typesFilterContainer = `<div id="typesFilter" style="display:inline;"></div>`;

            $("#changeRequestsTable_filter").append($(statusesFilterContainer));
            $("#changeRequestsTable_filter").append($(typesFilterContainer));

            spinnerOn($("#statusesFilter"));
            spinnerOn($("#typesFilter"));

            getCensusChangeReqFilters().then((data) => {
                this.api().columns(6).every(function () {
                    var column = this;

                    let selectControl = `<select id="statusSelectList" class="dataTables_filter ml-1" style="height:33px">
                                                     <option>Status...</option>`;
                    for (var i = 0; i < data.statuses.length; i++)
                        selectControl += `<option value="${data.statuses[i].Key}">${data.statuses[i].Value}</option>`;

                    selectControl += `</select>`;

                    $("#statusesFilter").html(selectControl);

                    var select = $(document).on('change', '#statusSelectList', function () {

                        var val = $(this).val();

                        column.search(val ? val : '', true, false)
                            .draw();
                    });

                });

                this.api().columns(7).every(function () {
                    var column = this;
                    let selectControl = `<select id="typeSelectList" class="dataTables_filter ml-1" style="height:33px">
                                                <option>Type...</option>`;

                    for (var i = 0; i < data.types.length; i++)
                        selectControl += `<option value="${data.types[i].Key}">${data.types[i].Value}</option>`;

                    selectControl += `</select>`;

                    $("#typesFilter").html(selectControl);

                    var select = $(document).on('change', '#typeSelectList', function () {

                        var val = $(this).val();

                        column.search(val ? val : '', true, false)
                            .draw();
                    });
                });
            }).catch((err) => {
                toastr.error("Something went wrong while loading change request statuses!");
            });
        },
        "order": [[1, 'asc']]
    });

    function format(d) {
        let acorrdion = `<div class="card card-override">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-4 col-sm-12">
                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="tmp_InsuredRole">Insured Role</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.tmp_InsuredRole != null ? d.tmp_InsuredRole : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="tmp_PrincipalName">Principal</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.tmp_PrincipalName ? d.tmp_PrincipalName : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="FirstName">First Name</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.FirstName ? d.FirstName : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="LastName">Last Name</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.LastName ? d.LastName : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="GenderName">Gender</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.GenderName ? d.GenderName : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="DateofBirth">Date Of Birth</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.DateofBirth ? moment(d.DateofBirth).format("DD-MM-YYYY") : ""}
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12">

                                        ${(d.tmp_ShowSalary ? 
                                            `<div class="row" >
                                                <div class="col-md-5 left-container">
                                                    <label class="control-label highlight left-container-text" for="Salary">Salary</label>
                                                </div>
                                                <div class="col-md-7 right-container">
                                                    <span class="right-container-text">
                                                        ${d.Salary ? d.Salary : ""}
                                                    </span>
                                                </div>
                                            </div >
            
                                            <div class="row">
                                                <div class="col-md-5 left-container">
                                                    <label class="control-label highlight left-container-text" for="SalaryCurrency">Salary Currency</label>
                                                </div>
                                                <div class="col-md-7 right-container">
                                                    <span class="right-container-text">
                                                        ${d.SalaryCurrency ? d.SalaryCurrency : ""}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="row" id="SalaryDescriptionD">
                                                <div class="col-md-5 left-container">
                                                    <label class="control-label highlight left-container-text" for="SalaryDescription">Salary Description</label>
                                                </div>
                                                <div class="col-md-7 right-container">
                                                    <span class="right-container-text">
                                                        ${d.SalaryDescription ? d.SalaryDescription : ""}
                                                    </span>
                                                </div>
                                            </div>` : ""
                                        )}
                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="tmp_PaymentResponsibleFullName">Payment Responsible</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.tmp_PaymentResponsibleFullName ? d.tmp_PaymentResponsibleFullName : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="tmp_PlanName">Plan</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.tmp_PlanName ? d.tmp_PlanName : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="DateJoined">Date Joined</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.DateJoined ? moment(d.DateJoined).format("DD-MM-YYYY") : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="DateLeft">Date Left</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.DateLeft ? moment(d.DateLeft).format("DD-MM-YYYY") : ""}
                                                </span>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-md-4 col-sm-12">

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="SumInsured">Sum Insured</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.SumInsured ? d.SumInsured : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="SumInsuredCurrency">Sum Insured Currency</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.SumInsuredCurrency ? d.SumInsuredCurrency : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="tmp_PortalUserName">Submmitted By</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.tmp_PortalUserName ? d.tmp_PortalUserName : ""}
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="tmp_CensusLineName">Census Line</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    <a name="openCensusLineDetailsModal" data-censusLineId='${d.CensusLineId}' href='#'>${d.tmp_CensusLineName ? d.tmp_CensusLineName : ""}</a>
                                                </span>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-5 left-container">
                                                <label class="control-label highlight left-container-text" for="Remarks">Remarks</label>
                                            </div>
                                            <div class="col-md-7 right-container">
                                                <span class="right-container-text">
                                                    ${d.Remarks ? d.Remarks : ""}
                                                </span>
                                            </div>
                                        </div>`;


        if (d.StatusCode.toUpperCase() == "REJECTED") {
            acorrdion += `<div class="row">
                                                <div class="col-md-5 left-container">
                                                    <label class="control-label highlight left-container-text" for="RejectionReason">Rejection Reason</label>
                                                </div>
                                                <div class="col-md-7 right-container">
                                                    <span class="right-container-text">
                                                        ${d.RejectionReason ? d.RejectionReason : ""}
                                                    </span>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="col-md-5 left-container">
                                                    <label class="control-label highlight left-container-text" for="RejectedOtherReason">Rejection Reason Other</label>
                                                </div>
                                                <div class="col-md-7 right-container">
                                                    <span class="right-container-text">
                                                        ${d.RejectedOtherReason ? d.RejectedOtherReason : ""}
                                                    </span>
                                                </div>
                                            </div>`;
        }

        acorrdion += `      </div>
                                </div>
                            </div>
                        </div>`;


        return acorrdion;
    }

    $(document).on("click", "[name=expandDetails]", function (e) {
        accordionateRows(dt, e.currentTarget, format);
    });

    // Grab the datatables input box and alter how it is bound to events
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("input keyup", function (e) { // Bind our desired behavior
            // If the length is 3 or more characters, or the user pressed ENTER, search
            if (e.keyCode == 13) {
                // Call the API search function
                dt.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dt.search("").draw();
            }

            return;
        });


    $(document).on("click", "[name=openCensusLineDetailsModal]", function () {
        let censusLineId = $(this).attr("data-censusLineId");
        window.open(`/CensusLine/Details?id=${censusLineId}`, '_blank', "width=1500,height=400, top=300, left=200");
    })

    $(function () {
        $.ajaxSetup({ cache: false });
        $("a[data-modal]").on("click", function (e) {
            $('#CensusLineDetailsModalContent').load(this.href, function () {
                $('#CensusLineDetailsModal').modal({
                    keyboard: true
                }, 'show');
            });
            return false;
        });
    });

    function getCensusChangeReqFilters() {
        return new Promise((res, rej) => {
            $.ajax({
                url: '/CensusChangeReq/GetChangeRequestFilters',
                type: 'GET',
                success: function (data) {
                    return res(data);
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }
});
