﻿$(document).ready(function () {

    var dt = $('#personsTable').DataTable({
        "autoWidth": false,
        "processing": true,
        "serverSide": true,
        "paggination": true,
        "ajax": {
            "url": "/Person/Index_Ajax",
            "data": function (d) {
                d.searchValue = d.search.value;
                d.genderCode = d.columns[3].search.value;
                d.orderColumnNumber = d.order[0].column;
                d.orderColumnOrientation = d.order[0].dir;
                delete d.columns;
            },
            "beforeSend": function () {
                spinnerOnDataTable($("#personsTable_processing"));
            },
            "complete": function () {
                spinnerOffDataTable($("#personsTable_processing"));
            }
        },
        "columns": [
            {
                "orderable": false,
                "data": null,
                "defaultContent": "",
                "render": function () {
                    return `<a class="actionButton details-control" name="expandDetails" href="#"><i class="fa fa-plus"></i></a>`;
                }
            },
            {
                "data": "FullName",
                "searchable": true
            },
            {
                "data": "ExternalId",
                "searchable": true
            },
            {
                "data": "Gender",
                "searchable": false
            },
            {
                "data": "DateOfBirth",
                "searchable": false
            },
            {
                "data": "Citizenship",
                "searchable": true
            },
            {
                "data": "ResidenceCountry",
                "searchable": true
            },
            {
                "orderable": false,
                "data": null,
                "searchable": false,
                "render": function (data) {
                    return `<a class="actionButton" name="uploadFile" href="#" data-toggle="tooltip" data-placement="top" title="Upload Document" data-accountname="${data.ParentCustomerName}" data-personfullname="${data.FullName}" data-personid="${data.Id}"><i class="fa fa-arrow-circle-up"></i></a>`;
                }
            }
        ],
        columnDefs: [
             {
                 targets: 4,
                 render: function (data) {
                     return moment(data).format("DD-MM-YYYY");
                 }
             }
        ],
        initComplete: function () {

            let genderFilterContainer = `<div id="gendersFilter" style="display:inline;"></div>`;

            $("#personsTable_filter").append($(genderFilterContainer));

            spinnerOn($("#gendersFilter"));

            this.api().columns(3).every(function () {
                var column = this;

                getGenders().then((data) => {
                    let selectControl = `<select id="genderSelectList" class="dataTables_filter ml-1" style="height:33px">
                                                <option value="0">Gender...</option>`;

                    for (var i = 0; i < data.length; i++)
                        selectControl += `<option value="${data[i].Key}">${data[i].Value}</option>`;

                    selectControl += `</select>`;

                    $("#gendersFilter").html(selectControl);

                    var select = $(document).on('change', '#genderSelectList', function () {

                        var val = $.fn.dataTable.util.escapeRegex($(this).val());

                        column.search(val ? val : '', true, false)
                              .draw();
                    });
                }).catch((err) => {
                    toast.error("Something went wrong while loading gender codes!");
                });
            });
        },
        "order": [[1, 'asc']]
    });

    $(document).on("click", "[name=expandDetails]", function (e) {
        accordionateRows(dt, e.currentTarget, format);
    });

    function format(d) {
        return `
                    <div class="card card-override">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6 col-sm-12">
                                    <div class="row">
                                        <div class="col-md-5 left-container">
                                            <label class="control-label highlight left-container-text" for="ResidenceCountry">Received Document ID</label>
                                        </div>
                                        <div class="col-md-7 right-container">
                                            <span class="right-container-text">
                                                ${d.documentTypeId ? d.documentTypeId : ""}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5 left-container">
                                            <label class="control-label highlight left-container-text" for="ResidenceCountry">Received Document Health Questionnaire</label>
                                        </div>
                                        <div class="col-md-7 right-container">
                                            <span class="right-container-text">
                                                ${d.documentTypeHealthQuestionnaire ? d.documentTypeHealthQuestionnaire : ""}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5 left-container">
                                            <label class="control-label highlight left-container-text" for="ResidenceCountry">Received Document Text Document</label>
                                        </div>
                                        <div class="col-md-7 right-container">
                                            <span class="right-container-text">
                                                ${d.documentTypeTextDocument ? d.documentTypeTextDocument : ""}
                                            </span>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-5 left-container">
                                            <label class="control-label highlight left-container-text" for="ResidenceCountry">Received Document Other</label>
                                        </div>
                                        <div class="col-md-7 right-container">
                                            <span class="right-container-text">
                                                ${d.documentTypeOther ? d.documentTypeOther  : ""}
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    `;
    }

    function getGenders() {
        return new Promise((res, rej) => {
            $.ajax({
                url: '/Person/GetGenders',
                type: 'GET',
                success: function (data) {
                    return res(data);
                },
                error: function (err) {
                    return res(err);
                }
            })
        });
    }

    // Grab the datatables input box and alter how it is bound to events
    $(".dataTables_filter input")
        .unbind() // Unbind previous default bindings
        .bind("input keyup", function (e) { // Bind our desired behavior
            // If the length is 3 or more characters, or the user pressed ENTER, search
            debugger;
            if (e.keyCode == 13) {
                // Call the API search function
                dt.search(this.value).draw();
            }
            // Ensure we clear the search if they backspace far enough
            if (this.value == "") {
                dt.search("").draw();
            }

            return;
        });
});
