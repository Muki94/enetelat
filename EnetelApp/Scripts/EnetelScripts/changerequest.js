﻿$(document).ready(function () {

  
    //--Init on Principal
    $("#InsuredRole").val(200000000);

    //-- ReplaceSpecialCharacters
    function ReplaceSpecialCharacters(input) {
        return input.replace('ć', 'c').replace('ž', 'z').replace('č', 'c').replace('š', 's').replace('đ', 'd');
    }


    //--Init DateJoined (With Formating)
    (function () {
        let elem = $("input#DateJoined").on("change", function () {

            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                    .format(this.getAttribute("data-date-format"))
            )
        });

        if (elem.val() != undefined && elem.val() != null && elem.val() != "")
            elem.trigger("change");
    })();

    function SetReportingResponsible(id, name, thispersonisreportingresposible) {

        $("input#ReportingResponsible[type=hidden]").val(id);
        let ReportingResponsibleCont = $("input#ReportingResponsibleCont");
        ReportingResponsibleCont.val(name);

        if (thispersonisreportingresposible != "true") {
            ReportingResponsibleCont.removeAttr("readonly");
        }
        else {
            ReportingResponsibleCont.attr("readonly", "readonly");
        }

    }
    function SetPaymentResponsible(id, name, thispersonispaymentresposible) {

        $("input#PaymentResponsible[type=hidden]").val(id);
        let PaymentResponsibleCont = $("input#PaymentResponsibleCont");
        PaymentResponsibleCont.val(name);
        if (thispersonispaymentresposible != "true") {
            PaymentResponsibleCont.removeAttr("readonly");
        }
        else {
            PaymentResponsibleCont.attr("readonly", "readonly");
        }
    }
    /*$("select#ThispersonisPaymentResponsible").change(function () {

        if ($(this).val() == "true") {
            SetPaymentResponsible($("#Insured[type=hidden]").val(), $("#InsuredCont[type=text]").val(), "true");
        }
        else {
            SetPaymentResponsible('', '', "false");
        }

      
    });
    
    $("select#ThispersonisPaymentResponsible").trigger('change');

    $("select#ThispersonisReportingResponsible").change(function () {


        if ($(this).val() == "true") {
            SetReportingResponsible($("#Insured[type=hidden]").val(), $("#InsuredCont[type=text]").val(), "true");
        }
        else {
            SetReportingResponsible('', '', "false");
        }

       
    });
    $("select#ThispersonisReportingResponsible").trigger('change');
    */
    (function () {
        //--Load Plans
        $.ajax({
            url: "/CensusChangeReq/GetPlans",
            type: "GET",
            dataType: "json",//includeCitizenShip,includeresidenceCountry
            data: { policyid: $("input#Policy").val() },
            success: function (data) {

                //console.log(data);
                debugger;
                if (data.data != null && data.data != undefined) {
                    for (var i = 0; i < data.data.length; i++) {
                        $("#Plan").append("<option value='" + data.data[i].id + "'>" + data.data[i].name + "</option>");
                    }
                    /*let tmp_PlanName = $("#tmp_PlanName").val();
                    if (tmp_PlanName != null && tmp_PlanName != undefined) {
                        $("#Plan").val(tmp_PlanName);
                    }*/

                }

            },
            error: function (xhr, ajaxOptions, thrownError) {
                console.log(thrownError);
            }

        });
        //--


    })();
    $("#PaymentResponsibleCont[type=text]").autocomplete({
        minLength: 2,
        source: function (request, response) {
            $.ajax({
                url: "/CensusChangeReq/GetCustomersPaymentResponsible",
                type: "GET",
                dataType: "json",
                data: { cont_fullname: request.term, policyid: $("input#Policy").val()},
                success: function (data) {
                    console.log(data);
                    response($.map(data.data, function (item) {
                        return { label: item.name, value: item.name, id: item.id }
                    }))
                },


            })
        }
        , select: function (event, ui) {
            $("input#PaymentResponsible[type=hidden]").val(ui.item.id);
        }/*,
                                messages: {
                                    noResults: "", results: ""
                                }*/
    });
    $("#ReportingResponsibleCont[type=text]").autocomplete({
        minLength: 2,
        source: function (request, response) {
            $.ajax({
                url: "/CensusChangeReq/GetCustomers",
                type: "GET",
                dataType: "json",
                data: { cont_fullname: request.term, includeCompany: true },
                success: function (data) {
                    console.log(data);
                    response($.map(data.data, function (item) {
                        return { label: item.name, value: item.name, id: item.id }
                    }))
                }

            })
        },
        select: function (event, ui) {
            $("input#ReportingResponsible[type=hidden]").val(ui.item.id);
        }/*,
                                messages: {
                                    noResults: "", results: ""
                                }*/
    });


    //--Load CitizenshipCountries, ResCountryCountries
    let _CitizenshipCountries = [];
    let _ResCountryCountries = [];
    (function () {
        $.ajax({
            url: "/CensusChangeReq/GetCountries",
            type: "GET",
            dataType: "json",//includeCitizenShip,includeresidenceCountry
            data: null/*{ policyid: $("input#Policy").val() }*/,
            success: function (data) {
                console.log(data);

                for (let i = 0; i < data.data.length; i++) {
                    _CitizenshipCountries.push(data.data[i]);
                    _ResCountryCountries.push(data.data[i]);
                }

                //--Add Event Listener
                $("#CitizenshipCont[type=text]").autocomplete({
                    minLength: 0,
                    source: function (request, response) {

                        response($.map(_CitizenshipCountries, function (item) {
                            if (item.name.toLowerCase().includes(request.term.toLowerCase())) {
                                return { label: item.name, value: item.name, id: item.id }
                            }
                        }))
                    },
                    select: function (event, ui) {
                        $("input#Citizenship[type=hidden]").val(ui.item.id);
                    }/*,
                                messages: {
                                    noResults: "", results: ""
                                }*/
                }).focus(function () {
                    $(this).autocomplete('search', $(this).val());
                });

                //--

                //--Add Event Listener
                $("#ResidenceCountryCont[type=text]").autocomplete({
                    minLength: 0,
                    source: function (request, response) {

                        response($.map(_ResCountryCountries, function (item) {
                            if (item.name.toLowerCase().includes(request.term.toLowerCase())) {
                                return { label: item.name, value: item.name, id: item.id }
                            }
                        }))
                    },
                    select: function (event, ui) {
                        $("input#ResidenceCountry[type=hidden]").val(ui.item.id);
                    }/*,
                                messages: {
                                    noResults: "", results: ""
                                }*/
                }).focus(function () {
                    $(this).autocomplete('search', $(this).val());
                });

                //--

            }

        })
    })();

  


    //--Load CustomersInsured
    let _CustomersInsured = [];
    (function () {
        $.ajax({
            url: "/CensusChangeReq/GetCustomersInsured",
            type: "GET",
            dataType: "json",//includeCitizenShip,includeresidenceCountry
            data: { cont_fullname: null, policyid: $("input#Policy").val() },
            success: function (data) {
                console.log(data);

                for (let i = 0; i < data.data.length; i++) {
                    _CustomersInsured.push(data.data[i]);
                }

                //--Add Event Listener
                $("#InsuredCont[type=text]").autocomplete({
                    minLength: 0,
                    source: function (request, response) {

                        response($.map(_CustomersInsured, function (item) {
                            if (ReplaceSpecialCharacters(item.name.toLowerCase()).includes(ReplaceSpecialCharacters(request.term.toLowerCase()))) {
                                return {
                                    label: item.name, value: item.name, id: item.id,
                                    firstname: item.firstname, lastname: item.lastname,
                                    dateofbirth: item.dateofbirth, gender: item.gender,
                                    citizenship: item.citizenship,
                                    citizenship_countryname: item.citizenship_countryname,
                                    residencecountry: item.residencecountry,
                                    residencecountry_countryname: item.residencecountry_countryname,
                                    externalid: item.externalid,
                                    companyid: item.companyid,
                                    companyname: item.companyname
                                }
                            }
                        }))
                    },
                    select: function (event, ui) {
                        $("input#Insured[type=hidden]").val(ui.item.id);
                        $("input#Citizenship[type=hidden]").val(ui.item.citizenship);

                        if ($("select#ThispersonisPaymentResponsible").val() == "true") {
                            SetPaymentResponsible(ui.item.id, ui.item.value, "true");
                        }
                        /* else {
                             SetPaymentResponsible('', '');
                         }*/


                        if ($("select#ThispersonisReportingResponsible").val() == "true") {
                            SetReportingResponsible(ui.item.id, ui.item.value, "true");
                        }
                        /*else {
                            SetReportingResponsible('', '');
                        }*/


                        let CitizenshipCont = $("#Citizenship");
                        CitizenshipCont.val(ui.item.citizenship);
                        CitizenshipCont.removeAttr("readonly");

                        let ResidenceCountryCont = $("#ResidenceCountry");
                        ResidenceCountryCont.val(ui.item.residencecountry);
                        ResidenceCountryCont.removeAttr("readonly");

                        let inputfirstname = $("input#FirstName[type=text]");
                        inputfirstname.val(ui.item.firstname);

                        let externalID = $("input#externalID[type=text]");
                        externalID.val(ui.item.externalid);
                        // externalID.removeAttr("readonly");

                        let lastname = $("input#LastName[type=text]");
                        lastname.val(ui.item.lastname);

                        let dateofbirth = $("input#DateOfBirth[type=date]");
                        dateofbirth.val(ui.item.dateofbirth);
                        dateofbirth.trigger('change');
                        // dateofbirth.removeAttr("readonly");

                        let gender = $("select#Gender");
                        gender.val(ui.item.gender);

                        let personsaccount = $("input#PersonsAccount[type=hidden]")
                        personsaccount.val(ui.item.companyid);
                        let personsaccountcont = $("input#PersonsAccountCont[type=text]")
                        personsaccountcont.val(ui.item.companyname);
                        // personsaccountcont.removeAttr("readonly");


                    }/*,
                                messages: {
                                    noResults: "", results: ""
                                }*/
                }).focus(function () {
                    $(this).autocomplete('search', $(this).val())
                });;
                
                //--
              
            }

        })
    })();

    let _CustomersPrincipals = [];
    (function () {
        $.ajax({
            url: "/CensusChangeReq/GetCustomersPrincipal",
            type: "GET",
            dataType: "json",//includeCitizenShip,includeresidenceCountry
            data: { cont_fullname: null, policyid: $("input#Policy").val() },
            success: function (data) {
                console.log(data);

                for (let i = 0; i < data.data.length; i++) {
                    _CustomersPrincipals.push(data.data[i]);
                }

                //--Add Event Listener
                $("#PrincipalCont[type=text]").autocomplete({
                    minLength: 0,
                    source: function (request, response) {

                        response($.map(_CustomersPrincipals, function (item) {
                            if (ReplaceSpecialCharacters(item.name.toLowerCase()).includes(ReplaceSpecialCharacters(request.term.toLowerCase()))) {
                                return {
                                    label: item.name, value: item.name, id: item.id,
                                    firstname: item.firstname, lastname: item.lastname,
                                    dateofbirth: item.dateofbirth, gender: item.gender,
                                    citizenship: item.citizenship,
                                    citizenship_countryname: item.citizenship_countryname,
                                    residencecountry: item.residencecountry,
                                    residencecountry_countryname: item.residencecountry_countryname,
                                    externalid: item.externalid,
                                    companyid: item.companyid,
                                    companyname: item.companyname
                                }
                            }
                        }))
                    },
                    select: function (event, ui) {

                        $("input#Principal[type=hidden]").val(ui.item.id);

                    }/*,
                                messages: {
                                    noResults: "", results: ""
                                }*/
                }).focus(function () {
                    $(this).autocomplete('search', $(this).val())
                });;

                //--

            }

        })
    })();

    $("select#InsuredRole").change(function () {
        let PrincipalDIV = $("#div_Principal");
        let PrincipalCont = $("input#PrincipalCont");
        let input_Principal = $("input#Principal[type=hidden]");
        if ($(this).val() == 200000000) {
            PrincipalCont.attr("readonly", "readonly");
            PrincipalDIV.addClass("collapse");
            PrincipalCont.val('');
            input_Principal.val('');
        }
        else {
            PrincipalCont.removeAttr("readonly");
            PrincipalDIV.removeClass("collapse");
        }
       
    });

    $("select#InsuredRole").trigger('change');
})